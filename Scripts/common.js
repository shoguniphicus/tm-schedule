$(document).ready(function(e){
	$('.datepicker').datepicker();

	return;
    $('.packages-list').each(function(i, item){
        var packageLength = $(item).find('.item').length;

        var owlOptions = {
            navigation : true,
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem: false,
            items: packageLength < 6 ? packageLength : 5,
            lazyLoad : false,
            autoPlay : false,
            stopOnHover : true
        }

        if (packageLength < 6) {
            $(item).css('padding', '0');
            $(item).find('.item img').css({ width:'auto', 'height':'212px', margin:'0 auto', display:'block' });
        }
        $(item).owlCarousel(owlOptions);
    });
});