define(["declare",
 		"widget"], 
        function(declare,_Widget) {

	var module =  declare("scripts.api.Factory", [], {});

	/*
		Initializes Factory dependencies keypairs
	*/
	module.init =  function() {
		var pairsR = [
			["Basepro", "Base"],
			["BaseCarousel", "carousel/Base"],

			["BaseList", "list/Base"],

			["Schedule", "Schedule"],
			["ScheduleHour", "list/ScheduleHour"],
			["ScheduleHourCell", "list/cell/ScheduleHour"],
			["ScheduleDate", "list/ScheduleDate"],
			["ScheduleDateCell", "list/cell/ScheduleDate"],

			["ScheduleHead", "list/cell/ScheduleHead"],
			["ScheduleRow", "list/cell/ScheduleRow"],
			["ScheduleColumn", "list/cell/ScheduleColumn"]
		];

		function procDependencies(_input){
			var baseJSCompURL = dojoConfig.baseJSURL + "components/";
			var _output = [];

			array.forEach(_input, function(single, index){
				var sA = [single[0]];
				sA.push(baseJSCompURL+single[1]+".js");

				_output.push(sA);
			});

			return _output;
		}

		function addToConfig(){
			var aliases = dojoConfig.aliases;
			dojoConfig.aliases = aliases.concat(pairs);
		}

		var pairs = procDependencies(pairsR);
		addToConfig();

		return pairs;
	};
	
	/*
		Util function to match key with pair, and return the pair if the key matches
	*/
	module.match = function(key){
		var aliases = dojoConfig.aliases;

		//console.log(aliases);

		var keypair = false;
		array.forEach(aliases, function(single, index){
			if(_.contains(single, key)){
				keypair = single;
			}
		});

		return keypair;
	};

	/*
		Returns relative path to the class specify by key
	*/
	module.path = function(key){
		var keypair = module.match(key);
		
		if(keypair){
			return keypair[1];
		}else{
			console.warn("Module not found in aliases", key);
			return null;
		}
	};

	/*
		Returns the template / widget html path
	*/
	module.temp = function(uri){
		return "dojo/text!"+dojoConfig.baseTmptURL+uri+".html";
	};

	/*
		Return the class which can be used in require, ie, Cell where widget can use this
	*/
	module.clas = function(uri){
		return dojoConfig.baseJSURL +uri+".js";
	};

	/*
		Pop is for modal!
	*/
	module.pop = function(key, setting, callback){
		module.spawn(key, win.body(), setting, callback);
	};

	/*
		Bread and butter function! To Spawn and conquer!
	*/
	module.spawn =  function(key, dom, setting, callback){
		var found = false;
		var keypair = null;
	
		keypair = module.match(key);
		if(keypair)
			found = true;

		if(!found){
			console.warn("Module not found in js/api/Factory.js init()", key);
			return null;
		}

		if(typeof dom === "undefined" || dom == null){
			console.warn("Dom is invalid for components to be injected", key);
			return null;
		}

		require([keypair[1]], function(_class){
			var instance = new _class();

            instance.placeAt(dom);
            instance.startup();
            instance.construct(setting);

            if(callback)
	            callback(instance);

	        return instance;
		});
	};

	module.assasinate = function(_id){
		var target = "suicide."+_id;
		topic.publish(target);
	};

	return module;
});