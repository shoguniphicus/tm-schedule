define(["declare",
 		"widget"], 
        function(declare,_Widget) {

	var module =  declare("scripts.api.Element", [], {});

	/*
		Util function to inject select options
	*/
	module.select = function(key, value){
		var _el= "";

		//<option value="">Salutation</option>
		_el += "<option value='";
		_el += key;
		_el += "'>";
		_el += value;
		_el += "</option>";

		return _el;
	};

	/*
		Util function to inject img!
	*/
	module.img = function(src, _class){
		var _el = "";

		el += "<img class='";
		el += _class;
		el += "'";
		el += "src='";
		el += src;
		el += "' />";

		return el;
	}


	return module;
});