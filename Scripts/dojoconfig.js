var _locale = "eng";
var _baseURL = "//14.102.146.81/hypptv/";

switch(location.hostname){
	case "hypp.dev":
		_baseURL = "//hypp.dev";
	break;

	case "localhost":
		_baseURL = "//localhost/tm/hypptv";
	break;

	case "generic.dev":
		_baseURL = "//generic.dev/tm/schedule";
	break;

	default:
	break;
}

if($.jStorage.get("app.data.locale") != null){
	_locale = $.jStorage.get("app.data.locale");
}

dojoConfig = {
	has : {"dojo-firebug":true, "dojo-debug-messages":true },
	parseOnLoad : false,
	async : true,
	packages : [{name:"Hypp TV", location:"Scripts"}],
	aliases : [
		["ready", "dojo/ready"],
		["declare", "dojo/_base/declare"],
		["widget", "dijit/_Widget"],
		["templatemixin", "dijit/_TemplatedMixin"],
		["topic", "dojo/topic"],
		["Factory", _baseURL + "/Scripts/api/Factory.js"],
		["Element", _baseURL + "/Scripts/api/Element.js"]
	],
	localstorage: $.jStorage.storageAvailable(),
	isExperimental: true,
	baseURL: _baseURL,
	dataURL: _baseURL + "/data/",
	imgURL: _baseURL + "/PublishingImages/",
	baseJSURL: _baseURL + "/Scripts/",
	baseTmptURL: _baseURL + "/template/",
	locale: _locale
};

var ajaxConfig = {
	carousel: {
		'home': dojoConfig.dataURL + 'home_carousel.json',
		'homeEverywhere': dojoConfig.dataURL + 'home_everywhere_carousel.json',
		'homeBiz': dojoConfig.dataURL + 'home_biz_carousel.json',
		'package': dojoConfig.dataURL + 'package_carousel.json',
		'packageEverywhere': dojoConfig.dataURL + 'package_everywhere_carousel.json',
		'packageBiz': dojoConfig.dataURL + 'package_biz_carousel.json',
		'subscribe': dojoConfig.dataURL + 'subscribenow_carousel.json',
		'whathotHighlight': dojoConfig.dataURL + 'whathot_carousel.json',
		'whathotVOD': dojoConfig.dataURL + 'whathot_carousel.json',
		'schedule': dojoConfig.dataURL + 'schedule_carousel.json'
	},
	packages:{
		'unifi': dojoConfig.dataURL + "packages.json",
		'everywhere': dojoConfig.dataURL + "packages_everywhere.json",
		'biz': dojoConfig.dataURL + "packages_biz.json"
	},
	schedule: dojoConfig.dataURL + 'all_channels_by_day.json',
	whathot: {
		highlight: dojoConfig.dataURL + 'whathot_highlight.json',
		vod: dojoConfig.dataURL + 'whathot_vod.json'
	},
	faq: {
		category: dojoConfig.dataURL + 'faq_category.json'
	},
	remote: dojoConfig.dataURL + 'remote_data.json',
	search: dojoConfig.dataURL + 'searchresult.json'
}
		
		
String.prototype.insertAt = function(index, string) { 
	return this.substr(0, index) + string + this.substr(index);
}
		
var html, domConstruct, topic, win, windows, on, dom, domStyle, query, domAttr, domClass, array, config, timer, Fac , Elem= null;

$(function($) {
});