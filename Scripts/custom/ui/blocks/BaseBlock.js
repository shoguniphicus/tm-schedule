define(["dojo/_base/declare",
    "dijit/_Widget",
    "dojo/topic",
    "dojo/window",
    "dojo/_base/array",
    "dijit/_TemplatedMixin",
    "dojo/text!./html/BaseBlock.html",
    "dojo/dom-style",
    "dojo/query",
    "dojox/timing/_base",
    "dojo/html",
    "dojo/dom-attr",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom",
    "dojo/on",
    "dojo/dom-geometry",
    "dojo/_base/config"
    ],
    function(declare, _Widget, topic, windows, array, TemplatedMixin, template, domStyle, query, timer, html, domAttr, domConstruct, domClass, dom, on, domGeom, config) {

        return declare("js.custom.ui.blocks.BaseBlock", [_Widget, TemplatedMixin], {
            templateString: template,
            currentLabel: "",
            construct: function() {
                topic.publish("block.loaded");
            },
            constructor: function() {

            },
            postCreate: function postCreate() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;

            },
            startup: function() {
                this.inherited(arguments);
                var domNode = this.domNode;
                var context = this;

                currentLabel = "Base";
            },
            destroy: function() {
            	var domNode = this.domNode;
            	topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            	
                this.inherited(arguments);
            },
            initLabel: function() {
                var domNode = this.domNode;
                domAttr.set(domNode, "data-label", currentLabel);
            },
            initNavigationPush: function(navigationindex) {
                var domNode = this.domNode;
                var context = this;

                domAttr.set(domNode, "navigation-index-data", navigationindex);
                context.initNavigationTransition();
            },
            initNavigationTransition: function() {
                var domNode = this.domNode;
                var context = this;
                var index = domAttr.get(domNode, "navigation-index-data");

                var vs = windows.getBox();
                var width = vs.w;
                var height = vs.h;

                index = parseInt(index);
                domStyle.set(domNode, "left", ((index - 1) * 100) + "%");
            },
            announceCurrentLabel: function() {
                var context = this;
                var domNode = this.domNode;
                var currentLabel = domAttr.get(domNode, "data-label");

                topic.publish("navigation.change.label", currentLabel);
            },checktheScrolling: function(thescrollingDom, override){
                var context = this;
                var domNode = this.domNode;
                
                if(override == null)
                	override = 155;

                var vs = windows.getBox();
                var width = vs.w;
                //this screen height removed the isseu occur during submit when keyboard open
                //var height = screen.height;
                var height = vs.h;
                //changed by munir - fixed clipped loan account btn
                domStyle.set(thescrollingDom, "height", (height-override)+"px");
                // $("#tabbar").show();
            }
        });

});