define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/booktestdrive.html"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template) {
        return declare("js.custom.ui.blocks.shoppingtools.Booktestdrive", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function() {
                this.inherited(arguments);
                
                var context = this;
                
                context.setupContent();
                context.startListening();
            }, destroy:function(){
			   var context = this;
	           var domNode = this.domNode;
	           
	           topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            
	           this.inherited(arguments);
            }, initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, setupContent:function(){
	            var context = this;
	            var preset = $.jStorage.get("booktestdrive.preset");
	            
	            var request = $.ajax({
	            	url: config.hostURL+"api/car/get_all?testdrive=yes",
/*                   url: config.hostURL+"api/car/get_model_type", */
                  type: "GET",
                  cache: false,
                  dataType: "json"
                });
                 
                request.done(function( response ) {
                    if (response.status != "success") {
                        console.warn("Error retrieving data");
                        return;
                    }
                    
                    alltruckdata = response.data;
                    array.forEach(response.data, function(single, index){
                        var optionContainer = context.selectNode;
                        var singleOption = '';
                        var selected = '';
                        
                        if(preset == single.id){
                        	selected = "selected";
	                        $.jStorage.deleteKey("booktestdrive.preset");
                        }else{
	                        selected = "";
                        }
                        
                        singleOption = "<option value='"+single.id+"'  "+selected+">"+single.car_model_name+"</option>";
/*                         var singleOption = "<option value='"+single.key+"'>"+single.value+"</option>"; */
                        
                        $(context.modelNode).append(singleOption);
                    });
                });
                 
                request.fail(function( jqXHR, textStatus ) {
                });
                
                request.always(function() {
                });
            }, initInteraction:function(_captchaWidgetID){
	            var context = this;
	            
                var now = new Date();
                var dateComponent = $(context.dateNode).datepicker({
                    onRender: function(date) {
                        return date.valueOf() < now.valueOf() ? 'disabled' : '';
                    }
                }).on('changeDate', function(ev){
                    var newDate = new Date(ev.date);
                    
                    dateComponent.hide();
                }).data('datepicker');

                $.validator.addMethod(
                    "dateformat",
                    function(value, element) {
                        return value.match(/(\d{4})-(\d{2})-(\d{2})/);
                    }     
                );
                $(context.bookFormNode).validate({    
                    rules: {
                        date: {
                            dateformat: true,
                            required: true
                        },
                        model: {
                            required: true
                        },
                        name: {
                            required: true
                        },
                        email: {
                            email : true,
                            required: true
                        },
                        phone: {
                            minlength: 6,
                            maxlength: 12,
                            digits: true,
                            required: true
                        },
                        agree: {
                            required: true
                        }
                    },
                    messages: {
                        date: {
                            dateformat: "Please follow the date format",
                            required: "Please choose a date"
                        },
                        name: "Please enter your name",
                        phone: {
                            minlength: "Please enter a valid phone number",
                            maxlength: "Please enter a valid phone number",
                            required: "Please enter your phone number"
                        },
                        agree: "Please accept the Terms & Conditions"
                    },submitHandler: function(form) {
                        context.submit(_captchaWidgetID);
                    }
                });
	        },submit:function(_captchaWidgetID){
                var context = this;
                var date = $(context.dateNode).val();
                var model = $(context.modelNode).val(); 
                var name = $(context.nameNode).val();
                var email = $(context.emailNode).val(); 
                var phone = $(context.phoneNode).val();
                var agree = $(context.agreeNode).prop( "checked" );
                
                var dataPackage = new Object();
                dataPackage.whentest_date = date;
                dataPackage.model = model;
                dataPackage.fullname = name;
                dataPackage.email = email;
                dataPackage.phone = phone;
				
				if(grecaptcha.getResponse(_captchaWidgetID) == "")
				{
					alert("Please check the 'I'm not a robot' checkbox");
					return;
				}

                var request = $.ajax({
                  url: config.hostURL+"api/form/testdrive",
                  type: "POST",
                  data: dataPackage,
                  cache: false,
                  dataType: "json"
                });
                 
                request.done(function( response ) {
                    //console.log(response);
                    if (response.status != "success") {
                        console.warn("Error in sending data");
                        return;
                    }
                    
                    alert("Your request has been submitted, thank you");
                    location.reload();
                });
                 
                request.fail(function( jqXHR, textStatus ) {

                });
                
                request.always(function() {
                });
            }, startListening: function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            var captchaWidgetID = null;

	            var captchaLoadedHandle = topic.subscribe("captcha.loaded", function(){
	            	captchaWidgetID = grecaptcha.render(context.captchaNode, {'sitekey':'6Le7igETAAAAABosH8mEAL880nDNlTZ5UbyDGxhe'});
	            	
	            	//console.log("Captcha created", captchaWidgetID);
	            	context.initInteraction(captchaWidgetID);
				});
				topic.publish("topic.handlers.add", captchaLoadedHandle, domAttr.get(domNode, "id"));
            }
        });

    });