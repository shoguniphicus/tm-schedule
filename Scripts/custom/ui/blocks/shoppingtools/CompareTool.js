define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/comparetool.html",
        "js/custom/ui/cell/CompareCell",
        "js/custom/ui/cell/CompareSpec",
        "js/custom/utils/URLparam",
        "js/custom/utils/DynamicVendor"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, CompareCell, CompareSpec, URLparam, DynamicVendor) {
        return declare("js.custom.ui.blocks.shoppingtools.CompareTool", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function() {
                this.inherited(arguments);
                
                var context = this;
                context.setupContent();
                context.startListening();
            },destroy: function() {
            	var domNode = this.domNode;
            	topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            	
                this.inherited(arguments);
            },  startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
                
            }, initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
	            var context = this;
	            
	        }, startListening: function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            var resetColumnHandle = topic.subscribe("compare.tool.reset.index", function(_index){
	            	switch(_index){
		            	case 1:
		            		domConstruct.empty(context.column1Node);
		            		var compareCell = new CompareCell().placeAt(context.column1Node);
							compareCell.construct(null,1); 
		            	break;
		            	
		            	case 2:	
		            		domConstruct.empty(context.column2Node);
		            		var compareCell = new CompareCell().placeAt(context.column2Node);
							compareCell.construct(null, 2); 
		            	break;
		            	
		            	case 3:
		            		domConstruct.empty(context.column3Node);
		            		var compareCell = new CompareCell().placeAt(context.column3Node);
							compareCell.construct(null, 3); 
		            	break;
		            	
		            	default:
		            	break;
	            	}
				});
				topic.publish("topic.handlers.add", resetColumnHandle, domAttr.get(domNode, "id"));
				
				var owlHandle = topic.subscribe("vendor.script.loaded.owl.carousel.min", function(){
					context.initOwl();
				});
				topic.publish("topic.handlers.add", owlHandle, domAttr.get(domNode, "id"));
            }, setupContent:function(){
	            var context = this;
	            
	            /*
var params = new URLparam().getUrlParams();
				var c1 = params['c1'];
				var c2 = params['c2'];
				var c3 = params['c3'];
				
*/
				var setting = $.jStorage.get("tools.compare.setting");
	            
	            var c1 = "";
	            var c2 = "";
	            var c3 = "";
	            
	            if(setting){
		            c1 = setting.c1;
		            c2 = setting.c2;
		            c3 = setting.c3;
	            }
	            $.jStorage.deleteKey("tools.compare.setting");
	            
	            var compareSpec = CompareSpec().placeAt(context.specListNode);
	            compareSpec.construct();
	            
	            if(typeof c1 !== "undefined"){
		            var compareCell = new CompareCell().placeAt(context.column1Node);
		            compareCell.construct(c1, 1);
	            }else{
		           var compareCell = new CompareCell().placeAt(context.column1Node);
		           compareCell.construct(null, 1); 
	            }
	            
	            if(typeof c2 !== "undefined"){
		            var compareCell = new CompareCell().placeAt(context.column2Node);
		            compareCell.construct(c2,2);
	            }else{
		             var compareCell = new CompareCell().placeAt(context.column2Node);
		            compareCell.construct(null,2);
	            }
	            
	            if(typeof c3 !== "undefined"){
		            var compareCell = new CompareCell().placeAt(context.column3Node);
		            compareCell.construct(c3, 3);
	            }else{
		            var compareCell = new CompareCell().placeAt(context.column3Node);
		            compareCell.construct(null, 3);
	            }
	            
	            context.setupOwl();
            }, setupOwl:function(){
	            var context = this;
	            
	            var owlCarouselCSS = new DynamicVendor().inject("owl.carousel", "css");
	            var owlThemeCSS = new DynamicVendor().inject("owl.theme", "css");
	            var owlTransitionCSS = new DynamicVendor().inject("owl.transitions", "css");
	            
	            var owlCarouselJS = new DynamicVendor().inject("owl.carousel.min", "js");
            }, initOwl:function(){
	            var context = this;
	            
	            var owl = $(context.listContainerNode);
				owl.owlCarousel({
			      navigation : false, // Show next and prev buttons,
			      lazyLoad: true, 
			      slideSpeed : 300,
			      paginationSpeed : 400,
			      pagination: false,
			      items : 3, 
			      itemsDesktop : [1199,3],
				  itemsDesktopSmall : [979,2],
				  itemsTablet: [768,1],
				  itemsMobile: [479,1],
				  afterMove: function(){
				  }
			  });
            }
        });

    });