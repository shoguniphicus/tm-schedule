define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/compare.html"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template) {
        return declare("js.custom.ui.blocks.shoppingtools.Compare", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function() {
                this.inherited(arguments);
                var context = this;
                
                context.setupContent();
                context.startListening();
                context.initInteraction();
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, setupContent:function(){
	            var context = this;
	            
	            domAttr.set(context.decoImageNode, "src", config.assetURL+"img/shoppingtools/deco.png");
            }, initInteraction:function(){
	            var context = this;
	            var alltruckdata = null;
	            
	            function makeitSelectable(){
                    $(context.select1Node).change(function() {
                        var idSelected = $(this).val();
                        
                        if(idSelected == ""){
	                        $(context.selectV1Node).prop('disabled', 'disabled');
                        }else{
                        	$(context.selectV1Node).prop('disabled', 'disabled');
                        	getVariant(context.selectV1Node, idSelected);
	                        //$(context.selectV1Node).prop('disabled', false);
                        }
                    });
                    
                    $(context.select2Node).change(function() {
                        var idSelected = $(this).val();
                        
                        if(idSelected == ""){
	                        $(context.selectV2Node).prop('disabled', 'disabled');
                        }else{
                        	$(context.selectV2Node).prop('disabled', 'disabled');
                        	getVariant(context.selectV2Node, idSelected);
                        	//$(context.selectV2Node).prop('disabled', false);
                        }
                    });
                    
                    $(context.select3Node).change(function() {
                        var idSelected = $(this).val();
                        
                        if(idSelected == ""){
	                        $(context.selectV3Node).prop('disabled', 'disabled');
                        }else{
                        	$(context.selectV3Node).prop('disabled', 'disabled');
                        	getVariant(context.selectV3Node, idSelected);
                        }
                    });
                }
	            
	            getModelType();
	            function getModelType(){
		            var request = $.ajax({
	                  url: config.hostURL+"api/car/get_model_type",
	                  type: "GET",
	                  cache: false,
	                  dataType: "json"
	                });
	                 
	                request.done(function( response ) {
	                    if (response.status != "success") {
	                        console.warn("Error retrieving data");
	                        return;
	                    }
	                    
	                    alltruckdata = response.data;
	                    array.forEach(response.data, function(single, index){
	                        var optionContainer = context.selectNode;
	                        var singleOption = "<option value='"+single.key+"'>"+single.value+"</option>";
	                        
	                        $(context.select1Node).append(singleOption);
	                        $(context.select2Node).append(singleOption);
	                        $(context.select3Node).append(singleOption);
	                    });
	                    
	                    makeitSelectable();
	                });
	                 
	                request.fail(function( jqXHR, textStatus ) {
	                    console.log("Fail to start up , please refresh your browser");
	                });
	                
	                request.always(function() {
	                });
	            }
	            
	            function getVariant(_el, _value){
	            	domConstruct.empty(_el);
	            	$(_el).append('<option value="">Please Select</option>');
	            	
		            var request = $.ajax({
	                  url: config.hostURL+"api/car/get_same_series?series="+_value,
	                  type: "GET",
	                  cache: true,
	                  dataType: "json"
	                });
	                 
	                request.done(function( response ) {
	                    if (response.status != "success") {
	                        console.warn("Error retrieving data");
	                        return;
	                    }
	                    
	                    alltruckdata = response.data;
	                    array.forEach(response.data, function(single, index){
	                        var optionContainer = context.selectNode;
	                        var singleOption = "<option value='"+single.id+"'>"+single.car_model_name+"</option>";
	                        
	                        $(_el).append(singleOption);
	                    });
	                    
	                    $(_el).prop('disabled', false);
	                });
	                 
	                request.fail(function( jqXHR, textStatus ) {
	                });
	                
	                request.always(function() {
	                });
	            }
	                            
                on(context.compareNowNode, "click", function(){
                	var setting = new Object();
                	setting.c1 = $(context.selectV1Node).val();
                	setting.c2 = $(context.selectV2Node).val();
                	setting.c3 = $(context.selectV3Node).val();
                	
                	$.jStorage.set("tools.compare.setting", setting);
	                window.location.href = config.hostURL+"tools/compare";
                });
	        }, startListening: function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            /*
var wallpaperHandle = topic.subscribe("home.change.wallpaper", function(_bg){
	            	$(context.wallpaperNode).removeClass().addClass("wrapper "+_bg);
				});
				topic.publish("topic.handlers.add", wallpaperHandle, domAttr.get(domNode, "id"));
*/
            }
        });

    });