define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/calculator.html"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template) {
        return declare("js.custom.ui.blocks.shoppingtools.Calculator", [BaseBlock, TemplatedMixin], {
            templateString: template,
            alltruckdata: null,
            construct: function() {
                this.inherited(arguments);
                var context = this;
                
                domAttr.set(context.calculatorImageNode, "src", config.assetURL+"img/shoppingtools/calculator.jpg");
                
                context.startListening();
                context.initInteraction();
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;  
                
                alltruckdata = null;  
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
	            var context = this;

                var currentPrice = 0;
                var currentTruck = null;
                var region = "Peninsular";
                var typeOfOwnership = "Private";
                var truckSelected = null;
                var idSelected = null;
                
                var request = $.ajax({
                  url: config.hostURL+"api/car/get_all?testdrive=yes",
                  type: "GET",
                  cache: false,
                  dataType: "json"
                });
                 
                request.done(function( response ) {
                    if (response.status != "success") {
                        console.warn("Error retrieving data");
                        return;
                    }
                    
                    alltruckdata = response.data;
                    
                    array.forEach(response.data, function(single, index){
                        var optionContainer = context.modelNode;
                        var singleOption = "<option value='"+single.id+"'>"+single.car_model_name+"</option>";
                        
                        if(single.available_for_booking == "1"){
	                        $(optionContainer).append(singleOption);
                        }
                       		
                    });
                    
                    makeitSelectable();
                });
                 
                request.fail(function( jqXHR, textStatus ) {
                    console.log("Fail to start up , please refresh your browser");
                });
                
                request.always(function() {
                });
                
                function makeitSelectable(){
                    $(context.modelNode).change(function() {
                        idSelected = $(this).val();
                        currentTruck = idSelected;
                        getprice();
                    });
                }
                
                $(context.ownershipNode).change(function() {
                    typeOfOwnership = $(this).val();
                    
                    getprice();
                });
                
                $(context.regionNode).change(function() {
                    region = $(this).val();
                    
                    getprice();
                });
                
                function getprice(){
                	var truck = _.findWhere(alltruckdata, {"id":currentTruck});
                	var colors = JSON.parse(truck.colors_with_price_modifier);
					
					domConstruct.empty(context.paintNode);
					array.forEach(colors, function(single, index){
						var singleOption = "<option value='"+single.modifier+"'>"+single.display_name+"</option>";
						$(context.paintNode).append(singleOption);
					})
					
	                var request = $.ajax({
	                  url: config.hostURL+"api/price/getprice/?car_id="+currentTruck+"&region="+region+"&type="+typeOfOwnership,
	                  type: "GET",
	                  cache: false,
	                  dataType: "json"
	                });
	                 
	                request.done(function( response ) {
	                	console.log("Response", response);
	                	
	                	if(response.status != "success"){
	                		$(context.priceNode).val("N/A");
		                	return;
	                	}
	                	
	                	var responseData = response.data;
	                	
	                	var price = parseFloat(responseData.price);
	                	var inspection_fee = parseFloat(responseData.inspection_fee);
	                	var insurance = parseFloat(responseData.insurance);
						var number_plate = parseFloat(responseData.number_plate);
						var ownership_claim = parseFloat(responseData.ownership_claim);
						var registration_fee = parseFloat(responseData.registration_fee);
						var road_tax = parseFloat(responseData.road_tax);
						var signwritting = parseFloat(responseData.signwritting);
	                	var handling_fee = parseFloat(responseData.handling_fee);
	                	
	                	currentPrice = price+inspection_fee+number_plate+signwritting+handling_fee;
	                	currentPrice *= 1.06;
	                	currentPrice += ownership_claim+registration_fee+road_tax+insurance;
	                	
	                	$(context.priceNode).val(currentPrice.toFixed(2));
	                });
	                 
	                request.fail(function( jqXHR, textStatus ) {
	
	                });
	                
	                request.always(function() {
	                });
                }
                
                
                $.validator.addMethod(
                    "mindownpayment",
                    function(value, element) {
                        // return value.match(/(\d{4})-(\d{2})-(\d{2})/);
                        var price = $(context.priceNode).val();
                        console.log(value >= price * 0.1);
                        return value >= price * 0.1;
                    }     
                );
                
                $(context.loanCalculatorNode).validate({    
                    rules: {
                        model: {
                            required: true
                        },
                        ownership: {
                            required: true
                        },
                        paint: {
                            required: true
                        },
                        region: {
                            required: true
                        },
                        downpayment: {
                            mindownpayment: true,
                            number: true,
                            required: true
                        },
                        interest: {
                            number: true,
                            required: true
                        }
                    },
                    messages: {
                        downpayment: {
                            mindownpayment: "Down payment must be at least 10% of price",
                            required: "Please enter a valid amount"
                        },
                        interest: "Please enter a valid amount"
                    },submitHandler: function(form) {	
                    	context.calculateMonthly();
                    }
                });
                
                function makePaints(data){
	                console.log(data);
                }
            }, calculateMonthly:function(){
	            var context = this;
	            var total = $(context.priceNode).val();
	            var interest = $(context.interestNode).val();
	            var period = $(context.periodNode).val();
	            var downpayment = $(context.downpaymentNode).val();
	            
				var amount = total - downpayment;
				var months = period * 12;
				var interestamount = interest*amount*period*0.01;
				var treeamount = interestamount + amount;
				var installment = (treeamount/months).toFixed(2);
				
				html.set(context.loanPaymentMonthlyNode, installment.toString());
/* 				html.set(context.monthlyAmountNode, ""+installment); */
            }, startListening: function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            /*
var wallpaperHandle = topic.subscribe("home.change.wallpaper", function(_bg){
	            	$(context.wallpaperNode).removeClass().addClass("wrapper "+_bg);
				});
				topic.publish("topic.handlers.add", wallpaperHandle, domAttr.get(domNode, "id"));
*/
            }
        });

    });