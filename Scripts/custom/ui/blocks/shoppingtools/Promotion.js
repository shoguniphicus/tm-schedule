define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/promotion.html",
        "js/custom/ui/blocks/carousel/SuperPromotion"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, CarouselSuperPromotion) {
        return declare("js.custom.ui.blocks.shoppingtools.Promotion", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function() {
                this.inherited(arguments);
                var context = this;
                
                context.setupContent();
                context.startListening();
                context.initInteraction();
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, setupContent:function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            var request = $.ajax({
        		  url: config.hostURL+"api/promotion/index",
        		  type: "GET",
        		  cache: false,
        		  dataType: "json"
        		});
        		 
        		request.done(function( response ) {
        			if (response.status != "success") {
        				console.warn("Error retrieving data");
        				return;
        			}
        
					var carouselSuperPromotion = new CarouselSuperPromotion().placeAt(context.owlCarouselNode);
		            carouselSuperPromotion.construct(response.data);
        		});
        		 
        		request.fail(function( jqXHR, textStatus ) {
        		});
        		
        		request.always(function() {
        		});
            }, initInteraction:function(){
	            var context = this;
	            
	        }, startListening: function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            /*
var wallpaperHandle = topic.subscribe("home.change.wallpaper", function(_bg){
	            	$(context.wallpaperNode).removeClass().addClass("wrapper "+_bg);
				});
				topic.publish("topic.handlers.add", wallpaperHandle, domAttr.get(domNode, "id"));
*/
            }
        });

    });