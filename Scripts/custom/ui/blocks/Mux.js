define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Mux.html"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template) {
        return declare("js.custom.ui.blocks.Mux", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function() {
                this.inherited(arguments);
                var context = this;
                
                context.setupContent();
                context.startListening();
                context.initInteraction();
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, startListening: function(){
	            var context = this;
	            var domNode = this.domNode;
	            
            }, setupContent:function(){
	            var context = this;
            }, initInteraction:function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            on(domNode, "click", function(){
	            	$.jStorage.set("iframe.modal", config.assetURL+"muxvr/webapp.html?html5=fallback");
	            	topic.publish("modal.page", "iframe");
	            });
            }
        });

    });