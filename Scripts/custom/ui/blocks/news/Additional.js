define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Additional.html",
        "js/custom/ui/cell/NewsCell"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, NewsCell) {
        return declare("js.custom.ui.blocks.news.additional", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function() {
                var context = this;
                
                context.setupContent();
                context.startListening();
                context.initInteraction();
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, setupContent:function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            var request = $.ajax({
        		  url: config.hostURL+"api/news/random",
        		  type: "GET",
        		  cache: false,
        		  dataType: "json"
        		});
        		 
        		request.done(function( response ) {
        			if (response.status != "success") {
        				console.warn("Error retrieving data");
        				return;
        			}
        			
        			array.forEach(response.data, function(single, index){
	        			var newsCell = new NewsCell().placeAt(domNode);
	        			newsCell.construct(single);
        			});
        		});
        		 
        		request.fail(function( jqXHR, textStatus ) {
        		});
        		
        		request.always(function() {
        		});
            }, initInteraction:function(){
	            var context = this;
	            
	        }, startListening: function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            /*
var wallpaperHandle = topic.subscribe("home.change.wallpaper", function(_bg){
	            	$(context.wallpaperNode).removeClass().addClass("wrapper "+_bg);
				});
				topic.publish("topic.handlers.add", wallpaperHandle, domAttr.get(domNode, "id"));
*/
            }
        });

    });