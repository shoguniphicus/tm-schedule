define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Sidebar.html",
        "js/custom/ui/cell/NewsYearCell",
        "js/custom/ui/cell/NewsPlainCell"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, NewsYearCell, NewsPlainCell) {
        return declare("js.custom.ui.blocks.news.Sidebar", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function() {
                var context = this;
                
                var thisDate = new Date();
                var thisYear = thisDate.getFullYear();
                
                context.setupContent(thisYear);
                context.startListening();
                context.initInteraction();
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
                this.inherited(arguments);
                
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, setupContent:function(thisYear){
                var context = this;
                var domNode = this.domNode;
                var refthisYear = thisYear;
                
                var thisDate = new Date();
                
                if(thisDate.getFullYear() == thisYear){
                    domConstruct.empty(context.yearListNode);
                    for(var i=0;i<3;i++){
                        var newsYearCell = new NewsYearCell().placeAt(context.yearListNode);
                        newsYearCell.construct(refthisYear, i);
                        
                        refthisYear--;
                    }
                }
                
                var request = $.ajax({
                  url: config.hostURL+"api/news/index?year="+thisYear,
                  type: "GET",
                  cache: false,
                  dataType: "json"
                });
                 
                request.done(function( response ) {
                    if (response.status != "success") {
                        console.warn("Error retrieving data");
                        return;
                    }
                    
                    array.forEach(response.data, function(single, index){
                        var newsPlainCell = new NewsPlainCell().placeAt(context.listContainerNode);
                        newsPlainCell.construct(single);
                    });
                });
                 
                request.fail(function( jqXHR, textStatus ) {
                });
                
                request.always(function() {
                });
            }, initInteraction:function(){
                var context = this;
                
            }, startListening: function(){
                var context = this;
                var domNode = this.domNode;
                
                
                var yearHandle = topic.subscribe("news.sidebar.change.year", function(_year){
                    console.log("news.sidebar.change.year", _year);
                    domConstruct.empty(context.listContainerNode);
                    context.setupContent(_year);
                });
                topic.publish("topic.handlers.add", yearHandle, domAttr.get(domNode, "id"));
            }
        });

    });