define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Accessories.html",
        "js/custom/ui/cell/AccessoriesCell"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, AccessoriesCell) {
        return declare("js.custom.ui.blocks.Accessories", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function() {
                this.inherited(arguments);
                var context = this;
                
                context.setupContent();
                context.startListening();
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, startListening: function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            /*
var wallpaperHandle = topic.subscribe("home.change.wallpaper", function(_bg){
	            	$(context.wallpaperNode).removeClass().addClass("wrapper "+_bg);
				});
				topic.publish("topic.handlers.add", wallpaperHandle, domAttr.get(domNode, "id"));
*/
            }, setupContent:function(){
	            var context = this;
	            
	            var request = $.ajax({
        		  url: config.hostURL+"api/accessories/get_all_accessories",
        		  type: "GET",
        		  cache: true,
        		  dataType: "json"
        		});
        		 
        		request.done(function( response ) {
        			if(response.status == "success"){
	        			array.forEach(response.data, function(single, index){
		        			var accessoriesCell = new AccessoriesCell().placeAt(context.listContainerNode);
		        			accessoriesCell.construct(single);
	        			});
        			}
        		});
        		 
        		request.fail(function( jqXHR, textStatus ) {
        		});
        		
        		request.always(function() {
        		});
            }
        });

    });