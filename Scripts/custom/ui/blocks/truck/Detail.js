define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Detail.html",
        "js/custom/ui/blocks/truck/Feature",
        "js/custom/ui/blocks/truck/Spec",
        "js/custom/ui/blocks/truck/Interested"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, Feature, Spec, Interested) {
        return declare("js.custom.ui.blocks.truck.Detail", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function(truckid, series) {
                var context = this;
                
                context.sectionization(truckid, series);
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
                
                context.startListening();
                context.initInteraction();
				
				var params = new URLparam().getUrlParams();
				var inner = params['m'];
            },initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            },initInteraction:function(){
	            var context = this;
            },startListening:function(){
	            var context = this;
            },sectionization:function(truckid, series){
            	var context = this;
            	var scrollSpyArray = new Array();
				
				var scrollSpySetting = new Object();
				scrollSpySetting.id = "feature";
				scrollSpySetting.section = "features";
				
	            switch(truckid){
                    /*
case "14":
                    	scrollSpySetting.active = true;
                    	scrollSpyArray.push(scrollSpySetting);
                    	scrollSpyArray.push(context.makeSection(truckid, "performance", "double"));
		            	scrollSpyArray.push(context.makeSection(truckid, "interior", "double"));
		            	scrollSpyArray.push(context.makeSection(truckid, "safety", "thumbstyle-copy"));
		            	scrollSpyArray.push(context.makeSpec(truckid, series, "specs", "spec-price"));
                    break;
*/
                    
                    case "14":
                    case "15":
                    case "16":
                    	scrollSpySetting.active = true;
                    	scrollSpyArray.push(scrollSpySetting);
                    	scrollSpyArray.push(context.makeSection(truckid, "durability", "double"));
                    	scrollSpyArray.push(context.makeSection(truckid, "performance", "double"));
		            	scrollSpyArray.push(context.makeSection(truckid, "exterior", "thumbstyle"));
		            	scrollSpyArray.push(context.makeSection(truckid, "interior", "double"));
		            	scrollSpyArray.push(context.makeSection(truckid, "safety", "thumbstyle-copy"));
		            	scrollSpyArray.push(context.makeSpec(truckid, series, "specs", "spec-price"));
                    break;
                    
                    case "17":
                    case "18":
                    case "19":
                    case "20":
                    case "21":
                    	scrollSpySetting.active = true;
                    	scrollSpyArray.push(scrollSpySetting);
                    	scrollSpyArray.push(context.makeSection(truckid, "performance", "double"));
		            	scrollSpyArray.push(context.makeSection(truckid, "exterior", "thumbstyle"));
		            	scrollSpyArray.push(context.makeSection(truckid, "interior", "double"));
		            	scrollSpyArray.push(context.makeSection(truckid, "safety", "thumbstyle-copy"));
		            	scrollSpyArray.push(context.makeSpec(truckid, series, "specs", "spec-price"));
                    break;
                    
                    case "22":
                    case "23":
                    	scrollSpySetting.active = true;
                    	scrollSpyArray.push(scrollSpySetting);
                    	scrollSpyArray.push(context.makeSection(truckid, "performance", "double"));
		            	scrollSpyArray.push(context.makeSection(truckid, "safety", "thumbstyle-copy"));
		            	scrollSpyArray.push(context.makeSpec(truckid, series, "specs", "spec-price"));
                    break;
                    
                    case "26":
                    case "27":
                    	scrollSpyArray.push({"id":"vr", "section":"MU-X VR", "active": true});
/*                     	scrollSpySetting.active = true; */
                    	scrollSpyArray.push(scrollSpySetting);
                    	scrollSpyArray.push(context.makeSection(truckid, "performance", "thumbstyle-copy"));
		            	scrollSpyArray.push(context.makeSection(truckid, "exterior", "thumbstyle"));
		            	//scrollSpyArray.push(context.makeSection(truckid, "exterior", "freeform:thumbstyle"));
		            	scrollSpyArray.push(context.makeSection(truckid, "interior", "single"));
		            	scrollSpyArray.push(context.makeSection(truckid, "safety", "double"));
		            	scrollSpyArray.push(context.makeSpec(truckid, series, "specs", "spec-price"));
						$(".bigass-title").addClass('dincond-bold');
					default:
					break;
	            }
	            
	            context.makeInterested(truckid, "Interested", "interested");
	            
	            topic.publish("scrollspy.nav.id.init", scrollSpyArray);
            }, makeSection:function(truckid, section, sectiontype){
	            var context = this;
	            var domNode = this.domNode
	            
	            var setting = new Object();
	            setting.truckid = truckid;
	            setting.section = section;
	            setting.sectiontype = sectiontype;
	            
				var featureID = new Feature().placeAt(domNode).construct(setting);
				
				var scrollSpySetting = new Object();
				scrollSpySetting.id = section;
				scrollSpySetting.section = section;
				
				return scrollSpySetting;
            }, makeSpec:function(truckid, series, section, sectiontype){
	            var context = this;
	            var domNode = this.domNode;
	            
	             var setting = new Object();
	            setting.truckid = truckid;
	            setting.section = section;
	            setting.series = series;
	            setting.sectiontype = sectiontype;
	            
	            var specSectionID = new Spec().placeAt(domNode).construct(setting);
	            
	            var scrollSpySetting = new Object();
				scrollSpySetting.id = section;
				scrollSpySetting.section = section;
				
				return scrollSpySetting;
            }, makeInterested:function(truckid, section, sectiontype){
	            var context = this;
	            var domNode = this.domNode
	            
	            var setting = new Object();
	            setting.truckid = truckid;
	            setting.section = section;
	            setting.sectiontype = sectiontype;
	            
				var interestedSectionID = new Interested().placeAt(domNode).construct(setting);
				
				var scrollSpySetting = new Object();
				scrollSpySetting.id = section;
				scrollSpySetting.section = section;
				
				return scrollSpySetting;
            }
        });

    });