define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Feature.html",
        "js/custom/ui/blocks/carousel/Single",
        "js/custom/ui/blocks/carousel/DoubleWithNav",
        "js/custom/ui/blocks/carousel/Thumb",
        "js/custom/ui/blocks/carousel/ThumbCopy"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, CarouselSingle, CarouselDouble, CarouselThumb, CarouselThumbCopy) {
        return declare("js.custom.ui.blocks.truck.Feature", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
                var context = this;
                var domNode = this.domNode;
                
                context.setupContent(setting);
                
                domAttr.set(domNode, "id", setting.section);
                //return domAttr.get(domNode, "id");
            }, postCreate: function postCreate() {
                this.inherited(arguments);
            }, startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
                
                context.startListening();
                context.initInteraction();
				
				var params = new URLparam().getUrlParams();
				var inner = params['m'];
            }, initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
	            var context = this;
            }, startListening:function(){
	            var context = this;
            }, getFeature:function(setting){
            	var context = this;
            	
            	var sectionName = setting.section.toLowerCase();
            	var category = _.findWhere(dojoConfig.featureCategoryData, {feature_name:sectionName});
            	//console.log(sectionName, dojoConfig.featureCategoryData);
            	var request = $.ajax({
    			  url: config.hostURL+"api/feature/get_features?carid="+setting.truckid+"&catid="+category.id,
    			  type: "GET",
    			  cache: false,
    			  dataType: "json"
    			});
    			 
    			request.done(function( response ) {
    				if (response.status != "success") {
    					//console.warn("Error retrieving data");
    					console.warn("No data on Section:"+sectionName, "Terminate this section");
    					context.destroy();
    					return;
    				}
    				
		            switch(setting.sectiontype){
			            case "double":
			            	var carouselDouble = new CarouselDouble().placeAt(context.listContainerNode);
			            	carouselDouble.construct(response.data);
			            break;
			            
			            case "single":
			            	var carouselSingle = new CarouselSingle().placeAt(context.listContainerNode);
			            	carouselSingle.construct(response.data);
			            break;
			            
			            case "freeform":
			            break;
			            
			            case "freeform:thumbstyle":
			            	var filtered = _.findWhere(response.data, {freeform_enabled: "1"});
			            	var listFiltered = _.where(response.data, {freeform_enabled: "0"});
			            	
			            	console.log("filtered", filtered);
			            	console.log("listFiltered", listFiltered);
			            	
			            	html.set(context.freeformContainerNode, filtered.freeform);
			            	
			            	//var carouselDouble = new CarouselDouble().placeAt(context.listContainerNode);
							//carouselDouble.construct(listFiltered);
							
							var carouselThumb = new CarouselThumb().placeAt(context.listContainerNode);
			            	carouselThumb.construct(listFiltered);
							
							$(context.freeformContainerNode).addClass("hidden-xs visible-sm visible-md visible-lg");
							$(context.listContainerNode).addClass("hidden-sm visible-xs");
			            break;	
			            
			            case "thumbstyle":
			            	var carouselThumb = new CarouselThumb().placeAt(context.listContainerNode);
			            	carouselThumb.construct(response.data);
			            break;
			            
			            case "thumbstyle-copy":
			            	var carouselThumbCopy = new CarouselThumbCopy().placeAt(context.listContainerNode);
			            	carouselThumbCopy.construct(response.data);
			            break;
			            
			            default:
			            break;
		            }
    			});
    			 
    			request.fail(function( jqXHR, textStatus ) {
    				var errorMessage = JSON.parse(jqXHR.responseText);
					var error = errorMessage.error.error_message;
					
					console.warn("Fail!", error);
    			});
    			
    			request.always(function() {
    			});
            }, setupContent:function(setting){
	            var context = this;
	            var domNode = this.domNode;
	            
	            switch(setting.section){
	            	case "durability":
		            	var featureSetting = _.findWhere(dojoConfig.featureSettingData, {section_id:'3'});
		            	
		            	html.set(context.sectionTitleNode, "DURABILITY");
		            	html.set(context.titleNode, featureSetting.section_bigass_title);
		            	html.set(context.sectionDesNode, featureSetting.section_des);
		            	
		            	domClass.add(domNode, featureSetting.background_class);
		            	context.getFeature(setting);
		            break;
	            
		            case "performance":
		            	var featureSetting = _.findWhere(dojoConfig.featureSettingData, {section_id:'1'});
		            	
		            	html.set(context.sectionTitleNode, "PERFORMANCE");
		            	html.set(context.titleNode, featureSetting.section_bigass_title);
		            	html.set(context.sectionDesNode, featureSetting.section_des);
		            	
		            	domClass.add(domNode, featureSetting.background_class);
		            	context.getFeature(setting);
		            break;
		            
		            case "exterior":
		            	var featureSetting = _.findWhere(dojoConfig.featureSettingData, {section_id:'5'});
		            	
		            	html.set(context.sectionTitleNode, "EXTERIOR");
		            	html.set(context.titleNode, featureSetting.section_bigass_title);
		            	html.set(context.sectionDesNode, featureSetting.section_des);
		            	
		            	domClass.add(domNode, featureSetting.background_class);
		            	context.getFeature(setting);
		            break;
		            
		            case "interior":
		            	var featureSetting = _.findWhere(dojoConfig.featureSettingData, {section_id:'6'});
		            
		            	html.set(context.sectionTitleNode, "INTERIOR");
		            	html.set(context.titleNode, featureSetting.section_bigass_title);
		            	html.set(context.sectionDesNode, featureSetting.section_des);
		            	
		            	domClass.add(domNode, featureSetting.background_class);
		            	context.getFeature(setting);
		            break;
		            
		            case "safety":
		            	var featureSetting = _.findWhere(dojoConfig.featureSettingData, {section_id:'4'});
		            
		            	html.set(context.sectionTitleNode, "SAFETY");
		            	html.set(context.titleNode, featureSetting.section_bigass_title);
		            	html.set(context.sectionDesNode, featureSetting.section_des);
		            	
		            	domClass.add(domNode, featureSetting.background_class);
		            	context.getFeature(setting);
		            break;
		            
		            default:
		            	html.set(context.titleNode, "");
		            break;
	            }
            }
        });

    });