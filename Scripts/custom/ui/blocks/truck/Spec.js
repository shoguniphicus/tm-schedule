define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Spec.html",
        "js/custom/ui/cell/SpecCell",
        "js/custom/ui/cell/PriceCell",
        "js/custom/utils/URLparam",
        "js/custom/utils/DynamicVendor"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, SpecCell, PriceCell, URLparam, DynamicVendor) {
        return declare("js.custom.ui.blocks.truck.Spec", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
                var context = this;
                var domNode = this.domNode;
                
                context.setupContent(setting);
                context.startListening();
                context.initInteraction();
                
                domAttr.set(domNode, "id", setting.section);
                //return domAttr.get(domNode, "id");
            },destroy: function() {
                var domNode = this.domNode;
                topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
                
                this.inherited(arguments);
            }, startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            }, initNavigationTransition:function(){
                this.inherited(arguments);
                
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
                var context = this;
                var domNode = this.domNode;
                
                on(context.specTriggerNode, "click", function(){
                    domStyle.set(context.specContainerNode, "display", "block");
                    domStyle.set(context.priceContainerNode, "display", "none");

                    resetActive();
                    domClass.add(context.specTriggerNode, "active");

                    setTimeout(function(){
                        topic.publish("speccell.added");
                    }, 50);
                });
                
                on(context.priceTriggerNode, "click", function(){
                    domStyle.set(context.specContainerNode, "display", "none");
                    domStyle.set(context.priceContainerNode, "display", "block");
                    
                    resetActive();
                    domClass.add(context.priceTriggerNode, "active");
					
					setTimeout(function(){
						topic.publish("speccell.added");
					}, 50);
					
                    /*
setTimeout(function(){
                        var owl = $(context.listPriceContainerNode);
                        owl.owlCarousel({
                          navigation : false, // Show next and prev buttons,
                          lazyLoad: false, 
                          slideSpeed : 300,
                          paginationSpeed : 400,
                          pagination: false,
                          items : 3, 
                          itemsDesktop : [1199,3],
                          itemsDesktopSmall : [979,2],
                          itemsTablet: [768,1],
                          itemsMobile: [479,1],
                          afterMove: function(){
                          }
                      });

                        topic.publish("speccell.added");
                    }, 50);
*/
                });

                function resetActive(){
                    if(domClass.contains(context.priceTriggerNode, "active"))
                        domClass.remove(context.priceTriggerNode, "active");

                    if(domClass.contains(context.specTriggerNode, "active"))
                        domClass.remove(context.specTriggerNode, "active");
                }
                
                on(context.viewFullNode, "click", function(){
                    var isExpanded = domAttr.get(context.viewFullNode, "data-explode");
                    
                    if(isExpanded == "true"){
                        html.set(context.viewFullNode, "VIEW FULL VERSION");
                        domAttr.set(context.viewFullNode, "data-explode", "false");
                        topic.publish("spec.implode");
                    }else{
                        html.set(context.viewFullNode, "CLOSE FULL VERSION");
                        domAttr.set(context.viewFullNode, "data-explode", "true");
                        topic.publish("spec.expand");   
                    }
                });
                
                on(context.compareNode, "click", function(){
                    location.href = config.hostURL+"tools/compare";
                });
                
                
                on(context.download01Node, "click", function(){
                    var downloadLink = $(".quicklink-download-link").attr("href");
                    window.open(downloadLink, "_blank");
                });
                
                on(context.downloadNode, "click", function(){
                    var downloadLink = $(".quicklink-download-link").attr("href");
                    window.open(downloadLink, "_blank");
                });
            }, startListening:function(){
                var context = this;
                var domNode = this.domNode;
                
                var resizeHandle = topic.subscribe("update.window.resize", resizeSpecColumn);
                var specAddedHandle = topic.subscribe("speccell.added", resizeSpecColumn);
                
                function resizeSpecColumn(){
                    var allSpecArray = new Array(".truck-name", ".header-features", ".ts-trans", ".ts-engine_model", ".ts-engine_type", ".ts-max_output", ".ts-max_torque", ".ts-tyre", ".ts-wheels", ".header-engine", ".sp-engine_model", ".sp-engine_type", ".sp-displacement", ".sp-bore", ".sp-compress_ratio", ".sp-max_output", ".sp-max_torque", ".sp-fuel_system", ".header-transmission", ".sp-trans_type", ".sp-trans_gear_1", ".sp-trans_gear_2", ".sp-trans_gear_3", ".sp-trans_gear_4", ".sp-trans_gear_5", ".sp-trans_gear_reverse", ".sp-trans_gear_final", ".header-suspension_brake", ".sp-sus_front", ".sp-sus_rear", ".sp-shock", ".sp-brake_front", ".sp-brake_rear", ".header-steering", ".sp-steering_system", ".sp-min_turning_radius", ".header-tyres_wheels", ".sp-tyres", ".sp-wheels", ".header-weight_capacities", ".sp-weight", ".sp-seating", ".sp-fuel", ".header-exterior_features", ".sp-headlamp", ".sp-front_fog_lamp", ".sp-engine_hood_garnish", ".sp-front_grille", ".sp-door_outer_mirror", ".sp-door_tail_handle", ".sp-front_bumper_guard", ".sp-side_body_cladding", ".sp-side_step", ".sp-roof_rail", ".sp-cargo_cover", ".sp-bed_liner", ".sp-rear_comb_lamp", ".sp-rear_bumper", ".header-interior_features", ".sp-meter_panel", ".sp-steering_material", ".sp-dash_door_trim", ".sp-climate_control", ".sp-audio_system", ".sp-assist_grip", ".sp-power_door_lock", ".sp-seats_material", ".sp-door_window", ".sp-middle_console_box", ".sp-map_lamp", ".sp-cabin_lamp", ".sp-ash_tray", ".sp-power_outlet", ".sp-rocker_plate", ".sp-cigarette_lighter", ".sp-carpet_mat", ".header-safety", ".sp-srs", ".sp-abs_ebd", ".sp-brake_assist", ".sp-esc", ".sp-tcs", ".sp-lspv", ".sp-lsd", ".sp-body_construction", ".sp-side_door_impact_beams", ".sp-collapsibile_steering", ".sp-windscreen", ".sp-wiper", ".sp-seat_belt", ".sp-child_seat_anchorage", ".sp-security", ".sp-child_safety_lock", ".sp-high_mount_stop_lamp", ".sp-reverse_sensor", ".header-selling_price", ".ps-roadtax", ".ps-registration_fee", ".ps-ownership_claim", ".ps-handling_fee", ".ps-number_plate", ".ps-signwriting", ".ps-inspection_fee", ".ps-otwwoin", ".header-comprehensive_insurance", ".ps-otwwin", ".header-insured_amount", ".header-selling_price", ".ps-roadtax", ".ps-registration_fee", ".ps-ownership_claim", ".ps-handling_fee", ".ps-number_plate", ".ps-signwriting", ".ps-inspection_fee", ".ps-otwwoin", ".header-comprehensive_insurance", ".ps-otwwin", ".header-insured_amount", ".ps-netsellingbeforegst",".sp-muffler_cutter", ".sp-rear_spoiler", ".sp-tailgate_garnish",".sp-front_seats", ".sp-2row_seats",".sp-3row_seats", ".sp-cargo_organizer", ".sp-tonne_cover", ".sp-wiper_rear", ".sp-climate_control_rear", ".sp-rear_dvd", ".sp-command4wd");
                    
                    array.forEach(allSpecArray, function(singleClassString, index){
                        var maxHeightEl = _.max($(singleClassString), function(single){ return $(single).height(); });
                        
                        try{
                            $(singleClassString).css("min-height", $(maxHeightEl).height()+"px");
                        }catch(err){
                            //console.warn(singleClassString, err);
                        }
                    });
                }
                
                topic.publish("topic.handlers.add", resizeHandle, domAttr.get(domNode, "id"));
                topic.publish("topic.handlers.add", specAddedHandle, domAttr.get(domNode, "id"));
                
                var expandHandle = topic.subscribe("spec.expand", function(){
                	domStyle.set(context.summaryContainerNode, "display", "none");
                    domStyle.set(context.expandContainerNode, "display", "block");
                    setTimeout(function(){
                        topic.publish("speccell.added");
                    }, 100);
                });
                topic.publish("topic.handlers.add", expandHandle, domAttr.get(domNode, "id"));
                
                var implodeHandle = topic.subscribe("spec.implode", function(){
                	domStyle.set(context.summaryContainerNode, "display", "block");
                    domStyle.set(context.expandContainerNode, "display", "none");
                    
                    setTimeout(function(){
                        topic.publish("speccell.added");
                    }, 100);
                });
                topic.publish("topic.handlers.add", implodeHandle, domAttr.get(domNode, "id"));
                
            }, setupContent:function(setting){
                var context = this;
                
                switch(setting.truckid){
                    case "10":
                    case "12":
                        context.getTruck(10, 6, 0);
                        context.getTruck(12, 6, 1);
                    break;
                    
                    case "14":
                    case "15":
                    case "16":
                        context.getTruck(14, 4, 0);
                        context.getTruck(15, 4, 1);
                        context.getTruck(16, 4, 2);
                    break;
                    
                    case "17":
                    case "18":
                    case "19":
                    case "20":
                    case "21":
                        context.getTruck(17, 4, 0);
                        context.getTruck(18, 4, 1);
                        context.getTruck(21, 4, 2);
                    break;
                    
                    case "22":
                    case "23":
                        context.getTruck(22, 6, 0);
                        context.getTruck(23, 6, 1);
                    break;
                    
                    case "26":
                    case "27":
                        context.getTruck(27, 6, 0);
                        context.getTruck(26, 6, 1);
                    break;
                    
                    default:
                    break;
                }
                
                context.getTruckVariant(setting.series);
                //context.setupOwl();
            }, getTruckVariant:function(_series){
                var context = this;
                var domNode = this.domNode;
                
                var truckIDSelected = null;
                var truckNameSelected = null;
                
                var request = $.ajax({
                  url: config.hostURL+"api/car/get_same_series?series="+_series+"&filter=id,car_model_name",
                  type: "GET",
                  cache: true,
                  dataType: "json"
                });
                 
                request.done(function( response ) {
                    if (response.status != "success") {
                        console.warn("Error retrieving data");
                        return;
                    }
                    
                    var optionContainer = context.variantNode;
                    array.forEach( response.data,function(single, index){
                        var singleOption = "<option value='"+single.id+"'>"+single.car_model_name+"</option>";
                        
                        if(index == 0){
                            truckIDSelected = single.id;
                            truckNameSelected = single.car_model_name;
                            
                            context.getTruckPrice(single.id, single.car_model_name, "Peninsular", "Private", 4, 0);
                            context.getTruckPrice(single.id, single.car_model_name, "Peninsular", "Company Private",4, 1);
                            context.getTruckPrice(single.id, single.car_model_name, "Peninsular", "Company Comm",4, 2);
                        }
                        
                        $(optionContainer).append(singleOption);
                    });
                    
                    makeSelectable();
                    //Do something here
                });
                 
                request.fail(function( jqXHR, textStatus ) {
                    var errorMessage = JSON.parse(jqXHR.responseText);
                    var error = errorMessage.error.error_message;
                    console.warn("Fail!", error);
                });
                
                request.always(function() {
                });
                
                var regionSelected = "Peninsular";
                
                function makeSelectable(){
                    $(context.regionNode).change(function(){
                        var valueSelected = $(this).val();
                        
                        regionSelected = valueSelected;
                        
                        getTruck();
                    });
                    
                    $(context.variantNode).change(function(){
                        var valueSelected = $(this).val();
                        var modelName = $(this).text();
                        
                        truckIDSelected = valueSelected;
                        truckNameSelected = modelName;
                        
                        getTruck();
                    });
                }
                
                function getTruck(){
                    var owl = $(context.listPriceContainerNode);
                    
                    owl.data('owlCarousel').destroy();
                    domConstruct.empty(context.listPriceContainerNode);
                    
                    context.getTruckPrice(truckIDSelected, truckNameSelected, regionSelected, "Private", 4, 0);
                    context.getTruckPrice(truckIDSelected, truckNameSelected, regionSelected, "Company Private",4, 1);
                    context.getTruckPrice(truckIDSelected, truckNameSelected, regionSelected, "Company Comm",4, 2);
                }
            },getTruck:function(_id, _column, _index){
                var context = this;
                var domNode = this.domNode;
				
				 var specCell = new SpecCell().placeAt(context.listContainerNode);
                specCell.construct(_id, _column, _index);
				
				setTimeout(function() {
                    var owlHandle = topic.subscribe("vendor.script.loaded.owl.carousel.min", function(){
                        context.initOwl(_column);
                    });

                    topic.publish("topic.handlers.add", owlHandle, domAttr.get(domNode, "id"));

                    context.setupOwl();
                },500);
            },getTruckPrice:function(_id, _name, _region, _type, _column, _index){
                var context = this;
				
				var priceCell = new PriceCell().placeAt(context.listPriceContainerNode);
				priceCell.construct(_id, _name, _region, _type, _column, _index);
				
				if(_index == 2){
					 var owl = $(context.listPriceContainerNode);
	                    owl.owlCarousel({
	                      navigation : false, // Show next and prev buttons,
	                      lazyLoad: false, 
	                      slideSpeed : 300,
	                      paginationSpeed : 400,
	                      pagination: false,
	                      items : 3, 
	                      itemsDesktop : [1199,3],
	                      itemsDesktopSmall : [979,2],
	                      itemsTablet: [768,1],
	                      itemsMobile: [479,1],
	                      afterMove: function(){
	                      }
	                     });
				}
            }, setupOwl:function(){
                var context = this;
                
                var owlCarouselCSS = new DynamicVendor().inject("owl.carousel", "css");
                var owlThemeCSS = new DynamicVendor().inject("owl.theme", "css");
                var owlTransitionCSS = new DynamicVendor().inject("owl.transitions", "css");
                
                var owlCarouselJS = new DynamicVendor().inject("owl.carousel.min", "js");
            }, initOwl:function(_column){
                var context = this;
                var domNode = this.domNode;
                
                var alreadyOwled = domAttr.get(domNode, "owled");
                
                if(alreadyOwled == "yes")
                    return;
                
                var maxItemCount = 3;
                
                if(_column == 6){
	                maxItemCount = 2;
                }
                
                domAttr.set(domNode, "owled", "yes");
                var owl = $(context.listContainerNode);
                owl.owlCarousel({
                  navigation : false, // Show next and prev buttons,
                  lazyLoad: false, 
                  slideSpeed : 300,
                  paginationSpeed : 400,
                  pagination: false,
                  items : maxItemCount, 
                  itemsDesktop : [1199,maxItemCount],
                  itemsDesktopSmall : [979,2],
                  itemsTablet: [768,1],
                  itemsMobile: [479,1],
                  afterMove: function(){
                  }
                 });
            }
        });

    });