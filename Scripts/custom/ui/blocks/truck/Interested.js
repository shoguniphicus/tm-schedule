define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Interested.html"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template) {
        return declare("js.custom.ui.blocks.truck.Interested", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
                var context = this;
                var domNode = this.domNode;
                
                domAttr.set(domNode, "id", setting.section);
                //return domAttr.get(domNode, "id");
            }, postCreate: function postCreate() {
                this.inherited(arguments);
            }, startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
                
                context.startListening();
                context.initInteraction();
				
				var params = new URLparam().getUrlParams();
				var inner = params['m'];
            }, initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
	            var context = this;
            }, startListening:function(){
	            var context = this;
            }
        });

    });