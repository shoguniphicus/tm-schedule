define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/403.html"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template) {
        return declare("js.custom.ui.blocks.access.403", [BaseBlock, TemplatedMixin], {
            templateString: template,
            filter:null,
            totalres:null,
            totalres_loaded:null,
            homeDropdownHidden:null,
            construct: function() {
                this.inherited(arguments);
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
                
                context.startListening();
                context.initInteraction();
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
	            var context = this;
	        }, startListening: function(){
	            var context = this;
	            var domNode = this.domNode;
	            
            }
        });

    });