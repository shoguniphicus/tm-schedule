define(["dojo/_base/declare",
        "dijit/_Widget",
        "./BaseBlock",
        "dojo/topic",
        "dojo/dom-style",
        "dojo/dom-attr",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/NavigationController.html",
        "dojo/window"
    ],
    function(declare, _Widget, BaseBlock, topic, domStyle, domAttr, TemplatedMixin, template, windows) {
        return declare("js.custom.ui.blocks.NavigationController", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function construct() {
                topic.publish("block.loaded");
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;

                context.startListening();
            },
            destroy: function() {
                this.inherited(arguments);
            },
            startListening: function() {
                var domNode = this.domNode;
                var currentIndex = 1;
                var vs = windows.getBox();
                var widthWindow = vs.w;

                var animateHandle = topic.subscribe("navigation.controller.animate.push", function(_index) {
                    if (typeof domNode === "undefined") {
                        animateHandle.remove();
                        return;
                    }

                    currentIndex = _index;
                    calculateWidth();

                    /*
$(domNode).hide();
                    $(domNode).get(0).offsetHeight; // no need to store this anywhere, the reference is enough
                    $(domNode).show();
*/

                    var animateToPosition = - widthWindow * (currentIndex - 1);
                    
                    var animateToPosition = -(_index - 1) * 100;
                    domStyle.set(domNode, "left", animateToPosition + "%");
                });

                var animatePopHandle = topic.subscribe("navigation.controller.animate.pop", function() {
                    if (typeof domNode === "undefined") {
                        animatePopHandle.remove();
                        return;
                    }

                    currentIndex--;
                    calculateWidth();

                    /*
$(domNode).hide();
                    $(domNode).get(0).offsetHeight; // no need to store this anywhere, the reference is enough
                    $(domNode).show();
*/

                    var animateToPosition = -(currentIndex - 1) * 100;
                    domStyle.set(domNode, "left", animateToPosition + "%");
                });

                var windowResize = topic.subscribe("update.window.resize", function(){
                    if(typeof domNode === "undefined")
                    {
                        windowResize.remove();
                        return;
                    }

                    calculateWidth();
                });

                function calculateWidth(){
                    vs = windows.getBox();
                    widthWindow = vs.w;
                }

                function animateWithTranslate3D(el, translation){
                     domStyle.set(el, "transform", translation);
                     domStyle.set(el, "-o-transform", translation);
                     domStyle.set(el, "-ms-transform", translation);
                     domStyle.set(el, "-moz-transform", translation);
                     domStyle.set(el, "-webkit-transform", translation);
                }
            }
        });

    });