define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Dealer.html",
        "js/custom/ui/cell/DealerCell"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, DealerCell) {
        return declare("js.custom.ui.blocks.Dealer", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function() {
                this.inherited(arguments);
                var context = this;
                
                context.setupContent("");
                context.startListening();
                context.initInteraction();
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
	            var context = this;
	            
	            var request = $.ajax({
        		  url: config.hostURL+"api/dealer/getregion",
        		  type: "GET",
        		  cache: false,
        		  dataType: "json"
        		});
        		 
        		request.done(function( response ) {
        			if (response.status != "success") {
        				console.warn("Error retrieving data");
        				return;
        			}
        			
        			array.forEach(response.data, function(single, index){
	        			var el = '<option value="'+single.key+'">'+single.value+'</option>';
	        			$(context.regionNode).append(el);
        			});
        			
        			$(context.regionNode).change(function(){
        				var value = $(this).val();
	        			
	        			domConstruct.empty(context.listContainerNode);
	        			context.setupContent(value);
        			});
        		});
        		 
        		request.fail(function( jqXHR, textStatus ) {
        		});
        		
        		request.always(function() {
        		});
	        }, startListening: function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            /*
var wallpaperHandle = topic.subscribe("home.change.wallpaper", function(_bg){
	            	$(context.wallpaperNode).removeClass().addClass("wrapper "+_bg);
				});
				topic.publish("topic.handlers.add", wallpaperHandle, domAttr.get(domNode, "id"));
*/
            }, setupContent:function(region){
	            var context = this;
	            
	            var request = $.ajax({
        		  url: config.hostURL+"api/dealer/getdealers/?region="+region,
        		  type: "GET",
        		  cache: false,
        		  dataType: "json"
        		});
        		 
        		request.done(function( response ) {
        			if(response.status == "success"){
	        			array.forEach(response.data, function(single, index){
		        			var dealerCell = new DealerCell().placeAt(context.listContainerNode);
		        			dealerCell.construct(single);
	        			});
        			}
        		});
        		 
        		request.fail(function( jqXHR, textStatus ) {
        		});
        		
        		request.always(function() {
        		});
            }
        });

    });