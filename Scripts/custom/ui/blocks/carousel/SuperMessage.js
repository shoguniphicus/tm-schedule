define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/carousel/Generic",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Super.html",
        "js/custom/ui/cell/carousel/Super"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, SuperCell) {
        return declare("js.custom.ui.blocks.carousel.SuperMessage", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function(setting, index) {
            	var context = this;
            	var domNode = this.domNode;
            	var id = domAttr.get(domNode, "id");
            	
            	var request = $.ajax({
    			  url: config.assetURL+setting,
    			  type: "GET",
    			  cache: false,
    			  dataType: "json"
    			});
    			 
    			request.done(function( response ) {
    				array.forEach(response.data, function(single, index){
    					var setupSetting = new Object();
    					setupSetting.template = "message";
    				
		                var superCell = new SuperCell(setupSetting).placeAt(context.owlCarouselNode);
		                superCell.construct(single);
	                });
					
					context.startListening();
	            	context.setupOwl();
    			});
    			 
    			request.fail(function( jqXHR, textStatus ) {
    			});
    			
    			request.always(function() {
    			});
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
            	 this.inherited(arguments);
            
	            var context = this;
	            
	        }, startListening: function(){
	        	 this.inherited(arguments);
	        
	            var context = this;
	            var domNode = this.domNode;
            }
        });

    });