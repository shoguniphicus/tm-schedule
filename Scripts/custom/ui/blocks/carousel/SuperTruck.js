define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/carousel/Generic",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/SuperTruck.html",
        "js/custom/ui/cell/carousel/Super",
        "js/custom/ui/cell/ColorCell"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, SuperCell, ColorCell) {
        return declare("js.custom.ui.blocks.carousel.SuperTruck", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function(setting, index) {
            	var context = this;
            	var domNode = this.domNode;
            	var id = domAttr.get(domNode, "id");
            	
            	html.set(context.titleNode, setting.title);
            	html.set(context.copyNode, setting.copy);
            	domAttr.set(context.truckVariantNode, "src", config.baseURL+setting.dir+setting.variant_image);
            	domStyle.set(context.backgroundImageNode, "background-image", "url("+config.baseURL+setting.dir+setting.background_image+")");
            	
            	setTimeout(function(){
	            	var downloadLink = $("#quicklink-download-link").attr("href");
	                domAttr.set(context.downloadLinkNode, "href", downloadLink);
            	}, 400);
            	
            	if(setting.thumbnail_path == ''){
	            	$(context.colorIncludeSelectContainerNode).hide();
            	}

                // console.log(setting);
                // console.log(setting.thumbnail_path);
            	
            	array.forEach(setting.data, function(single, index){
            		var setupSetting = new Object();
					setupSetting.template = "truck-super";
				
	                var superCell = new SuperCell(setupSetting).placeAt(context.listContainerNode);
	                superCell.construct(config.baseURL+setting.dir+single.image_path);
	                
	                var colorCell = new ColorCell().placeAt(context.colorContainerNode);
	                colorCell.construct(config.baseURL+setting.dir+single.thumbnail_path, id, index);
                });
				
                this.inherited(arguments);
                
                if(typeof index !== "undefined"){
                	topic.publish("carousel.nav."+id, index);
                }
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
            	 this.inherited(arguments);
            
	            var context = this;
	            
	        }, startListening: function(){
	        	 this.inherited(arguments);
	        
	            var context = this;
	            var domNode = this.domNode;
            }, initOwl:function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            var id = domAttr.get(domNode, "id");
	            var alreadyOwled = domAttr.get(context.listContainerNode, "owled");
	            
	            if(alreadyOwled == "yes")
	            	return;
	            
	            domAttr.set(context.listContainerNode, "owled", "yes");
	            var owl = $(context.listContainerNode);
 
				  owl.owlCarousel({
				      navigation : false, // Show next and prev buttons
				      slideSpeed : 300,
				      paginationSpeed : 400,
				      singleItem:true,
				      lazyLoad : false,
				      pagination: false,
                      transitionStyle : "fade",
                      touchDrag: false,
                      mouseDrag: false
				  });
	            
				var navHandle = topic.subscribe("carousel.nav."+id, function(_slide){
					//console.log("owl",_slide);
					owl.trigger('owl.goTo', _slide);
/* 					owl.trigger('owl.next'); */
				});
				topic.publish("topic.handlers.add", navHandle, domAttr.get(domNode, "id"));
            }
        });

    });