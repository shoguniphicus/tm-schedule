define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/carousel/Generic",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Super.html",
        "js/custom/ui/cell/carousel/Double"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, DoubleCell) {
        return declare("js.custom.ui.blocks.carousel.DoubleWithNav", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
            	var context = this;
            	var domNode = this.domNode;
            	
            	if(typeof setting === "string"){
	            	var request = $.ajax({
        			  url: config.hostURL+setting,
        			  type: "GET",
        			  cache: false,
        			  dataType: "json"
        			});
        			 
        			request.done(function( response ) {
        				var setupSetting = new Object();
        				
        				switch(setting){
	        				case "api/news/index?limit=5":
	        					setupSetting.template = "homenews";
	        				break;
	        				
	        				default:
	        				break;
        				}
        				
        				array.forEach(response.data, function(single, index){
        					var doubleCell = new DoubleCell(setupSetting).placeAt(domNode);
							doubleCell.construct(single);
		                });
						
						context.startListening();
		            	context.setupOwl();
        			});
        			 
        			request.fail(function( jqXHR, textStatus ) {
        			});
        			
        			request.always(function() {
        			});
            	}else{
	            	context.populateContent(setting);
					this.inherited(arguments);
            	}
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
            	this.inherited(arguments);
            
	            var context = this;
	            
	        }, startListening: function(){
	        	this.inherited(arguments);
	        
	            var context = this;
	            var domNode = this.domNode;
            }, initOwl:function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            var alreadyOwled = domAttr.get(domNode, "owled");
	            
	            if(alreadyOwled == "yes")
	            	return;
	            
	            domAttr.set(domNode, "owled", "yes");
	            
	            var owl = $(context.owlCarouselNode).owlCarousel({
 
			      navigation : false, // Show next and prev buttons
			      slideSpeed : 300,
			      paginationSpeed : 400,
			      items : 2, 
			      itemsDesktop : [1199,2],
				  itemsDesktopSmall : [979,2]
			  });
			  
			  $(context.customControlNode).show();
			  
			  // Custom Navigation Events
			  $(context.nextNode).click(function(){
			    owl.trigger('owl.next');
			  })
			  $(context.prevNode).click(function(){
			    owl.trigger('owl.prev');
			  })
            }, populateContent:function(setting){
	            var context = this;
	            var domNode = this.domNode;
	            
	            array.forEach(setting, function(single, index){
	                var doubleCell = new DoubleCell().placeAt(context.owlCarouselNode);
	                doubleCell.construct(single);
                });
            }
        });

    });