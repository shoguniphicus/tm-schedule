define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/carousel/Generic",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Super.html",
        "js/custom/ui/cell/carousel/Super"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, SuperCell) {
        return declare("js.custom.ui.blocks.carousel.SuperMedia", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function(setting, index) {
            	var context = this;
            	var domNode = this.domNode;
            	var id = domAttr.get(domNode, "id");
            	
            	array.forEach(setting, function(single, index){
            		var setupSetting = new Object();
					setupSetting.template = "media";
				
	                var superCell = new SuperCell(setupSetting).placeAt(context.owlCarouselNode);
	                superCell.construct(single);
                });
				
                this.inherited(arguments);
                
                if(typeof index !== "undefined"){
                	topic.publish("carousel.nav."+id, index);
                }	
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
            	 this.inherited(arguments);
            
	            var context = this;
	            
	        }, startListening: function(){
	        	 this.inherited(arguments);
	        
	            var context = this;
	            var domNode = this.domNode;
            }
        });

    });