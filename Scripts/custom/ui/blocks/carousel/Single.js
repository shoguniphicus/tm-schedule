define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/carousel/Generic",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Super.html",
        "js/custom/ui/cell/carousel/Super"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, SingleCell) {
        return declare("js.custom.ui.blocks.carousel.Single", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
            	var context = this;
            	var domNode = this.domNode;
            	
				context.populateContent(setting);
                this.inherited(arguments);
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
            	this.inherited(arguments);
            
	            var context = this;
	            
	        }, startListening: function(){
	        	this.inherited(arguments);
	        
	            var context = this;
	            var domNode = this.domNode;
            }, initOwl:function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            var alreadyOwled = domAttr.get(domNode, "owled");
	            
	            if(alreadyOwled == "yes")
	            	return;
	            
	            domAttr.set(domNode, "owled", "yes");
	            
	           var owl = $(context.owlCarouselNode).owlCarousel({
 
			      navigation : false, // Show next and prev buttons
			      slideSpeed : 300,
			      paginationSpeed : 400,
			      items : 1, 
			      itemsDesktop : [1199,1],
				  itemsDesktopSmall : [979,1]
			  });
			  
			  $(context.customControlNode).show();
			  $(context.nextNode).click(function(){
			    owl.trigger('owl.next');
			  })
			  $(context.prevNode).click(function(){
			    owl.trigger('owl.prev');
			  })
            }, populateContent:function(setting){
	            var context = this;
	            var domNode = this.domNode;
	            
	            array.forEach(setting, function(single, index){
	                var singleCell = new SingleCell().placeAt(context.owlCarouselNode);
	                singleCell.construct(single);
                });
            }
        });

    });