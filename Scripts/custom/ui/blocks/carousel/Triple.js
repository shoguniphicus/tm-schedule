define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/carousel/Generic",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Triple.html",
        "js/custom/ui/cell/carousel/Triple"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, TripleCell) {
        return declare("js.custom.ui.blocks.carousel.Triple", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function(setting, title, _secondRow) {
            	var context = this;
            	var domNode = this.domNode;
            	
            	html.set(context.titleNode, title);
            	domAttr.set(domNode, "data-carousel-synced", setting);
            	
            	if(_secondRow){
	            	domAttr.set(domNode, "data-second", "yes");
	            	domClass.add(domNode, "hidden-xs visible-lg");
            	}
            	
            	if(typeof setting === "string"){
	            	var request = $.ajax({
        			  url: config.hostURL+setting,
        			  type: "GET",
        			  cache: false,
        			  dataType: "json"
        			});
        			 
        			request.done(function( response ) {
        				var totalLength = response.data.length;
        				
        				array.forEach(response.data, function(single, index){
        					if(_secondRow){
	        					if(index > 2){
		        					var tripleCell = new TripleCell().placeAt(context.listContainerNode);
									tripleCell.construct(single, response.data, index);
	        					}
        					}else{
	        					if(index < totalLength - 3){
		        					var tripleCell = new TripleCell().placeAt(context.listContainerNode);
									tripleCell.construct(single, response.data, index);
	        					}
        					}
		                });
						
						context.startListening();
		            	context.setupOwl();
        			});
        			 
        			request.fail(function( jqXHR, textStatus ) {
        			});
        			
        			request.always(function() {
        			});
            	}else{
	            	context.populateContent(setting);
					this.inherited(arguments);
            	}
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
            	this.inherited(arguments);
            
	            var context = this;
	            
	            
	        }, startListening: function(){
	        	this.inherited(arguments);
	        
	            var context = this;
	            var domNode = this.domNode;
            }, initOwl:function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            var alreadyOwled = domAttr.get(context.listContainerNode, "owled");
	            var owlSynced = domAttr.get(domNode, "data-carousel-synced");
	            var owlSyncedDelayKey = owlSynced+".delay";
	            
	            if(alreadyOwled == "yes")
	            	return;
	            
	            domAttr.set(context.listContainerNode, "owled", "yes");
	            var _secondRow = domAttr.get(domNode, "data-second");
	            
	            var owl = $(context.listContainerNode);
				owl.owlCarousel({
				      navigation : false, // Show next and prev buttons,
				      lazyLoad: true, 
				      slideSpeed : 300,
				      paginationSpeed : 400,
				      pagination: false,
				      items : 3, 
				      itemsDesktop : [1199,2],
					  itemsDesktopSmall : [979,2],
					  afterMove: function(){
					  	//console.log(owlSynced, this.owl.currentItem);
					  	topic.publish(owlSynced, this.owl.currentItem);
					  	//$.jStorage.set(owlSynced, this.owl.currentItem);
					  }
				  });
			  
			  if(_secondRow == "yes"){
	        		domConstruct.destroy(context.titleContainerNode);
	        		
	        		domStyle.set(context.arrowNode, "display", "block");
	        		
	        		on(context.prevNode, "click", function(){
		        		owl.trigger('owl.prev');
	        		});
	        		
	        		on(context.nextNode, "click", function(){
		        		owl.trigger('owl.next');
	        		});
	        	}
	        	
	        	topic.subscribe(owlSynced, function(_index){
	        		var delay = $.jStorage.get(owlSynced+_secondRow);
	        		if(delay)
						return;
	        		
	        		$.jStorage.set(owlSynced+_secondRow, true, {TTL: 500});
	        		owl.trigger('owl.goTo', _index);
	    		});
            }, populateContent:function(setting){
	            var context = this;
	            var domNode = this.domNode;
	            
	            array.forEach(setting, function(single, index){
	                var doubleCell = new DoubleCell().placeAt(domNode);
	                doubleCell.construct(single);
                });
            }
        });

    });