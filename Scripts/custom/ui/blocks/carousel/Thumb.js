/*
	This is not a carousel
*/


define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Thumb.html",
        "js/custom/ui/cell/carousel/Thumb"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, ThumbCell) {
        return declare("js.custom.ui.blocks.carousel.Thumb", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
            	var context = this;
            	var domNode = this.domNode;
            	
				context.populateContent(setting);
                this.inherited(arguments);
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
            	this.inherited(arguments);
            
	            var context = this;
	            
	        }, startListening: function(){
	        	this.inherited(arguments);
	        
	            var context = this;
	            var domNode = this.domNode;
            }, populateContent:function(setting){
	            var context = this;
	            var domNode = this.domNode;
	            
	            array.forEach(setting, function(single, index){
	                var thumbCell = new ThumbCell().placeAt(domNode);
	                thumbCell.construct(single, setting, index);
                });
            }
        });

    });