define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Generic.html",
        "js/custom/utils/DynamicVendor"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, DynamicVendor) {
        return declare("js.custom.ui.blocks.carousel.Generic", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function() {
            	var context = this;
            	
            	context.startListening();
            	context.setupOwl();
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
                
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
	            var context = this;
	            
	        }, startListening: function(){
	        	var context = this;
	            var domNode = this.domNode;
	            var id = domAttr.get(domNode, "id");
	            
	            var navHandle = topic.subscribe("carousel.generic."+id, function(){
				});
				topic.publish("topic.handlers.add", navHandle, domAttr.get(domNode, "id"));
				
				var owlHandle = topic.subscribe("vendor.script.loaded.owl.carousel.min", function(){
					context.initOwl();
				});
				topic.publish("topic.handlers.add", owlHandle, domAttr.get(domNode, "id"));
            }, setupOwl:function(){
	            var context = this;
	            
	            var owlCarouselCSS = new DynamicVendor().inject("owl.carousel", "css");
	            var owlThemeCSS = new DynamicVendor().inject("owl.theme", "css");
	            var owlTransitionCSS = new DynamicVendor().inject("owl.transitions", "css");
	            
	            var owlCarouselJS = new DynamicVendor().inject("owl.carousel.min", "js");
            }, initOwl:function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            var id = domAttr.get(domNode, "id");
	            
	            var alreadyOwled = domAttr.get(domNode, "owled");
	            
	            if(alreadyOwled == "yes")
	            	return;
	            
	            domAttr.set(domNode, "owled", "yes");
	            
	            var owl = $(context.listContainerNode);
	            
	            var owl = $(context.owlCarouselNode).owlCarousel({
				      navigation : false, // Show next and prev buttons
				      slideSpeed : 300,
				      paginationSpeed : 400,
				      singleItem:true,
				      lazyLoad : true
				}).data('owlCarousel');
				
				var navHandle = topic.subscribe("carousel.nav."+id, function(_slide){
					owl.jumpTo(_slide);
				});
				topic.publish("topic.handlers.add", navHandle, domAttr.get(domNode, "id"));
            }
        });

    });