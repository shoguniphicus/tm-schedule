define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/carousel/Generic",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Super.html",
        "js/custom/ui/cell/carousel/Super"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, SuperCell) {
        return declare("js.custom.ui.blocks.carousel.Super", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function(setting, index) {
            	var context = this;
            	var domNode = this.domNode;
            	var id = domAttr.get(domNode, "id");
            	
            	if(typeof setting === "string"){
	            	var request = $.ajax({
        			  url: config.assetURL+setting,
        			  type: "GET",
        			  cache: false,
        			  dataType: "json"
        			});
        			 
        			request.done(function( response ) {
        				array.forEach(response, function(single, index){
        					var setupSetting = new Object();
        					setupSetting.template = "home";
        				
			                var superCell = new SuperCell(setupSetting).placeAt(context.owlCarouselNode);
			                superCell.construct(single);
		                });
						
						context.startListening();
		            	context.setupOwl();
        			});
        			 
        			request.fail(function( jqXHR, textStatus ) {
        			});
        			
        			request.always(function() {
        			});
            	}else{
	            	array.forEach(setting, function(single, index){
		                var superCell = new SuperCell().placeAt(context.owlCarouselNode);
		                superCell.construct(single);
	                });
					
	                this.inherited(arguments);
	                
	                if(typeof index !== "undefined"){
	                	topic.publish("carousel.nav."+id, index);
	                }	
            	}
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
            	 this.inherited(arguments);
            
	            var context = this;
	            
	        }, startListening: function(){
	        	 this.inherited(arguments);
	        
	            var context = this;
	            var domNode = this.domNode;
            }, initOwl:function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            var id = domAttr.get(domNode, "id");
	            
	            var alreadyOwled = domAttr.get(domNode, "owled");
	            
	            if(alreadyOwled == "yes")
	            	return;
	            
	            domAttr.set(domNode, "owled", "yes");
	            
	            var owl = $(context.listContainerNode);
	            
	            var owl = $(context.owlCarouselNode).owlCarousel({
				      navigation : false, // Show next and prev buttons
				      slideSpeed : 300,
				      paginationSpeed : 400,
				      singleItem:true,
				      lazyLoad : true,
				      autoPlay : 5000,
				      stopOnHover : true
				 
				      // "singleItem:true" is a shortcut for:
				      // items : 1, 
				      // itemsDesktop : false,
				      // itemsDesktopSmall : false,
				      // itemsTablet: false,
				      // itemsMobile : false
				 
				}).data('owlCarousel');
				
				console.log("Listening", "carousel.nav."+id);
				var navHandle = topic.subscribe("carousel.nav."+id, function(_slide){
					owl.jumpTo(_slide);
				});
				topic.publish("topic.handlers.add", navHandle, domAttr.get(domNode, "id"));
            }
        });

    });