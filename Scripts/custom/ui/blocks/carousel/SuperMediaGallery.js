define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/carousel/Generic",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Super.html",
        "js/custom/ui/cell/carousel/Super"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, SuperCell) {
        return declare("js.custom.ui.blocks.carousel.SuperMediaGallery", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function(setting, index) {
                var context = this;
                var domNode = this.domNode;
                var id = domAttr.get(domNode, "id");
                
/*                 domAttr.set(context.findoutMoreNode, "href", config.hostURL+"gallery"); */
/*                 domStyle.set(context.findoutMoreNode, "display", "block"); */
                
                array.forEach(setting, function(single, index){
                    var setupSetting = new Object();
                    setupSetting.template = "gallery";
                    
                    var superCell = new SuperCell(setupSetting).placeAt(context.owlCarouselNode);
                    superCell.construct(single);
                });
                
                this.inherited(arguments);
                
                if(typeof index !== "undefined"){
                    topic.publish("carousel.nav."+id, index);
                }   
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
                this.inherited(arguments);
                
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
                 this.inherited(arguments);
            
                var context = this;
                
            }, startListening: function(){
                 this.inherited(arguments);
            
                var context = this;
                var domNode = this.domNode;
            }, initOwl:function(){
                var context = this;
                var domNode = this.domNode;
                
                var alreadyOwled = domAttr.get(domNode, "owled");
                
                if(alreadyOwled == "yes")
                    return;
                
                domAttr.set(domNode, "owled", "yes");
                
                $(context.owlCarouselNode).owlCarousel({
 
                  navigation : false, // Show next and prev buttons
                  slideSpeed : 300,
                  paginationSpeed : 400,
                  items : 1, 
                  itemsDesktop : [1199,1],
                  itemsDesktopSmall : [979,1]
              });
            }
        });

    });