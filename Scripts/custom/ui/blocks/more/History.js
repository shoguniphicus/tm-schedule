define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/History.html",
        "js/custom/ui/blocks/carousel/SuperHistory"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, CarouselSuperHistory) {
        return declare("js.custom.ui.blocks.other.History", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function() {
                this.inherited(arguments);
                var context = this;
                
                context.setupContent();
                context.startListening();
                context.initInteraction();
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, setupContent:function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            var carouselSuperHistory = new CarouselSuperHistory().placeAt(context.listContainerNode);
	            carouselSuperHistory.construct('resources/data/history.json');
            }, initInteraction:function(){
	            var context = this;
	            
	        }, startListening: function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            /*
var wallpaperHandle = topic.subscribe("home.change.wallpaper", function(_bg){
	            	$(context.wallpaperNode).removeClass().addClass("wrapper "+_bg);
				});
				topic.publish("topic.handlers.add", wallpaperHandle, domAttr.get(domNode, "id"));
*/
            }
        });

    });