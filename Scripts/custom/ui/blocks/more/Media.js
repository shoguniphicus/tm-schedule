define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Media.html",
        "js/custom/ui/blocks/carousel/SuperMedia",
        "js/custom/ui/blocks/carousel/SuperMediaPress",
        "js/custom/ui/blocks/carousel/SuperMediaGallery"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, CarouselSuperMedia, CarouselSuperMediaPress, CarouselSuperMediaGallery) {
        return declare("js.custom.ui.blocks.more.Media", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function() {
                this.inherited(arguments);
                var context = this;
                
                context.setupContent();
                context.startListening();
                context.initInteraction();
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
                this.inherited(arguments);
                
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, setupContent:function(){
                var context = this;
                var domNode = this.domNode;
                
                getNews();
                getPress();
                getGallery();
                
                function getNews(){
                    var request = $.ajax({
                      url: config.hostURL+"api/news/index?limit="+3,
                      type: "GET",
                      cache: false,
                      dataType: "json"
                    });
                     
                    request.done(function( response ) {
                        if (response.status != "success") {
                            console.warn("Error retrieving data");
                            return;
                        }
                        
                        var superMedia = CarouselSuperMedia().placeAt(context.newsEventNode);
                        superMedia.construct(response.data);
                    });
                     
                    request.fail(function( jqXHR, textStatus ) {
                        
                    });
                    
                    request.always(function() {
                    });
                }
                
                function getPress(){
                    var request = $.ajax({
                      url: config.hostURL+"api/press/index?limit="+3,
                      type: "GET",
                      cache: false,
                      dataType: "json"
                    });
                     
                    request.done(function( response ) {
                        if (response.status != "success") {
                            console.warn("Error retrieving data");
                            return;
                        }
                        
                        var superMedia = CarouselSuperMediaPress().placeAt(context.pressNode);
                        superMedia.construct(response.data);
                    });
                     
                    request.fail(function( jqXHR, textStatus ) {
                        
                    });
                    
                    request.always(function() {
                    });
                }
                
                function getGallery(){
                    var request = $.ajax({
                      url: config.hostURL+"api/gallery/last",
                      type: "GET",
                      cache: false,
                      dataType: "json"
                    });
                     
                    request.done(function( response ) {
                        if (response.status != "success") {
                            console.warn("Error retrieving data");
                            return;
                        }
                        
                        var superMedia = CarouselSuperMediaGallery().placeAt(context.galleryNode);
                        superMedia.construct(response.data);
                        
                        html.set(context.galleryTitleNode, response.data.title);
                    });
                     
                    request.fail(function( jqXHR, textStatus ) {
                        
                    });
                    
                    request.always(function() {
                    });
                }
            }, initInteraction:function(){
                var context = this;
                
            }, startListening: function(){
                var context = this;
                var domNode = this.domNode;
                
                /*
var wallpaperHandle = topic.subscribe("home.change.wallpaper", function(_bg){
                    $(context.wallpaperNode).removeClass().addClass("wrapper "+_bg);
                });
                topic.publish("topic.handlers.add", wallpaperHandle, domAttr.get(domNode, "id"));
*/
            }
        });

    });