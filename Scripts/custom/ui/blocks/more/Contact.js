define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Contact.html"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template) {
        return declare("js.custom.ui.blocks.more.Contact", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function() {
                this.inherited(arguments);
                var context = this;
                
                context.setupContent();
                context.startListening();
                context.initInteraction();
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, setupContent:function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            var request = $.ajax({
                  url: config.hostURL+"api/car/get_all?dropdown=yes",
/* 				  url: config.hostURL+"api/car/get_model_type", */
                  type: "GET",
                  cache: false,
                  dataType: "json"
                });
                 
                request.done(function( response ) {
                    if (response.status != "success") {
                        console.warn("Error retrieving data");
                        return;
                    }
                    
                    alltruckdata = response.data;
                    array.forEach(response.data, function(single, index){
                        var optionContainer = context.own_modelNode;
                        var singleOption = "<option value='"+single.id+"'>"+single.car_model_name+"</option>";
/* 						var singleOption = "<option value='"+single.key+"'>"+single.value+"</option>";	 */
						                        
                        $(optionContainer).append(singleOption);
                    });
                });
                 
                request.fail(function( jqXHR, textStatus ) {
                    console.log("Fail to start up , please refresh your browser");
                });
                
                request.always(function() {
                });
            }, initInteraction:function(){
	            var context = this;
                var domNode = this.domNode;
                
                $('input[type=radio][name=ownone]').change(function() {
			        if (this.value == 'yes') {
			        	domStyle.set(context.ownModelContainerNode, "display", "block");
			        }
			        else if (this.value == 'no') {
			        	domStyle.set(context.ownModelContainerNode, "display", "none");
			        }
			    });
                
                $(context.contactFormNode).validate({
                    rules:{
                        e_own_model: {
                            required: true
                        },e_fullname: {
                            minlength: 3,
                            required: true
                        },e_email: {
                            email: true,
                            required: true
                        },e_phone: {
                            minlength: 10,
                            required: true
                        },e_enquiry: {
                            required: true
                        }
                    },
                     highlight: function(element) {
                        var id_attr = "#" + $( element ).attr("id") + "1";
                        $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                        $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');         
                    },
                    unhighlight: function(element) {
                        var id_attr = "#" + $( element ).attr("id") + "1";
                        $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                        $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');         
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function(error, element) {
                        var indicator = "#" + $( element ).attr("id") + "1";
                        error.insertAfter($(indicator));
                    },submitHandler: function(form) {
                        context.submit();
                    }

                });
	            
	        }, startListening: function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            /*
var wallpaperHandle = topic.subscribe("home.change.wallpaper", function(_bg){
	            	$(context.wallpaperNode).removeClass().addClass("wrapper "+_bg);
				});
				topic.publish("topic.handlers.add", wallpaperHandle, domAttr.get(domNode, "id"));
*/
            },submit:function(){
                var context = this;
                var date = $(context.dateNode).val();
                var model = $(context.own_modelNode).val(); 
                var fullname = $(context.fullnameNode).val();
                var email = $(context.emailNode).val(); 
                var phone = $(context.phoneNode).val();
                var enquiry = $(context.enquiryNode).val();
                
                var dataPackage = new Object();
                dataPackage.date = $(context.dateNode).val();
                dataPackage.own_model = $(context.own_modelNode).val(); 
                dataPackage.fullname = $(context.fullnameNode).val();
                dataPackage.email = $(context.emailNode).val(); 
                dataPackage.phone = $(context.phoneNode).val();
                dataPackage.enquiry = $(context.enquiryNode).val();

                if($(context.cb_yesdoyouownisuzuNode).is(':checked')){
                    dataPackage.doyouownisuzu = 1;
                    
                    if(model == "-"){
                    	alert("Please select your truck model");
	                    return;
                    }
                }

                if($(context.cb_nodoyouownisuzuNode).is(':checked')){
                    dataPackage.doyouownisuzu = 0;
                }

                if($(context.cb_tncNode).is(':checked')){
                }else{
                	alert("Please check T&C checkbox");
                	return;
                }

                if($(context.cb_emailchannelpreferenceNode).is(':checked')){
                    dataPackage.contact_method = "email";
                }

                if($(context.cb_phonechannelpreferenceNode).is(':checked')){
                    dataPackage.contact_method = "phone";
                }

                // if($(context.cb_yesournewsletterNode).is(':checked')){
                //     dataPackage.newsletter = 1;
                // }

                // if($(context.cb_noournewsletterNode).is(':checked')){
                //     dataPackage.newsletter = 0;
                // }

                console.log("dataPackage", dataPackage);


                var request = $.ajax({
                  url: config.hostURL+"api/form/contact",
                  type: "POST",
                  data: dataPackage,
                  cache: false,
                  dataType: "json"
                });
                 
                request.done(function( response ) {
                    console.log(response);
                    if (response.status != "success") {
                        console.warn("Error in sending data");
                        return;
                    }
                    
                    alert("Your enquiry has been submited. Thank you");
                    location.reload();
                });
                 
                request.fail(function( jqXHR, textStatus ) {

                });
                
                request.always(function() {
                });
            }
        });

    });