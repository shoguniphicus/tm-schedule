define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Careers.html",
        "js/custom/ui/cell/accordion/Careers"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, CareersAccCell) {
        return declare("js.custom.ui.blocks.other.Careers", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function() {
                this.inherited(arguments);
                var context = this;
                
                context.setupContent();
                context.startListening();
                context.initInteraction();
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, setupContent:function(){
	            var context = this;
	            var domNode = this.domNode;
				var id = domAttr.get(domNode, "id");
				var accordionID = "accordion_careers_"+id;
				
				domAttr.set(context.listContainerNode, "id", accordionID);
	           
	            var request = $.ajax({
        		  url: config.hostURL+"api/careers/getcareers/",
        		  type: "GET",
        		  cache: false,
        		  dataType: "json"
        		});
        		 
        		request.done(function( response ) {
        			if (response.status != "success") {
        				console.warn("Error retrieving data");
        				return;
        			}
        
					array.forEach(response.data, function(single, index){
						var careersAccCell = new CareersAccCell().placeAt(context.listContainerNode);
						careersAccCell.construct(single, accordionID, index);
					});
					
					$(context.listContainerNode).collapse();
        		});
        		 
        		request.fail(function( jqXHR, textStatus ) {
        		});
        		
        		request.always(function() {
        		});
	            
            }, initInteraction:function(){
	            var context = this;
	            
	        }, startListening: function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            /*
var wallpaperHandle = topic.subscribe("home.change.wallpaper", function(_bg){
	            	$(context.wallpaperNode).removeClass().addClass("wrapper "+_bg);
				});
				topic.publish("topic.handlers.add", wallpaperHandle, domAttr.get(domNode, "id"));
*/
            }
        });

    });