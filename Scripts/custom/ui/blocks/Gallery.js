define(["dojo/_base/declare",
        "dijit/_Widget",
        "js/custom/ui/blocks/BaseBlock",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Gallery.html",
        "js/custom/ui/blocks/carousel/Triple"
    ],
    function(declare, _Widget, BaseBlock,  TemplatedMixin, template, CarouselTriple) {
        return declare("js.custom.ui.blocks.Gallery", [BaseBlock, TemplatedMixin], {
            templateString: template,
            construct: function(yearFilter) {
                this.inherited(arguments);
                var context = this;
               
                context.setupContent(yearFilter);
                context.startListening();
                context.initInteraction();
            },
            postCreate: function postCreate() {
                this.inherited(arguments);
            },
            startup: function() {
                this.inherited(arguments);

                var domNode = this.domNode;
                var context = this;
            },
            initNavigationTransition:function(){
	            this.inherited(arguments);
	            
                var domNode = this.domNode;
                var context = this;
                
                var index = domAttr.get(domNode, "navigation-index-data");
            }, initInteraction:function(){
	            var context = this;
	           
	        }, startListening: function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            /*
var wallpaperHandle = topic.subscribe("home.change.wallpaper", function(_bg){
	            	$(context.wallpaperNode).removeClass().addClass("wrapper "+_bg);
				});
				topic.publish("topic.handlers.add", wallpaperHandle, domAttr.get(domNode, "id"));
*/
            }, setupContent:function(yearFilter){
	            var context = this;
	            
	            var request = $.ajax({
        		  url: config.hostURL+"api/gallery/index/"+yearFilter,
        		  type: "GET",
        		  cache: true,
        		  dataType: "json"
        		});
        		 
        		request.done(function( response ) {
        			if(response.status == "success"){
	        			array.forEach(response.data, function(single, index){
		        			var carouselTriple = new CarouselTriple().placeAt(context.listContainerNode);
		        			carouselTriple.construct("api/gallery/album?id="+single.id, single.title, false);
		        			
		        			var carouselTripleSecondRow = new CarouselTriple().placeAt(context.listContainerNode);
		        			carouselTripleSecondRow.construct("api/gallery/album?id="+single.id, single.title, true);
	        			});
        			}
        		});
        		 
        		request.fail(function( jqXHR, textStatus ) {
        		});
        		
        		request.always(function() {
        		});
            }
        });

    });