define(["dojo/_base/declare", 
	"dijit/_Widget",
	"js/custom/ui/modal/BaseModal",
	"dijit/_TemplatedMixin", 
	"dojo/text!./html/MessageModal.html"], 
	function(declare, _Widget, BaseModal, TemplatedMixin,template) {
		
		return declare("js.custom.ui.modal.MessageModal",[BaseModal, TemplatedMixin], { 
			templateString:template,
			construct: function(setting, index)
			{
				var context = this;
				var setting = $.jStorage.get("message.setting");
				
				html.set(context.titleNode, setting.title);
				html.set(context.desNode, setting.des);
				
				if(setting.refresh){
					$(".modal").on('hidden.bs.modal', function () {
						location.reload();
					});
				}
				
				$.jStorage.deleteKey("message.setting");
			},startup:function(){
				this.inherited(arguments);
				
				var domNode = this.domNode;
				var context = this;
				
				context.initModal();
				context.initInteraction();
			},initInteraction:function(){
				var context = this;
				var domNode = this.domNode;
			}
		});

});