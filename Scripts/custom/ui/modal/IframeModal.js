define(["dojo/_base/declare", 
	"dijit/_Widget",
	"js/custom/ui/modal/BaseModal",
	"dijit/_TemplatedMixin", 
	"dojo/text!./html/IframeModal.html"], 
	function(declare, _Widget, BaseModal, TemplatedMixin,template) {
		
		return declare("js.custom.ui.modal.IframeModal",[BaseModal, TemplatedMixin], { 
			templateString:template,
			construct: function()
			{
				var context = this;
				var domNode = this.domNode;
			
				
				var setting = $.jStorage.get("iframe.modal");
				domAttr.set(context.videoNode, "src", setting);
/* 				domAttr.set(context.videoNode, "src", "//www.youtube.com/embed/"+setting); */
				
				$.jStorage.deleteKey("iframe.modal")
			},destroy: function() {
            	var domNode = this.domNode;
            	
            	if(domNode == null)
            		return;
            	
            	topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            	
                this.inherited(arguments);
            },startup:function(){
				this.inherited(arguments);
				
				var domNode = this.domNode;
				var context = this;
				
				context.startListening();
				context.initModal();
				context.initInteraction();
			},initInteraction:function(){
				var context = this;
				var domNode = this.domNode;
				
				on(context.closeNode, "click", function(){
					topic.publish("promodal.animate.hide");
				});
			},startListening:function(){
				var context = this;
				var domNode = this.domNode;
				
				var resizeHandle = topic.subscribe("update.window.resize", function(w, h){
					if(w < 960){
						topic.publish("promodal.animate.hide");
					}
				});
			}
		});

});