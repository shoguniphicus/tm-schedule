define(["dojo/_base/declare", 
	"dijit/_Widget",
	"dijit/_TemplatedMixin"], 
	function(declare, _Widget, TemplatedMixin) {
		
		return declare("js.custom.ui.modal.BaseModal",[_Widget, TemplatedMixin], {
			postCreate: function postCreate(){
				this.inherited(arguments);
				var context =this;
			},destroy: function() {
            	var domNode = this.domNode;
            	
            	if(domNode == null)
            		return;
            	
            	topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            	
                this.inherited(arguments);
            },initModal:function(){
				var domNode = this.domNode;
				var context = this;
				
				$(domNode).modal({
					backdrop : true,
					show: true,
					keyboard: false
				});	

				$(".modal").on('hidden.bs.modal', function () {
					var body = win.body();
		
					if(domClass.contains(body, "modal-open")){
						domClass.remove(body, "modal-open");
					}
					
					if(domClass.contains(body, "page-overflow")){
						domClass.remove(body, "page-overflow");
					}
					
					
					$(".datepicker").remove();
					topic.publish("nuke.this", domNode);
					context.destroy();
				});
				
				var promodalHideHandle = topic.subscribe("promodal.animate.hide", function(){
					$(".modal-backdrop").remove();
					topic.publish("nuke.this", domNode);
					context.destroy();
				});
				topic.publish("topic.handlers.add", promodalHideHandle, domAttr.get(domNode, "id"));
				
				topic.publish("hide.global.loading");
			}
		});

});