define(["dojo/_base/declare", 
	"dijit/_Widget",
	"js/custom/ui/modal/BaseModal",
	"dijit/_TemplatedMixin", 
	"dojo/text!./html/GoogleMap.html",
	"js/custom/ui/cell/DealerCell"], 
	function(declare, _Widget, BaseModal, TemplatedMixin,template, DealerCell) {
		
		return declare("js.custom.ui.modal.GoogleMap",[BaseModal, TemplatedMixin], { 
			templateString:template,
			construct: function()
			{
				var context = this;
				var domNode = this.domNode;
				
				var setting = $.jStorage.get("googlemap.coord.modal");
				var mapDomID = domAttr.set(context.mapNode, "id", "map-canvas-"+domAttr.get(domNode, "id"));
				var myLatlng = new google.maps.LatLng(parseFloat(setting.dealer_coord_lat),parseFloat(setting.dealer_coord_lon));
				
				var mapOptions = {
		          center: myLatlng,
		          zoom: 16,
		          disableDefaultUI: true,
		          panControl: true,
				  zoomControl: true,
				  mapTypeControl: true,
				  mapTypeControl: true,
				    mapTypeControlOptions: {
				        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
				        position: google.maps.ControlPosition.BOTTOM_CENTER
				    },
				  scaleControl: true
		        };
		      
		        setTimeout(function(){
			        var map = new google.maps.Map(context.mapNode, mapOptions);
			        var contentString = new DealerCell().dom(setting);
					
					var infowindow = new google.maps.InfoWindow({
					  content: contentString,
					  maxWidth: 300
					});
					
					var marker = new google.maps.Marker({
					  position: myLatlng,
					  map: map,
					  title: 'ISUZU Dealer'
					});
					
					infowindow.open(map,marker);
					google.maps.event.addListener(marker, 'click', function() {
					    infowindow.open(map,marker);
					});

		        }, 500);
				
				$.jStorage.deleteKey("googlemap.coord.modal")
			},startup:function(){
				this.inherited(arguments);
				
				var domNode = this.domNode;
				var context = this;
				
				context.initModal();
				context.initInteraction();
			},initInteraction:function(){
				var context = this;
				var domNode = this.domNode;
				
				on(context.closeNode, "click", function(){
					topic.publish("promodal.animate.hide");
				});
			}
		});

});