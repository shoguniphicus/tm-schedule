define(["dojo/_base/declare", 
	"dijit/_Widget",
	"js/custom/ui/modal/BaseModal",
	"dijit/_TemplatedMixin", 
	"dojo/text!./html/ImageModal.html",
	"js/custom/ui/blocks/carousel/SuperImage",
	"js/custom/ui/blocks/carousel/SuperGallery"], 
	function(declare, _Widget, BaseModal, TemplatedMixin,template, CarouselSuperImage, CarouselSuperGallery) {
		
		return declare("js.custom.ui.modal.ImageModal",[BaseModal, TemplatedMixin], { 
			templateString:template,
			construct: function()
			{
				var context = this;
				var domNode = this.domNode;
				
				var setting = $.jStorage.get("imagemodal.setting");
				$.jStorage.deleteKey("imagemodal.setting");
				
				if(setting.data == null)
				{
					topic.publish("promodal.animate.hide");
					return;
				}
				
				
				if(typeof setting.type !== "undefined"){
					if(setting.type == "gallery"){
						var carouselSuperGallery = new CarouselSuperGallery().placeAt(context.bodyNode);
					    carouselSuperGallery.construct(setting.data, setting.index);
						return;
					}	
				}
				
				var carouselSuperImage = new CarouselSuperImage().placeAt(context.bodyNode);
			    carouselSuperImage.construct(setting.data, setting.index);
			},startup:function(){
				this.inherited(arguments);
				
				var domNode = this.domNode;
				var context = this;
				
				context.initModal();
				context.initInteraction();
			},initInteraction:function(){
				var context = this;
				var domNode = this.domNode;
				
				on(context.closeNode, "click", function(){
					topic.publish("promodal.animate.hide");
				});
			}
		});

});