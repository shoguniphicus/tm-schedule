define(["dojo/_base/declare",
        "dijit/_Widget",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Super.html",
        "dojo/text!./html/SuperHome.html",
        "dojo/text!./html/SuperPromotion.html",
        "dojo/text!./html/SuperAbout.html",
        "dojo/text!./html/SuperHistory.html",
        "dojo/text!./html/SuperMessage.html",
        "dojo/text!./html/SuperMedia.html",
        "dojo/text!./html/SuperMediaGallery.html",
        "dojo/text!./html/SuperTruck.html",
        "dojo/text!./html/SuperImage.html",
        "dojo/text!./html/SuperGalleryModal.html"
    ],
    function(declare, _Widget, TemplatedMixin, template, template_home, template_promotion, template_about, template_history, template_message, template_media, template_gallery, template_supertruck, template_superimage, template_gallerymodal) {
        return declare("js.custom.ui.cell.carousel.Super", [_Widget, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
            	var context = this;
            	var domNode = this.domNode;
            	
            	//console.log(setting)
            	
            	var templateFrom = domAttr.get(domNode, "data-template");
            	switch(templateFrom){
	            	case "home":
	            		domAttr.set(context.imageLGNode, "src", config.baseURL+setting.image_path[0].lg);
		            	Holder.run({images: context.imageLGNode});
		            	
		            	domAttr.set(context.imageMDNode, "src", config.baseURL+setting.image_path[0].md);
		            	Holder.run({images: context.imageMDNode});
		            	
		            	domAttr.set(context.imageSMNode, "src", config.baseURL+setting.image_path[0].sm);
		            	Holder.run({images: context.imageSMNode});
		            	
		            	domAttr.set(context.imageXSNode, "src", config.baseURL+setting.image_path[0].xs);
		            	Holder.run({images: context.imageXSNode});
	            	
	            		html.set(context.titleNode, setting.title);
	            		html.set(context.desNode, setting.desc);
	            		
	            		if(setting.cta == ""){
		            		domStyle.set(context.ctaNode, "display", "none");
	            		}
	            		
	            		on(context.ctaNode, "click", function(){
	            			if(setting.cta.indexOf("http") > -1){
		            			window.open(setting.cta, "_blank");
	            			}else{
		            			window.location = config.hostURL+setting.cta;
	            			}
	            		});
	            	break;
	            	
	            	case "promotion":
	            		context.getImage(context.imageLGNode, setting.image_id, {"w":2000,"h":800});
	            		context.getImage(context.imageMDNode, setting.image_id, {"w":2000,"h":800});
	            		context.getImage(context.imageSMNode, setting.image_id, {"w":2000,"h":800});
	            		context.getImage(context.imageXSNode, setting.image_id, {"w":2000,"h":800});
	            		
	            		html.set(context.desNode, setting.promotion_description);
	            	break;
	            	
	            	case "about":
	            		html.set(context.titleNode, setting.title);
	            		html.set(context.desNode, setting.des);
	            	break;
	            	
	            	case "history":
	            		html.set(domNode, setting.des);
	            	break;
	            	
	            	case "message":
	            		html.set(domNode, setting.des);
	            	break;
	            	
	            	case "media":
	            	case "media-press":
	            		domConstruct.destroy(context.imageNode);
	            		//context.getImage(context.imageNode, setting.image_id, {"w":400,"h":250});
	            		context.getImageForbackground(context.backgroundImageNode, setting.image_id, {"w":400,"h":250});
	            		
	            		html.set(context.publishedDateNode, setting.published_date);
	            		html.set(context.titleNode, setting.title.substring(0,40)+"...");
	            		html.set(context.desNode, setting.summary.substring(0, 120)+"...");
	            		domStyle.set(context.findoutMoreNode, "display", "block");
	            		
	            		domAttr.set(context.findoutMoreNode, "href", config.hostURL+"news/"+setting.slug);
	            		
	            		/*
on(domNode, "click", function(){
		            		window.location = config.hostURL+"news/"+setting.slug;
	            		});
*/
	            	break;
	            	
	            	case "gallery":
/* 	            		console.log(setting); */
	            		domConstruct.destroy(context.imageNode);
	            		context.getImageForbackground(context.backgroundImageNode, setting.image_list[0].id, {"w":400,"h":250});
	            		html.set(context.publishedDateNode, setting.published_date);
	            		html.set(context.titleNode, setting.title);
	            		
	            		domAttr.set(context.findoutMoreNode, "href", config.hostURL+"gallery");
	            		//context.getImage(context.imageNode, setting.id, {"w":400,"h":250});
	            	break;
	            	
	            	case "truck-super":
	            		domAttr.set(context.imageLGNode, "src", setting);
	            		domAttr.set(context.imageMDNode, "src", setting);
	            		domAttr.set(context.imageSMNode, "src", setting);
	            		domAttr.set(context.imageXSNode, "src", setting);
	            	break;
	            	
	            	case "gallery-modal":
	            		context.getImageForbackground(context.imageNode, setting.id, {"w":1200,"h":800});
	            	break;
	            	
	            	case "superimage":
	            		context.getImage(context.imageLGNode, setting.image_rectangle_id, {"w":1200,"h":600});
	            		context.getImage(context.imageMDNode, setting.image_rectangle_id, {"w":1200,"h":600});
	            		context.getImage(context.imageSMNode, setting.image_rectangle_id, {"w":1200,"h":600});
	            		context.getImage(context.imageXSNode, setting.image_rectangle_id, {"w":1200,"h":600});
	            	break;
	            	
	            	default:
	            		context.getImage(context.imageLGNode, setting.image_rectangle_id, {"w":800,"h":400});
	            		context.getImage(context.imageMDNode, setting.image_rectangle_id, {"w":800,"h":400});
	            		context.getImage(context.imageSMNode, setting.image_rectangle_id, {"w":800,"h":400});
	            		context.getImage(context.imageXSNode, setting.image_rectangle_id, {"w":800,"h":400});
	            	break;
            	}
            },constructor:function(arguments){
	            var context = this;
	            
	            if(typeof arguments !== "undefined"){
		           switch(arguments.template){
			           case "home":
			           	context.templateString = template_home;
			           break;
			           
			           case "promotion":
			           	context.templateString = template_promotion;
			           break;
			           
			           case "about":
			           	context.templateString = template_about;
			           break;
			           
			           case "history":
			           	context.templateString = template_history;
			           break;
			           
			           case "message":
			           	context.templateString = template_message;
			           break;
			           
			           case "media":
			           case "media-press":
			           	context.templateString = template_media;
			           break;
			           
			           case "gallery":
			           	context.templateString = template_gallery;
			           break;
			           
			           case "truck-super":
			           	context.templateString = template_supertruck;
			           break;
			           
			           case "gallery-modal":
			           	context.templateString = template_gallerymodal;
			           break;
			           
			           case "superimage":
			           	context.templateString = template_superimage;
			           break;
			           
			           default:
			           break;
		           }
	            }
            },destroy: function() {
            	var domNode = this.domNode;
            	topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            	
                this.inherited(arguments);
            },initInteraction:function(setting){
	            var context = this;
	            var domNode = this.domNode;
	            
            },startListening:function(){
	            var context = this;
	            var domNode = this.domNode;
            },setupContent:function(){
	            var context = this;
            },getImage:function(_dom, _id, _size, _lazy){
	            var context = this;
	            
	            var w = _size.w;
	            var h = _size.h;
	            
	            var request = $.ajax({
				  url: config.hostURL+"api/image/index?id="+_id,
				  type: "GET",
				  cache: false,
				  dataType: "json"
				});
				 
				request.done(function( response ) {
					if(response.status == "success"){
						//console.log(config.resizerURL+"?width="+w+"&height="+h+"&cropratio="+w+"x"+h+"&image="+config.baseURL+response.data.image_url);
						
						if(_lazy){
							domAttr.set(_dom, "data-src", config.resizerURL+"?width="+w+"&height="+h+"&cropratio="+w+"x"+h+"&image="+config.baseURL+response.data.image_url);
						}else{
							domAttr.set(_dom, "src", config.resizerURL+"?width="+w+"&height="+h+"&cropratio="+w+"x"+h+"&image="+config.baseURL+response.data.image_url);	
						}
					}
				});
				 
				request.fail(function( jqXHR, textStatus ) {
				});
				
				request.always(function() {
				});
            },getImageForbackground:function(_dom, _id, _size, _lazy){
	            var context = this;
	            
	            var w = _size.w;
	            var h = _size.h;
	            
	            var request = $.ajax({
				  url: config.hostURL+"api/image/index?id="+_id,
				  type: "GET",
				  cache: false,
				  dataType: "json"
				});
				 
				request.done(function( response ) {
					if(response.status == "success"){
						domStyle.set(_dom, "background", "url("+config.resizerURL+"?width="+w+"&height="+h+"&cropratio="+w+"x"+h+"&image="+config.baseURL+response.data.image_url+")");
					}
				});
				 
				request.fail(function( jqXHR, textStatus ) {
				});
				
				request.always(function() {
				});
            }
        });

    });