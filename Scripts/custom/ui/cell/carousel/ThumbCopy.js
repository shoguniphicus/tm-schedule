define(["dojo/_base/declare",
        "dijit/_Widget",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/ThumbCopy.html"
    ],
    function(declare, _Widget, TemplatedMixin, template) {
        return declare("js.custom.ui.cell.carousel.ThumbCopy", [_Widget, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
            	var context = this;
            	var domNode = this.domNode;
            	
            	if(setting.image_rectangle_id != "0")
            	{
	            	context.getImage(context.imageNode, setting.image_rectangle_id);
            	}else{
	            	context.getImage(context.imageNode, setting.image_square_id);
            	}
            	
            	
            	html.set(context.titleNode, setting.feature_name);
            	html.set(context.desNode, setting.feature_description);
            },destroy: function() {
            	var domNode = this.domNode;
            	topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            	
                this.inherited(arguments);
            },initInteraction:function(setting){
	            var context = this;
	            var domNode = this.domNode;
	            
            },startListening:function(){
	            var context = this;
	            var domNode = this.domNode;
            },setupContent:function(){
	            var context = this;
            },getImage:function(dom, _id){
	            var context = this;
	            
	            var request = $.ajax({
				  url: config.hostURL+"api/image/index?id="+_id,
				  type: "GET",
				  cache: false,
				  dataType: "json"
				});
				 
				request.done(function( response ) {
					if(response.status == "success"){
						domAttr.set(dom, "src", config.resizerURL+"?width=300&height=300&cropratio=300x300&image="+config.baseURL+response.data.image_url);
/* 						domAttr.set(dom, "src", config.baseURL+response.data.image_url); */
					}
				});
				 
				request.fail(function( jqXHR, textStatus ) {
				});
				
				request.always(function() {
				});
            }
        });

    });