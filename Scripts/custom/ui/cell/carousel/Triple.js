define(["dojo/_base/declare",
        "dijit/_Widget",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Triple.html"
    ],
    function(declare, _Widget, TemplatedMixin, template) {
        return declare("js.custom.ui.cell.carousel.Triple", [_Widget, TemplatedMixin], {
            templateString: template,
            construct: function(setting, gallerydata, index) {
            	var context = this;
            	var domNode = this.domNode;
            	
            	domAttr.set(context.imageNode, "data-src", config.resizerURL+"?width=200&height=200&cropratio=200x200&image="+config.baseURL+setting.image_url);
            	
            	$(context.imageNode).on('load', function(){
            		domConstruct.destroy(context.imageNode);
	            	domStyle.set(context.realImageNode, "background-image", "url("+config.resizerURL+"?width=400&height=400&cropratio=400x400&image="+config.baseURL+setting.image_url+")");
            	});
            	
            	on(domNode, "mouseover", function(){
	            	$(context.plusNode).show();
            	});
            	
            	on(domNode, "mouseout", function(){
	            	$(".gallery-plus").hide();
            	});
            	
            	on(domNode, "click", function(){
	            	//console.log(gallerydata);
	            	
	            	var setting = new Object();
	            	setting.data = gallerydata;
	            	setting.type = "gallery";
	            	setting.index = index;
	            	
	            	$.jStorage.set("imagemodal.setting", setting);
	            	
	            	topic.publish("modal.page", "image");
            	});
            },destroy: function() {
            	var domNode = this.domNode;
            	topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            	
                this.inherited(arguments);
            },initInteraction:function(setting){
	            var context = this;
	            var domNode = this.domNode;
	            
            },startListening:function(){
	            var context = this;
	            var domNode = this.domNode;
            },setupContent:function(){
	            var context = this;
            },getImage:function(dom, _id){
	            var context = this;
	            
	            var request = $.ajax({
				  url: config.hostURL+"api/image/index?id="+_id,
				  type: "GET",
				  cache: false,
				  dataType: "json"
				});
				 
				request.done(function( response ) {
					if(response.status == "success"){
						domAttr.set(dom, "src", config.baseURL+response.data.image_url);
					}
				});
				 
				request.fail(function( jqXHR, textStatus ) {
				});
				
				request.always(function() {
				});
            }
        });

    });