define(["dojo/_base/declare",
        "dijit/_Widget",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Thumb.html"
    ],
    function(declare, _Widget, TemplatedMixin, template) {
        return declare("js.custom.ui.cell.carousel.Thumb", [_Widget, TemplatedMixin], {
            templateString: template,
            construct: function(setting, fullsetting, index) {
            	var context = this;
            	var domNode = this.domNode;
            	
            	context.getImage(context.imageSquareNode, setting.image_square_id);
            	
            	if(setting.freeform_enabled == "-1"){
            		context.destroy();
            		return;
            	}
            	
            	on(domNode, "click", function(){
            		var imageSetting = new Object();
            		imageSetting.index = index;
            		imageSetting.data = fullsetting;
            		
            		$.jStorage.set("imagemodal.setting", imageSetting);
	            	topic.publish("modal.page", "image");
            	});
            },destroy: function() {
            	var domNode = this.domNode;
            	topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            	
                this.inherited(arguments);
            },initInteraction:function(setting){
	            var context = this;
	            var domNode = this.domNode;
	            
            },startListening:function(){
	            var context = this;
	            var domNode = this.domNode;
            },setupContent:function(){
	            var context = this;
            },getImage:function(dom, _id){
	            var context = this;
	            
	            var request = $.ajax({
				  url: config.hostURL+"api/image/index?id="+_id,
				  type: "GET",
				  cache: false,
				  dataType: "json"
				});
				 
				request.done(function( response ) {
					if(response.status == "success"){
						domAttr.set(dom, "src", config.resizerURL+"?width=230&height=230&cropratio=230x230&image="+config.baseURL+response.data.image_url);
/* 						domAttr.set(dom, "src", config.baseURL+response.data.image_url); */
					}
				});
				 
				request.fail(function( jqXHR, textStatus ) {
				});
				
				request.always(function() {
				});
            }
        });

    });