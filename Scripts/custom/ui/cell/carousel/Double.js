define(["dojo/_base/declare",
        "dijit/_Widget",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Double.html",
        "dojo/text!./html/DoubleHomeNews.html"
    ],
    function(declare, _Widget, TemplatedMixin, template, template_newshome) {
        return declare("js.custom.ui.cell.carousel.Double", [_Widget, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
            	var context = this;
            	var domNode = this.domNode;
            	
            	var templateFrom = domAttr.get(domNode, "data-template");
            	
            	switch(templateFrom){
	            	case "homenews":
	            		html.set(context.titleNode, setting.title);
	            		html.set(context.copyNode, setting.summary);
	            		context.getImage(context.imageSquareNode, setting.image_id, {"w":400,"h":250});
	            		
	            		on(domNode, "click", function(){
		            		location.href = config.hostURL + "news/"+ setting.slug;
	            		});
	            	break;
	            	
	            	default:
	            		context.getImage(context.imageRectNode, setting.image_rectangle_id, {"w":400,"h":250});
	            		html.set(context.titleNode, setting.feature_name);
		            	html.set(context.copyNode, setting.feature_description);
	            	break;
            	}
            },constructor:function(arguments){
	            var context = this;
	            
	            if(typeof arguments !== "undefined"){
		           switch(arguments.template){
			           case "homenews":
			           	context.templateString = template_newshome;
			           break;
			           
			           default:
			           break;
		           }
	            }
            },destroy: function() {
            	var domNode = this.domNode;
            	topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            	
                this.inherited(arguments);
            },initInteraction:function(setting){
	            var context = this;
	            var domNode = this.domNode;
	            
            },startListening:function(){
	            var context = this;
	            var domNode = this.domNode;
            },setupContent:function(){
	            var context = this;
            },getImage:function(_dom, _id, _size){
	            var context = this;
	            
	            var w = _size.w;
	            var h = _size.h;
	            
	            var request = $.ajax({
				  url: config.hostURL+"api/image/index?id="+_id,
				  type: "GET",
				  cache: false,
				  dataType: "json"
				});
				 
				request.done(function( response ) {
					if(response.status == "success"){
						//console.log(config.resizerURL+"?width="+w+"&height="+h+"&cropratio="+w+"x"+h+"&image="+config.baseURL+response.data.image_url);
						domAttr.set(_dom, "src", config.resizerURL+"?width="+w+"&height="+h+"&cropratio="+w+"x"+h+"&image="+config.baseURL+response.data.image_url);
					}
				});
				 
				request.fail(function( jqXHR, textStatus ) {
				});
				
				request.always(function() {
				});
            }
        });

    });