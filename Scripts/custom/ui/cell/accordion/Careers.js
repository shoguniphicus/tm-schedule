define(["dojo/_base/declare",
        "dijit/_Widget",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/Careers.html"
    ],
    function(declare, _Widget, TemplatedMixin, template) {
        return declare("js.custom.ui.cell.accordion.Careers", [_Widget, TemplatedMixin], {
            templateString: template,
            construct: function(setting ,parentid, index) {
            	var context = this;
            	var domNode = this.domNode;
            	var id = domAttr.get(domNode, "id");
            	
            	var toggleID = id+index;
            	var headingID = "heading_"+id+index;
            	
            	domAttr.set(context.collapseToggleNode, "data-parent", "#"+parentid);
            	domAttr.set(context.collapseToggleNode, "href", "#"+toggleID);
            	domAttr.set(context.collapseToggleNode, "aria-controls", toggleID);
            	domAttr.set(context.collapsePanelNode, "id", toggleID);
            	
            	domAttr.set(context.panelHeadingNode, "id", headingID);
            	domAttr.set(context.collapsePanelNode, "aria-labelledby", headingID);
            	
            	html.set(context.titleNode, setting.career_title);
            	html.set(context.divisionNode, setting.career_division);
            	html.set(context.locationNode, setting.career_location);
            	html.set(context.panelBodyNode, setting.career_des);
            },destroy: function() {
            	var domNode = this.domNode;
            	topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            	
                this.inherited(arguments);
            },initInteraction:function(setting){
	            var context = this;
	            var domNode = this.domNode;
	            
            },startListening:function(){
	            var context = this;
	            var domNode = this.domNode;
            },setupContent:function(){
	            var context = this;
            }
        });

    });