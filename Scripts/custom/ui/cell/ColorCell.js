define(["dojo/_base/declare",
        "dijit/_Widget",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/ColorCell.html"
    ],
    function(declare, _Widget, TemplatedMixin, template) {
        return declare("js.custom.ui.cell.ColorCell", [_Widget, TemplatedMixin], {
            templateString: template,
            construct: function(setting, carousel_container_id, index) {
            	var context = this;
            	var domNode = this.domNode;
            	
            	domAttr.set(context.imageNode, "src", setting);
            	context.initInteraction(carousel_container_id, index);
            },destroy: function() {
            	var domNode = this.domNode;
            	topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            	
                this.inherited(arguments);
            },initInteraction:function(carousel_container_id, index){
	            var context = this;
	            var domNode = this.domNode;
	            
	            on(domNode, "click", function(){
	            	topic.publish("carousel.nav."+carousel_container_id, index);
	            });
            },startListening:function(){
	            var context = this;
	            var domNode = this.domNode;
            },setupContent:function(){
	            var context = this;
            }
        });

    });