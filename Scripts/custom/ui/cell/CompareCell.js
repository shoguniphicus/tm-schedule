define(["dojo/_base/declare",
        "dijit/_Widget",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/CompareCell.html"
    ],
    function(declare, _Widget, TemplatedMixin, template) {
        return declare("js.custom.ui.cell.CompareCell", [_Widget, TemplatedMixin], {
            templateString: template,
            construct: function(setting, index) {
            	var context = this;
            	var domNode = this.domNode;
            	
            	//console.log("setting", setting);
            	
            	if(setting == null || setting == ""){
	            	domStyle.set(context.previewContainerNode, "display", "none");
            	}else{
	            	domStyle.set(context.selectContaienrNode, "display", "none");
            	}
            	
            	context.setupContent(setting, index);
            	context.initInteraction(setting, index);
            },destroy: function() {
            	var domNode = this.domNode;
            	topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            	
                this.inherited(arguments);
            },initInteraction:function(setting, index){
	            var context = this;
	            var domNode = this.domNode;
	            
	            on(context.closeNode, "click", function(){
		            topic.publish("compare.tool.reset.index", index);
		            topic.publish("comparecell.added");
	            });
	            
	            on(context.editNode, "click", function(){
		            domStyle.set(context.previewContainerNode, "display", "none");
		            domStyle.set(context.selectContaienrNode, "display", "block");
	            });
	            
	            $(".sp-cont5").hide();
	            $(".sp-cont6").hide();
	            $(".sp-cont7").hide();
	            $(".sp-cont8").hide();
	            $(".sp-cont9").hide();
	            $(".sp-cont10").hide();
	            $(".sp-cont11").hide();
	            $(".cta-cell").hide();
	            
	            if(index == 3){
					domStyle.set(context.ph1Node, "display", "none");
					domStyle.set(context.toggle1Node, "display", "block");
					
					domStyle.set(context.ph2Node, "display", "none");
					domStyle.set(context.toggle2Node, "display", "block");
					
					domStyle.set(context.ph3Node, "display", "none");
					domStyle.set(context.toggle3Node, "display", "block");
					
					domStyle.set(context.ph4Node, "display", "none");
					domStyle.set(context.toggle4Node, "display", "block");
					
					domStyle.set(context.ph5Node, "display", "none");
					domStyle.set(context.toggle5Node, "display", "block");
					
					domStyle.set(context.ph6Node, "display", "none");
					domStyle.set(context.toggle6Node, "display", "block");
					
					domStyle.set(context.ph7Node, "display", "none");
					domStyle.set(context.toggle7Node, "display", "block");
					
					domStyle.set(context.ph8Node, "display", "none");
					domStyle.set(context.toggle8Node, "display", "block");
					
					domStyle.set(context.ph9Node, "display", "none");
					domStyle.set(context.toggle9Node, "display", "block");
					
					domStyle.set(context.ph10Node, "display", "none");
					domStyle.set(context.toggle10Node, "display", "block");
					
					domStyle.set(context.ph11Node, "display", "none");
					domStyle.set(context.toggle11Node, "display", "block");

/* 					domStyle.set(context.ctaCellNode, "display", "none"); */
					

					
					
				}else{
					domStyle.set(context.ph1Node, "display", "block");
					domStyle.set(context.toggle1Node, "display", "none");
					
					domStyle.set(context.ph2Node, "display", "block");
					domStyle.set(context.toggle2Node, "display", "none");
					
					domStyle.set(context.ph3Node, "display", "block");
					domStyle.set(context.toggle3Node, "display", "none");
					
					domStyle.set(context.ph4Node, "display", "block");
					domStyle.set(context.toggle4Node, "display", "none");
					
					domStyle.set(context.ph5Node, "display", "block");
					domStyle.set(context.toggle5Node, "display", "none");
					
					domStyle.set(context.ph6Node, "display", "block");
					domStyle.set(context.toggle6Node, "display", "none");
					
					domStyle.set(context.ph7Node, "display", "block");
					domStyle.set(context.toggle7Node, "display", "none");
					
					domStyle.set(context.ph8Node, "display", "block");
					domStyle.set(context.toggle8Node, "display", "none");
					
					domStyle.set(context.ph9Node, "display", "block");
					domStyle.set(context.toggle9Node, "display", "none");
					
					domStyle.set(context.ph10Node, "display", "block");
					domStyle.set(context.toggle10Node, "display", "none");
					
					domStyle.set(context.ph11Node, "display", "block");
					domStyle.set(context.toggle11Node, "display", "none");
				}

				// function makeCollapisble(_toggleNode, _classToCollapse, _classToManiuplate){
				// 		on(_toggleNode, "click", function(){
				// 			var currentState = domAttr.get(_toggleNode, "data-toggle");

				// 			if(currentState == "open"){
				// 				$(_classToCollapse).hide();
				// 				$(_classToManiuplate).html("+");
				// 				domAttr.set(_toggleNode, "data-toggle", "close");
				// 				//$(this).find(".glyphicon-chevron-down").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-right");
				// 			}else{
				// 				$(_classToCollapse).show();
				// 				$(_classToManiuplate).html("-");
				// 				domAttr.set(_toggleNode, "data-toggle", "open");
				// 				//$(this).find(".glyphicon-chevron-right").removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-down");
				// 			}
							
				// 			topic.publish("comparecell.added");
				// 		});
				// 	}

				// 	makeCollapisble(context.toggle1Node, ".sp-cont1", ".compare-sec-01");
				// 	makeCollapisble(context.toggle2Node, ".sp-cont2", ".compare-sec-02");
				// 	makeCollapisble(context.toggle3Node, ".sp-cont3", ".compare-sec-03");
				// 	makeCollapisble(context.toggle4Node, ".sp-cont4", ".compare-sec-04");
				// 	makeCollapisble(context.toggle5Node, ".sp-cont5", ".compare-sec-05");
				// 	makeCollapisble(context.toggle6Node, ".sp-cont6", ".compare-sec-06");
				// 	makeCollapisble(context.toggle7Node, ".sp-cont7", ".compare-sec-07");
				// 	makeCollapisble(context.toggle8Node, ".sp-cont8", ".compare-sec-08");
				// 	makeCollapisble(context.toggle9Node, ".sp-cont9", ".compare-sec-09");
				// 	makeCollapisble(context.toggle10Node, ".sp-cont10", ".compare-sec-10");
				// 	makeCollapisble(context.toggle11Node, ".sp-cont11", ".compare-sec-11");

					function makeCollapisble(_toggleNode){
						on(_toggleNode, "click", function(){
							var currentState = domAttr.get(_toggleNode, "data-toggle");
							var _classToCollapse =  "."+domAttr.get(_toggleNode, "data-class-collapse");
							var _classToManiuplate =  "."+domAttr.get(_toggleNode, "data-class-mani");

							if(currentState == "open"){
								$(_classToCollapse).hide();
								$(_classToManiuplate).find(".compare-arrow-down").removeClass("compare-arrow-down").addClass("compare-arrow-up");
								// $(_classToManiuplate).html("+");
								domAttr.set(_toggleNode, "data-toggle", "close");
							}else{
								$(_classToCollapse).show();
								// $(_classToManiuplate).html("-");
								$(_classToManiuplate).find(".compare-arrow-up").removeClass("compare-arrow-up").addClass("compare-arrow-down");
								domAttr.set(_toggleNode, "data-toggle", "open");
							}
							
							topic.publish("comparecell.added");
						});
					}


					makeCollapisble(context.toggle1Node);
					makeCollapisble(context.toggle2Node);
					makeCollapisble(context.toggle3Node);
					makeCollapisble(context.toggle4Node);
					makeCollapisble(context.toggle5Node);
					makeCollapisble(context.toggle6Node);
					makeCollapisble(context.toggle7Node);
					makeCollapisble(context.toggle8Node);
					makeCollapisble(context.toggle9Node);
					makeCollapisble(context.toggle10Node);
					makeCollapisble(context.toggle11Node);


				on(context.bookTestDriveNode, "click", function(){
					var truckId = domAttr.get(domNode, "data-truck-id");
					//console.log(truckId);

					$.jStorage.set("booktestdrive.preset", truckId);
					window.location.href = config.hostURL+"tools#testdrive";
				});

				on(context.viewVehicleNode, "click", function(){
					var truckSlug = domAttr.get(domNode, "data-truck-slug");
					var truckId = domAttr.get(domNode, "data-truck-id");
					
					window.location.href = config.hostURL+"truck/"+truckSlug;
				});
            },startListening:function(){
	            var context = this;
	            var domNode = this.domNode;
            },setupContent:function(setting, index){
	            var context = this;
	            
	            getContent(setting);
	            getAllVehicle();
	            
        		function getContent(_setting){
        		
	        		var request = $.ajax({
	        		  url: config.hostURL+"api/car/index/?id="+_setting,
	        		  type: "GET",
	        		  cache: false,
	        		  dataType: "json"
	        		});
	        		 
	        		request.done(function( response ) {
	        			if (response.status != "success") {
	        				console.warn("Error retrieving data");
	        				return;
	        			}
						
						context.populateContent(response.data, index);
						topic.publish("comparecell.added");
	        		});
	        		 
	        		request.fail(function( jqXHR, textStatus ) {
	        		});
	        		
	        		request.always(function() {
	        		});
        		}
        		
        		function getAllVehicle(){
	        		var request = $.ajax({
	                  url: config.hostURL+"api/car/get_model_type",
	                  type: "GET",
	                  cache: false,
	                  dataType: "json"
	                });
	                 
	                request.done(function( response ) {
	                    if (response.status != "success") {
	                        console.warn("Error retrieving data");
	                        return;
	                    }
	                    
	                    alltruckdata = response.data;
	                    array.forEach(response.data, function(single, index){
	                        var optionContainer = context.selectNode;
	                        var singleOption = "<option value='"+single.key+"'>"+single.value+"</option>";
	                        
	                        $(optionContainer).append(singleOption);
	                    });
	                    
	                    makeitSelectable();
	                });
	                 
	                request.fail(function( jqXHR, textStatus ) {
	                });
	                
	                request.always(function() {
	                });
	                
	                function makeitSelectable(){
	                    $(context.selectNode).change(function() {
	                        var idSelected = $(this).val();
                        
	                        if(idSelected == ""){
		                        $(context.selectVNode).prop('disabled', 'disabled');
	                        }else{
	                        	$(context.selectVNode).prop('disabled', 'disabled');
	                        	getVariant(context.selectVNode, idSelected);
	                        }
	                    });
	                    
	                    $(context.selectVNode).change(function() {
	                        var idSelected = $(this).val();
                        
							getContent(idSelected);
							domStyle.set(context.previewContainerNode, "display", "block");
							domStyle.set(context.selectContaienrNode, "display", "none");
							domStyle.set(context.ctaCellNode, "display", "block");
							$(".cta-cell").show();
							// $(".sp-cont11").show();
	                    });
	                }
        		}
        		
        		function getVariant(_el, _value){
	            	domConstruct.empty(_el);
	            	$(_el).append('<option value="">Please Select</option>');
	            	
		            var request = $.ajax({
	                  url: config.hostURL+"api/car/get_same_series?series="+_value,
	                  type: "GET",
	                  cache: true,
	                  dataType: "json"
	                });
	                 
	                request.done(function( response ) {
	                    if (response.status != "success") {
	                        console.warn("Error retrieving data");
	                        return;
	                    }
	                    
	                    alltruckdata = response.data;
	                    array.forEach(response.data, function(single, index){
	                        var optionContainer = context.selectNode;
	                        var singleOption = "<option value='"+single.id+"'>"+single.car_model_name+"</option>";
	                        
	                        $(_el).append(singleOption);
	                    });
	                    
	                    $(_el).prop('disabled', false);
	                });
	                 
	                request.fail(function( jqXHR, textStatus ) {
	                });
	                
	                request.always(function() {
	                });
	            }
            },populateContent:function(data, index){
	            var context = this;
        			
				var tsd = data; //Truck Spec Data
				var allSetting = $.jStorage.get("all_setting");
				var gearRatio = JSON.parse(tsd.gear_ratio);
				//console.log(tsd);
				//console.log("All Setting", allSetting);
				
				if(allSetting == null)
					location.reload();
				
				domStyle.set(context.ctaCellNode, "display", "block");
				context.getImage(context.thumbNode, tsd.thumb_image_id);
				
				html.set(context.modelName, tsd.car_display_name);
				html.set(context.majorSpecName, tsd.car_spec_shortname);
				
				html.set(context.gear1Node, (_.findWhere(gearRatio, {"display_name":"1st"})).modifier.toFixed(3).toString());
				html.set(context.gear2Node, (_.findWhere(gearRatio, {"display_name":"2nd"})).modifier.toFixed(3).toString());
				html.set(context.gear3Node, (_.findWhere(gearRatio, {"display_name":"3rd"})).modifier.toFixed(3).toString());
				html.set(context.gear4Node, (_.findWhere(gearRatio, {"display_name":"4th"})).modifier.toFixed(3).toString());
				html.set(context.gear5Node, (_.findWhere(gearRatio, {"display_name":"5th"})).modifier.toFixed(3).toString());
				html.set(context.gearReverseNode, (_.findWhere(gearRatio, {"display_name":"Reverse"})).modifier.toFixed(3).toString());
				html.set(context.gearFinalNode, (_.findWhere(gearRatio, {"display_name":"Final"})).modifier.toFixed(3).toString());
				
				html.set(context.transmissionTypeNode, getSettingValue(tsd.transmission));
				html.set(context.transmissionType2Node, getSettingValue(tsd.transmission));
				
				html.set(context.engineCCNode, getSettingValue(tsd.car_engine_cc));
				html.set(context.engineBoreNode, getSettingValue(tsd.bore_stroke));
				html.set(context.engineRatioNode, getSettingValue(tsd.compression_ratio));
				
				html.set(context.engineModelNode, getSettingValue(tsd.engine_model));
				html.set(context.engineTypeNode, getSettingValue(tsd.car_engine_type));
				html.set(context.maxOutputNode, getSettingValue(tsd.car_max_output));
				html.set(context.maxTorqueNode, getSettingValue(tsd.car_max_torque));
				
				html.set(context.engineModel2Node, getSettingValue(tsd.engine_model));
				html.set(context.engineType2Node, getSettingValue(tsd.car_engine_type));
				html.set(context.maxOutput2Node, getSettingValue(tsd.car_max_output));
				html.set(context.maxTorque2Node, getSettingValue(tsd.car_max_torque));
				
				html.set(context.tyreNode, getSettingValue(tsd.tyre));
				html.set(context.wheelNode, getSettingValue(tsd.wheel));
				
				html.set(context.susFrontNode, getSettingValue(tsd.suspension_front));
				html.set(context.susRearNode, getSettingValue(tsd.suspension_rear));
				html.set(context.shockNode, getSettingValue(tsd.shock_absorber));
				html.set(context.brakeFrontNode, getSettingValue(tsd.brake_front));
				html.set(context.breakRearNode, getSettingValue(tsd.brake_rear));
				
				html.set(context.steeringSystemNode, getSettingValue(tsd.steering_system));
				html.set(context.minTurningNode, tsd.turning_radius);
				
				html.set(context.tyre2Node, getSettingValue(tsd.tyre));
				html.set(context.wheel2Node, getSettingValue(tsd.wheel));
				
				html.set(context.weightNode, tsd.gross_weight);
				html.set(context.seatingNode, tsd.seating_capacity);
				html.set(context.fuelNode, tsd.fuel_tank_capacity);
				
				html.set(context.headlampNode, getSettingValue(tsd.headlamps));
				html.set(context.frontFogLampNode, getSettingValue(tsd.front_fog_lamp));
				html.set(context.engineHoodGarnishNode, getSettingValue(tsd.engine_hood_garnish));
				html.set(context.frontGrilleNode, getSettingValue(tsd.front_grille));
				html.set(context.doorOuterMirrorNode, getSettingValue(tsd.door_outer_mirror));
/* 				html.set(context.doorTailHandleNode, getSettingValue(tsd.door_tailgate_handle)); */
				
				html.set(context.doorHandleNode, getSettingValue(tsd.door_handle));
				html.set(context.tailHandleNode, getSettingValue(tsd.tailgate_handle));

				html.set(context.frontBumperGuardNode, getSettingValue(tsd.front_bumper_guard));
				html.set(context.sideBodyCladdingNode, getSettingValue(tsd.side_body_cladding));
				html.set(context.sideStepNode, getSettingValue(tsd.side_step));
				html.set(context.roofRailNode, getSettingValue(tsd.roof_rail));
				html.set(context.cargoCoverNode, getSettingValue(tsd.cargo_cover));
				html.set(context.bedLinerNode, getSettingValue(tsd.bed_liner));
				html.set(context.rearCombLampNode, getSettingValue(tsd.rear_comb_lamp));
				html.set(context.rearBumperNode, getSettingValue(tsd.rear_bumper));
				
				html.set(context.meterPanelNode, getSettingValue(tsd.meter_panel));
				html.set(context.steeringMaterialNode, getSettingValue(tsd.steering_material));
				html.set(context.dashDoorTrimNode, getSettingValue(tsd.dash_doortrim));
				html.set(context.climateControlNode, getSettingValue(tsd.climate_control));
				html.set(context.audioSystemNode, getSettingValue(tsd.audio_system));
				html.set(context.assistGripNode, getSettingValue(tsd.assist_grip));
				html.set(context.powerDoorLockNode, getSettingValue(tsd.power_door_lock));
				html.set(context.seatMaterialNode, getSettingValue(tsd.seats_material));
				html.set(context.doorWindowNode, getSettingValue(tsd.door_window));
				html.set(context.middleConsoleBoxNode, getSettingValue(tsd.middle_console));
				html.set(context.mapLampNode, getSettingValue(tsd.map_lamp));
				html.set(context.cabinLampNode, getSettingValue(tsd.cabin_lamp));
				html.set(context.ashTrayNode, getSettingValue(tsd.ashtray));
				html.set(context.powerOutletNode, getSettingValue(tsd.power_outlet));
				html.set(context.rockerPlateNode, getSettingValue(tsd.rocker_plate));
				html.set(context.cigLighterNode, getSettingValue(tsd.cigarette_lighter));
				html.set(context.carpetMatNode, getSettingValue(tsd.carpet_mat));
				
				html.set(context.srsNode, getSettingValue(tsd.srs_airbag));
				html.set(context.absNode, getSettingValue(tsd.abs_ebd));
				html.set(context.baNode, getSettingValue(tsd.brake_assist));
				html.set(context.escNode, getSettingValue(tsd.esc));
				html.set(context.tcsNode, getSettingValue(tsd.tcs));
				html.set(context.lspvNode, getSettingValue(tsd.lspv));
				html.set(context.lsdNode, getSettingValue(tsd.lsd));
				html.set(context.bodyConstNode, getSettingValue(tsd.body_construction));
				html.set(context.sideDoorImpactNode, getSettingValue(tsd.side_door_impact_beams));
				html.set(context.collapsibleSteeringNode, getSettingValue(tsd.collapsible_steering_column));
				html.set(context.windscreeNode, getSettingValue(tsd.windscreen));
				html.set(context.wiperNode, getSettingValue(tsd.wiper));
				html.set(context.seatBeltsNode, getSettingValue(tsd.seat_belts));
				html.set(context.childSeatAnchorageNode, getSettingValue(tsd.isofix));
				html.set(context.securityNode, getSettingValue(tsd.security_system));
				html.set(context.childSafetyNode, getSettingValue(tsd.child_protector_safety_lock));
				html.set(context.highMountStopLampNode, getSettingValue(tsd.high_mount_stop_lamp));
				html.set(context.reverseSensorNode, getSettingValue(tsd.reverse_sensor));

				html.set(context.ctaModelName, tsd.car_display_name);
				html.set(context.ctaMajorSpecName, tsd.car_spec_shortname);

				domAttr.set(context.truckIdNode, "data-truck-id", tsd.id);
				domAttr.set(context.truckIdNode, "data-truck-slug", tsd.slug);
				
				context.getprice(context.pricePenPrivateNode, tsd.id, "Peninsular", "Private");
				context.getprice(context.pricePenComPrivateNode, tsd.id, "Peninsular", "Company Private");
				context.getprice(context.pricePenComCommNode, tsd.id, "Peninsular", "Company Comm");
				
				context.getprice(context.priceSabahPrivateNode, tsd.id, "Sabah", "Private");
				context.getprice(context.priceSabahComPrivateNode, tsd.id, "Sabah", "Company Private");
				context.getprice(context.priceSabahComCommNode, tsd.id, "Sabah", "Company Comm");
				
				context.getprice(context.priceSarPrivateNode, tsd.id, "Sarawak", "Private");
				context.getprice(context.priceSarComPrivateNode, tsd.id, "Sarawak", "Company Private");
				context.getprice(context.priceSarComCommNode, tsd.id, "Sarawak", "Company Comm");
				
				function getSettingValue(key){
					var setting = (_.findWhere(allSetting, {"key":key}));
					
					if(typeof setting === "undefined"){
						return "-";
					}
					
					var value = setting.value;
					if(typeof value === "undefined")
						return "-";
					
					if(value == "Not Applicable (Will hide in Specs)"){
						return "N/A";
					}
					
					//console.log(key, value);
					return value;
				}
            },getImage:function(dom, _id){
	            var context = this;
	            
	            var request = $.ajax({
				  url: config.hostURL+"api/image/index?id="+_id,
				  type: "GET",
				  cache: false,
				  dataType: "json"
				});
				 
				request.done(function( response ) {
					if(response.status == "success"){
						domAttr.set(dom, "src", config.baseURL+response.data.image_url);
					}
				});
				 
				request.fail(function( jqXHR, textStatus ) {
				});
				
				request.always(function() {
				});
            },getprice:function(_el, _truckid, _region, _typeOfOwnership){
                var context = this;	
            	
                var request = $.ajax({
                  url: config.hostURL+"api/price/getprice/?car_id="+_truckid+"&region="+_region+"&type="+_typeOfOwnership,
                  type: "GET",
                  cache: false,
                  dataType: "json"
                });
                 
                request.done(function( response ) {
/*                 	console.log("Response", response); */
                	
                	if(response.status != "success"){
                		html.set(_el, "");
	                	return;
                	}
                	
                	var responseData = response.data;
                	
                	var price = parseFloat(responseData.price);
                	var inspection_fee = parseFloat(responseData.inspection_fee);
                	var insurance = parseFloat(responseData.insurance);
					var number_plate = parseFloat(responseData.number_plate);
					var ownership_claim = parseFloat(responseData.ownership_claim);
					var registration_fee = parseFloat(responseData.registration_fee);
					var road_tax = parseFloat(responseData.road_tax);
					var signwritting = parseFloat(responseData.signwritting);
                	var handling_fee = parseFloat(responseData.handling_fee);
                	
                	//var currentPrice = price+inspection_fee+insurance+number_plate+ownership_claim+registration_fee+road_tax+signwritting+handling_fee;
                	
                	var currentPrice = price+inspection_fee+number_plate+signwritting+handling_fee;
                	currentPrice *= 1.06;
                	currentPrice += ownership_claim+registration_fee+road_tax+insurance;
                	
                	try{
/* 						html.set(_el, currentPrice.toFixed(2).toLocaleString('en')); */
						var formatted = currentPrice.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').toString();
						html.set(_el, formatted);
/* 	                	html.set(_el, Number(currentPrice).toLocaleString('en')); */
                	}catch(err){
                		html.set(_el, "");
	                	console.log(err);
	                	console.log(_el);
                	}
                });
                 
                request.fail(function( jqXHR, textStatus ) {
					
                });
                
                request.always(function() {
                });
            }
        });

    });