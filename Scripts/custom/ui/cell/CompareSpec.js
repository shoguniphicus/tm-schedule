define(["dojo/_base/declare",
        "dijit/_Widget",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/CompareSpec.html"
    ],
    function(declare, _Widget, TemplatedMixin, template) {
        return declare("js.custom.ui.cell.CompareSpec", [_Widget, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
            	var context = this;
            	var domNode = this.domNode;
            	
            	context.startListening();
            },destroy: function() {
            	var domNode = this.domNode;
            	topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            	
                this.inherited(arguments);
            },initInteraction:function(setting){
	            var context = this;
	            var domNode = this.domNode;
	            
            },startListening:function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            var resizeHandle = topic.subscribe("update.window.resize", resizeSpecColumn);
	            var specAddedHandle = topic.subscribe("comparecell.added", resizeSpecColumn);
	            
	            function resizeSpecColumn(){
	            	var allSpecArray = new Array(".t-sunmmary-features", ".sf-transmission_type", ".sf-engine_model", ".sf-engine_type", ".sf-max_output", ".sf-max_torque", ".sf-tyres", ".sf-wheels", ".t-engine", ".e-engine_model", ".e-engine_type", ".e-displacement", ".e-bore", ".e-compress_ratio", ".e-max_output", ".e-max_torque", ".e-fuel_system", ".t-transmission", ".trans-type", ".trans-gear_ratio1", ".trans-gear_ratio2", ".trans-gear_ratio3", ".trans-gear_ratio4", ".trans-gear_ratio5", ".trans-reverse", ".trans-final", ".t-suspension_brake", ".sb-sus_front", ".sb-sus_rear", ".sb-shock", ".sb-brake_front", ".sb-brake_rear", ".t-steering", ".steering-system", ".steering-min_turning_radius", ".t-tyres_wheels", ".tw-tyres", ".tw-wheels", ".t-weight_capacities", ".wc-weight", ".wc-seating", ".wc-fuel", ".t-exterior_features", ".ef-headlamp", ".ef-front_fog_lamp", ".ef-engine_hood_garnish", ".ef-front_grille", ".ef-door_outer_mirror", ".ef-door_handle", ".ef-tail_handle", ".ef-front_bumper_guard", ".ef-side_body_cladding", ".ef-side_step", ".ef-roof_rail", ".ef-cargo_cover", ".ef-bed_liner", ".ef-rear_comb_lamp", ".ef-rear_bumper", ".t-interior_features", ".if-meter_panel", ".if-steering_material", ".if-dash_door_trim", ".if-climate_control", ".if-audio_system", ".if-assist_grip", ".if-power_door_lock", ".if-seats_material", ".if-door_window", ".if-middle_console_box", ".if-map_lamp", ".if-cabin_lamp", ".if-ash_tray", ".if-power_outlet", ".if-rocker_plate", ".if-cigarette_lighter", ".if-carpet_mat", ".t-safety", ".safety-srs", ".safety-abs_ebd", ".safety-brake_assist", ".safety-esc", ".safety-tcs", ".safety-lspv", ".safety-lsd", ".safety-body_construction", ".safety-side_door_impact_beams", ".safety-collapsibile_steering", ".safety-windscreen", ".safety-wiper", ".safety-seat_belt", ".safety-child_seat_anchorage", ".safety-security", ".safety-child_safety_lock", ".safety-high_mount_stop_lamp", ".safety-reverse_sensor", ".t-price", ".price-p_peninsular", ".price-p_sabah", ".price-p_sarawak", ".price-cp_peninsular", ".price-cp_sabah", ".price-cp_sarawak", ".price-cc_peninsular", ".price-cc_sabah", ".price-cc_sarawak");
	            	
	            	array.forEach(allSpecArray, function(singleClassString, index){
	            		var maxHeightEl = _.max($(singleClassString), function(single){ return $(single).height(); });
	            		try{
                            $(singleClassString).css("min-height", $(maxHeightEl).height()+"px");
                        }catch(err){
                            console.log("Cannot find class", singleClassString)
                        }

	            		//console.log(singleClassString, $(maxHeightEl).height()+"px");
	            	});
	            }
	            
				topic.publish("topic.handlers.add", resizeHandle, domAttr.get(domNode, "id"));
				topic.publish("topic.handlers.add", specAddedHandle, domAttr.get(domNode, "id"));
            },setupContent:function(){
	            var context = this;
            }
        });

    });