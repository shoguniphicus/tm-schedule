define(["dojo/_base/declare",
        "dijit/_Widget",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/NewsYearCell.html"
    ],
    function(declare, _Widget, TemplatedMixin, template) {
        return declare("js.custom.ui.cell.NewsYearCell", [_Widget, TemplatedMixin], {
            templateString: template,
            construct: function(setting, index) {
            	var context = this;
            	var domNode = this.domNode;
            	
            	if(index == 0){
	            	domClass.add(domNode, "active");
            	}
            	
            	html.set(domNode, setting+"");
            	
            	context.startLitening();
                context.initInteraction(setting);
             },destroy: function() {
            	var domNode = this.domNode;
            	topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            	
                this.inherited(arguments);
            }, startLitening:function(){
	             var context = this;
	             var domNode = this.domNode;
	             
	             var activeHandle = topic.subscribe("news.sidebar.change.year", function(){
	             	if(domClass.contains(context.yearTriggerNode, "active"))
                         domClass.remove(context.yearTriggerNode, "active");
				});
				topic.publish("topic.handlers.add", activeHandle, domAttr.get(domNode, "id"));
             }, initInteraction:function(setting){
                 var context = this;
                 var domNode = this.domNode;
                
                 on(context.yearTriggerNode, "click", function(){

                     topic.publish("news.sidebar.change.year", setting);
                     domClass.add(context.yearTriggerNode, "active");
                 });
            }
        });

    });