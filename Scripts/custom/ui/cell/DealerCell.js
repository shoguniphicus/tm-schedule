define(["dojo/_base/declare",
        "dijit/_Widget",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/DealerCell.html"
    ],
    function(declare, _Widget, TemplatedMixin, template) {
        return declare("js.custom.ui.cell.DealerCell", [_Widget, TemplatedMixin], {
            templateString: template,
            dom:function(setting){
                var context = this;
                var domNode = this.domNode;
                
                html.set(context.nameNode, setting.dealer_name);
                html.set(context.addressNode, setting.dealer_address);
                html.set(context.telNode, setting.dealer_tel);
                html.set(context.faxNode, setting.dealer_fax);
                html.set(context.emailNode, setting.dealer_email);
                html.set(context.webNode, setting.dealer_web);
                html.set(context.openingNode, setting.opening_hour);
                
                if(setting.dealer_email == ""){
                	domConstruct.destroy(context.emailNodeLabel);
                }
                
                 if(setting.dealer_web == ""){
                 	domConstruct.destroy(context.webNodeLabel);
                }
                
                var splitFSTel = setting.dealer_tel.split("/");
                var splitUNTel = splitFSTel[0].split("(");
/*                 domAttr.set(context.telAbleNode,  "href", "tel:"+splitUNTel[0]); */
                
                domClass.remove(context.innerNode, "dealer-cell-inner");
                domClass.remove(domNode, "col-lg-4");
                domClass.remove(domNode, "col-xs-12");
                domClass.remove(domNode, "col-sm-6");
                domClass.remove(domNode, "col-md-4");
                
                domConstruct.destroy(context.booktestDriveNode);
                domConstruct.destroy(context.viewMapNode);
                
                return domNode;
            },construct: function(setting) {
                var context = this;
                var domNode = this.domNode;
                
                html.set(context.nameNode, setting.dealer_name);
                html.set(context.addressNode, setting.dealer_address);
/*              html.set(context.telNode, setting.dealer_tel); */
                html.set(context.faxNode, setting.dealer_fax);
                html.set(context.emailNode, setting.dealer_email);
                html.set(context.webNode, setting.dealer_web);
                html.set(context.openingNode, setting.opening_hour);

                var splitFSTel = setting.dealer_tel.split("/");
                var splitUNTel = splitFSTel[0].split("(");
/*              domAttr.set(context.telAbleNode,  "href", "tel:"+splitUNTel[0]); */
                
                array.forEach(splitFSTel, function(single, index){
                    var el = '<a href="tel:'+single.split('(')[0].trim()+'"><span>'+single.trim()+'</span></a><span class="split-comma">, </span>';
                    
                    $(context.telNode).append(el);
                });

                if(setting.dealer_tel == ""){
                    domStyle.set(context.telNodeLabel, "display", "none");
                }
                if(setting.dealer_fax == ""){
                    domStyle.set(context.faxNodeLabel, "display", "none");
                }
                if(setting.dealer_email == ""){
                    domStyle.set(context.emailNodeLabel, "display", "none");
                }
                if(setting.dealer_web == ""){
                    domStyle.set(context.webNodeLabel, "display", "none");
                }
                if(setting.opening_hour == ""){
                    domStyle.set(context.openingNodeLabel, "display", "none");
                }
                
                if(setting.is_service == "1"){
                    domStyle.set(context.isServiceNode, "display", "block");
                }
                
                if(setting.is_showroom == "1"){
                    domStyle.set(context.isShowRoomNode, "display", "block");
                    domStyle.set(context.booktestDriveNode, "display", "inline-block");
                    domStyle.set(context.booktestDriveNode, "display", "inline-block");
                }
                
                if(setting.is_spareparts == "1"){
                    domStyle.set(context.isSparepartNode, "display", "block");
                }

                domAttr.set(context.emailNode, "href", "mailto:"+setting.dealer_email);
                domAttr.set(context.webNode, "href", setting.dealer_web);
                
                context.initInteraction(setting);
                
                if(setting.dealer_coord_lat == 0){
                    domConstruct.destroy(context.viewMapNode);
                    return;
                }
            },destroy: function() {
                var domNode = this.domNode;
                topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
                
                this.inherited(arguments);
            },initInteraction:function(setting){
	            var context = this;
	            var domNode = this.domNode;
	            
	            on(context.viewMapNode, "click", function(){
	            	$.jStorage.set("googlemap.coord.modal", setting);
		            topic.publish("modal.page", "googlemap");
	            });
	            
	            on(context.booktestDriveNode, "click", function(){
		            window.location.href = config.hostURL+"tools#testdrive";
	            });
            },startListening:function(){
                var context = this;
                var domNode = this.domNode;
            },setupContent:function(){
                var context = this;
            }
        });

    });