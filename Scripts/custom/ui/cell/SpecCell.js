define(["dojo/_base/declare",
        "dijit/_Widget",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/SpecCell.html"
    ],
    function(declare, _Widget, TemplatedMixin, template) {
        return declare("js.custom.ui.cell.SpecCell", [_Widget, TemplatedMixin], {
            templateString: template,
            construct: function(_id, _column, _index) {
            	var context = this;
            	var domNode = this.domNode;
            	var id = domAttr.get(domNode, "id");
            	
            	//domClass.add(domNode, "col-lg-"+_column);
            	// domClass.add(domNode, "col-md-"+_column);
            	// domClass.add(domNode, "col-sm-"+_column);
            	// domClass.add(domNode, "col-xs-"+_column);
            	context.startListening(_index);
            	context.getTruck(_id, _column, _index);
            	
            	setTimeout(function(){
	            	topic.publish("speccell.added");
            	}, 500);
            },destroy: function() {
            	var domNode = this.domNode;
            	topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            	
                this.inherited(arguments);
            },getTruck:function(_id, _column, _index){
	            var context = this;
	            
	            var request = $.ajax({
                  url: config.hostURL+"api/car/index/?id="+_id,
                  type: "GET",
                  cache: false,
                  dataType: "json"
                });
                 
                request.done(function( response ) {
                    if (response.status != "success") {
                        console.warn("Error retrieving data");
                        return;
                    }
                    
                    context.setupContent(response.data);
                });
                 
                request.fail(function( jqXHR, textStatus ) {
                });
                
                request.always(function() {
                    
                }); 
            },initInteraction:function(setting){
	            var context = this;
	            var domNode = this.domNode;
	            
            },startListening:function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            var expandHandle = topic.subscribe("spec.expand", function(){
	            	domStyle.set(context.summaryContainerNode, "display", "none");
	            	domStyle.set(context.expandContainerNode, "display", "block");
				});
				topic.publish("topic.handlers.add", expandHandle, domAttr.get(domNode, "id"));
				
				var implodeHandle = topic.subscribe("spec.implode", function(){
					domStyle.set(context.summaryContainerNode, "display", "block");
	            	domStyle.set(context.expandContainerNode, "display", "none");
				});
				topic.publish("topic.handlers.add", implodeHandle, domAttr.get(domNode, "id"));
            },setupContent:function(setting){
	            var context = this;
	            var allSetting = $.jStorage.get("all_setting");
	            var tsd = setting;
	            var gearRatio = JSON.parse(tsd.gear_ratio);
	            
	            html.set(context.nameNode, setting.car_spec_shortname);
	            
	            html.set(context.gear1Node, (_.findWhere(gearRatio, {"display_name":"1st"})).modifier.toFixed(3).toString());
				html.set(context.gear2Node, (_.findWhere(gearRatio, {"display_name":"2nd"})).modifier.toFixed(3).toString());
				html.set(context.gear3Node, (_.findWhere(gearRatio, {"display_name":"3rd"})).modifier.toFixed(3).toString());
				html.set(context.gear4Node, (_.findWhere(gearRatio, {"display_name":"4th"})).modifier.toFixed(3).toString());
				html.set(context.gear5Node, (_.findWhere(gearRatio, {"display_name":"5th"})).modifier.toFixed(3).toString());
				html.set(context.gearReverseNode, (_.findWhere(gearRatio, {"display_name":"Reverse"})).modifier.toFixed(3).toString());
				html.set(context.gearFinalNode, (_.findWhere(gearRatio, {"display_name":"Final"})).modifier.toFixed(3).toString());
				
				html.set(context.transmissionTypeNode, getSettingValue(tsd.transmission));
				html.set(context.transmissionType2Node, getSettingValue(tsd.transmission));
				
				html.set(context.engineCCNode, getSettingValue(tsd.car_engine_cc));
				html.set(context.engineBoreNode, getSettingValue(tsd.bore_stroke));
				html.set(context.engineRatioNode, getSettingValue(tsd.compression_ratio));
				
				html.set(context.engineModelNode, getSettingValue(tsd.engine_model));
				html.set(context.engineTypeNode, getSettingValue(tsd.car_engine_type));
				html.set(context.maxOutputNode, getSettingValue(tsd.car_max_output));
				html.set(context.maxTorqueNode, getSettingValue(tsd.car_max_torque));
				
				html.set(context.engineModel2Node, getSettingValue(tsd.engine_model));
				html.set(context.engineType2Node, getSettingValue(tsd.car_engine_type));
				html.set(context.maxOutput2Node, getSettingValue(tsd.car_max_output));
				html.set(context.maxTorque2Node, getSettingValue(tsd.car_max_torque));
				
				html.set(context.tyreNode, getSettingValue(tsd.tyre));
				html.set(context.wheelNode, getSettingValue(tsd.wheel));
				
				html.set(context.susFrontNode, getSettingValue(tsd.suspension_front));
				html.set(context.susRearNode, getSettingValue(tsd.suspension_rear));
				html.set(context.shockNode, getSettingValue(tsd.shock_absorber));
				html.set(context.brakeFrontNode, getSettingValue(tsd.brake_front));
				html.set(context.breakRearNode, getSettingValue(tsd.brake_rear));
				
				html.set(context.steeringSystemNode, getSettingValue(tsd.steering_system));
				html.set(context.minTurningNode, tsd.turning_radius);
				
				html.set(context.tyre2Node, getSettingValue(tsd.tyre));
				html.set(context.wheel2Node, getSettingValue(tsd.wheel));
				
				html.set(context.weightNode, tsd.gross_weight);
				html.set(context.seatingNode, tsd.seating_capacity);
				html.set(context.fuelNode, tsd.fuel_tank_capacity);
				
				setOrHideRow(context.headlampNode, tsd.headlamps);
				setOrHideRow(context.frontFogLampNode, tsd.front_fog_lamp);
				setOrHideRow(context.engineHoodGarnishNode, tsd.engine_hood_garnish);
				setOrHideRow(context.frontGrilleNode, tsd.front_grille);
				setOrHideRow(context.doorOuterMirrorNode, tsd.door_outer_mirror);
				setOrHideRow(context.doorHandleNode, tsd.door_handle);
				setOrHideRow(context.tailHandleNode, tsd.tailgate_handle);
				setOrHideRow(context.frontBumperGuardNode, tsd.front_bumper_guard);
				setOrHideRow(context.sideBodyCladdingNode, tsd.side_body_cladding);
				setOrHideRow(context.sideStepNode, tsd.side_step);
				setOrHideRow(context.roofRailNode, tsd.roof_rail);
				setOrHideRow(context.cargoCoverNode, tsd.cargo_cover);
				setOrHideRow(context.bedLinerNode, tsd.bed_liner);
				setOrHideRow(context.rearCombLampNode, tsd.rear_comb_lamp);
				setOrHideRow(context.rearBumperNode, tsd.rear_bumper);
				setOrHideRow(context.rearSpoilerNode, tsd.rear_spoiler);
				setOrHideRow(context.tailgateGarnishNode, tsd.tailgate_garnish);
				setOrHideRow(context.mufflerCutterNode, tsd.muffler_cutter);
				
				setOrHideRow(context.meterPanelNode, tsd.meter_panel);
				setOrHideRow(context.steeringMaterialNode, tsd.steering_material);
				setOrHideRow(context.dashDoorTrimNode, tsd.dash_doortrim);
				setOrHideRow(context.climateControlNode, tsd.climate_control);
				setOrHideRow(context.climateControlRearNode, tsd.climate_control_rear_blower);
				
				setOrHideRow(context.audioSystemNode, tsd.audio_system);
				setOrHideRow(context.rearDVDNode, tsd.dvd_monitor);
				setOrHideRow(context.command4wdNode, tsd.terrain_command_4wd_system);
				
				
				setOrHideRow(context.assistGripNode, tsd.assist_grip);
				setOrHideRow(context.powerDoorLockNode, tsd.power_door_lock);
				
				setOrHideRow(context.seatMaterialNode, tsd.seats_material);
				setOrHideRow(context.seatFrontNode, tsd.seats_front);
				setOrHideRow(context.seat2RowNode, tsd.seats_2ndrow);
				setOrHideRow(context.seat3RowNode, tsd.seats_3rdrow);
				
				setOrHideRow(context.doorWindowNode, tsd.door_window);
				setOrHideRow(context.middleConsoleBoxNode, tsd.middle_console);
				setOrHideRow(context.mapLampNode, tsd.map_lamp);
				setOrHideRow(context.cabinLampNode, tsd.cabin_lamp);
				setOrHideRow(context.ashTrayNode, tsd.ashtray);
				setOrHideRow(context.powerOutletNode, tsd.power_outlet);
				setOrHideRow(context.rockerPlateNode, tsd.rocker_plate);
				setOrHideRow(context.cigLighterNode, tsd.cigarette_lighter);
				
				setOrHideRow(context.cargoOrganizerNode, tsd.cargo_organizer_box);
				setOrHideRow(context.tonneCoverNode, tsd.tonneau_cover);
				
				setOrHideRow(context.carpetMatNode, tsd.carpet_mat);
				
				
				setOrHideRow(context.srsNode, tsd.srs_airbag);
				setOrHideRow(context.absNode, tsd.abs_ebd);
				setOrHideRow(context.baNode, tsd.brake_assist);
				setOrHideRow(context.escNode, tsd.esc);
				setOrHideRow(context.tcsNode, tsd.tcs);
				
				setOrHideRow(context.lspvNode, tsd.lspv);
				setOrHideRow(context.lsdNode, tsd.lsd);
				setOrHideRow(context.bodyConstNode, tsd.body_construction);
				setOrHideRow(context.sideDoorImpactNode, tsd.side_door_impact_beams);
				setOrHideRow(context.collapsibleSteeringNode, tsd.collapsible_steering_column);
				setOrHideRow(context.windscreeNode, tsd.windscreen);
				
				setOrHideRow(context.wiperNode, tsd.wiper);
				setOrHideRow(context.wiperRearNode, tsd.wiper_rear);
				
				setOrHideRow(context.seatBeltsNode, tsd.seat_belts);
				setOrHideRow(context.childSeatAnchorageNode, tsd.isofix);
				setOrHideRow(context.securityNode, tsd.security_system);
				setOrHideRow(context.childSafetyNode, tsd.child_protector_safety_lock);
				
				setOrHideRow(context.highMountStopLampNode, tsd.high_mount_stop_lamp);
				setOrHideRow(context.reverseSensorNode, tsd.reverse_sensor);	            
	            
	            function setOrHideRow(_node, _valueKey){
		            var value = getSettingValue(_valueKey);
		            
		            if(value == ""){
			            var sharedClass = $(_node).attr('class');
			            $('.'+sharedClass).parent().remove();
		            }else{
			            html.set(_node, value);
		            }
	            }
	            
	            function getSettingValue(key){
					var setting = (_.findWhere(allSetting, {"key":key}));
					
					if(typeof setting === "undefined"){
						return "-";
					}
					
					var value = setting.value;
					if(typeof value === "undefined")
						return "-";
					
					if(value == "Not Applicable (Will hide in Specs)"){
						return "";
					}
					
					//console.log(key, value);
					return value;
				}
            }
        });

    });