define(["dojo/_base/declare",
        "dijit/_Widget",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/AccessoriesCell.html"
    ],
    function(declare, _Widget, TemplatedMixin, template) {
        return declare("js.custom.ui.cell.AccessoriesCell", [_Widget, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
            	var context = this;
            	var domNode = this.domNode;
            	
            	context.getImage(context.imageNode, setting.image_id);
            	html.set(context.titleNode, setting.accs_name);
            },destroy: function() {
            	var domNode = this.domNode;
            	topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            	
                this.inherited(arguments);
            },initInteraction:function(setting){
	            var context = this;
	            var domNode = this.domNode;
	            
            },startListening:function(){
	            var context = this;
	            var domNode = this.domNode;
            },setupContent:function(){
	            var context = this;
            },getImage:function(dom, _id){
	            var context = this;
	            
	            var request = $.ajax({
				  url: config.hostURL+"api/image/index?id="+_id,
				  type: "GET",
				  cache: false,
				  dataType: "json"
				});
				 
				request.done(function( response ) {
					if(response.status == "success"){
						domAttr.set(dom, "src", config.resizerURL+"?width=400&height=200&cropratio=400x200&image="+config.baseURL+response.data.image_url);
					}
				});
				 
				request.fail(function( jqXHR, textStatus ) {
				});
				
				request.always(function() {
				});
            }
        });

    });