define(["dojo/_base/declare",
        "dijit/_Widget",
        "dijit/_TemplatedMixin",
        "dojo/text!./html/PriceCell.html"
    ],
    function(declare, _Widget, TemplatedMixin, template) {
        return declare("js.custom.ui.cell.PriceCell", [_Widget, TemplatedMixin], {
            templateString: template,
            construct: function(_id, _name, _region, _type, _column, _index){
            	var context = this;
            	var domNode = this.domNode;
            	var id = domAttr.get(domNode, "id");
            	
            	context.getContent(_id, _name, _region, _type, _column, _index);
            	
            	/*
setTimeout(function(){
	            	topic.publish("speccell.added");
            	}, 500);
*/
            },destroy: function() {
            	var domNode = this.domNode;
            	topic.publish("topic.handlers.destroy", domAttr.get(domNode, "id"));
            	
                this.inherited(arguments);
            },initInteraction:function(setting){
	            var context = this;
	            var domNode = this.domNode;
	            
            },startListening:function(){
	            var context = this;
	            var domNode = this.domNode;
	            
	            
            },getContent: function(_id, _name, _region, _type, _column, _index){
	            var context = this;
	            var domNode = this.domNode;
	            
	             var request = $.ajax({
                  url: config.hostURL+"api/price/getprice/?car_id="+_id+"&region="+_region+"&type="+_type,
                  type: "GET",
                  cache: false,
                  dataType: "json"
                });
                 
                request.done(function( response ) {
                    if (response.status != "success") {
                        console.warn("Error retrieving data");
                        context.destroy();
                        return;
                    }
                    
                    //priceCell.construct(response.data, _name, _type, _column, _index);
                    //construct: function(setting, _name, _type, _column, _index) 
                    
                    context.setupContent(response.data, _name, _type);
	            	context.startListening(_index);
                });
                 
                request.fail(function( jqXHR, textStatus ) {
                });
                
                request.always(function() {
                	setTimeout(function(){
                        topic.publish("speccell.added");
                    }, 500);
                });
            },setupContent:function(setting, _name, _type){
	            var context = this;
	            
	            var beforeGST = parseFloat(setting.price) + parseFloat(setting.handling_fee) + parseFloat(setting.number_plate) + parseFloat(setting.signwritting) + parseFloat(setting.inspection_fee);
	            var gst = beforeGST * 0.06;
	            	
	            gst = gst.toFixed(2);
	            	
	            var afterGST = beforeGST * 1.06;
	            
	            var otrWithoutIns = afterGST+ parseFloat(setting.road_tax) + parseFloat(setting.registration_fee) + parseFloat(setting.ownership_claim);
	            var totalOTRWI = parseFloat(setting.insurance) + otrWithoutIns;
	            
	            switch(_type){
		            case "Private":
		            	 html.set(context.nameNode, "Individual Private");
		            break;
		            
		            case "Company Private":
		            	 html.set(context.nameNode, "Company Private");
		            break;
		            
		            case "Company Comm":
		            	 html.set(context.nameNode, "Company Commercial");
		            break;
		            
		            default:
		            break;
	            }
	            
	            
	            //setting.price.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,').toString()
				
				html.set(context.priceNode, Number(setting.price).toLocaleString('en', {'minimumFractionDigits': 2 }));
				html.set(context.roadTaxNode, Number(setting.road_tax).toLocaleString('en', {'minimumFractionDigits': 2 }));
				
	            html.set(context.registrationNode, Number(setting.registration_fee).toFixed(2));
	            html.set(context.ownershipNode, Number(setting.ownership_claim).toFixed(2));
	            html.set(context.handlingNode, Number(setting.handling_fee).toFixed(2));
	            html.set(context.plateNode, Number(setting.number_plate).toFixed(2));
	            html.set(context.signNode, Number(setting.signwritting).toFixed(2));
	            html.set(context.inspectionNode, Number(setting.inspection_fee).toFixed(2));
	            
	            html.set(context.netSellingPreGSTNode, Number(beforeGST).toLocaleString('en', {'minimumFractionDigits': 2 }));
	            html.set(context.gstNode, Number(gst).toLocaleString('en', {'minimumFractionDigits': 2 }));
	            html.set(context.netSellingPostGSTNode, Number(afterGST).toLocaleString('en', {'minimumFractionDigits': 2 }));
	            
	            html.set(context.otdWOI, Number(otrWithoutIns.toFixed(2)).toLocaleString('en', {'minimumFractionDigits': 2 }));
	            html.set(context.insuranceNode, Number(setting.insurance).toLocaleString('en', {'minimumFractionDigits': 2 }));
	            html.set(context.otdWI, Number(totalOTRWI.toFixed(2)).toLocaleString('en', {'minimumFractionDigits': 2 }));
	            html.set(context.insuredAmountNode, Number(setting.insured_amount).toLocaleString('en', {'minimumFractionDigits': 2 }));
            }
        });

    });