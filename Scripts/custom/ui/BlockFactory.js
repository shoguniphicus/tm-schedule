define([
        "declare",
        "dojo/topic",
        "dojo/_base/array",
        "Scripts/custom/ui/blocks/NavigationController"
    ],

    function(declare, topic, array, NavigationController) {

        var block = declare("js.custom.ui.BlockFactory", [], {});
        block.nitro = function(keypairsData, restricted){
        	
            array.forEach(keypairsData, function(single, i){
                try{
                    var kuriString = single.kuri;
                    var knString = single.kn;

                    require([kuriString],function(knString){
                        //console.log("Nitro Load", kuriString);
                    });
                }catch(err){
                    console.warn("Nitro cannot load", err);
                }
            });
        };

        block.create = function(objType, container, keypairsData, restricted) {
            var module_name, module, obj;
            var _objType = objType;
            var key = objType;

            /**
            *
            * Roadblock for login state
            *
            **/
            var isLogin = $.jStorage.get("app.login");
            if(!isLogin){
                var bypassList = [  "navigationcontroller"
                                 ];
                var isBypass = false;

                array.forEach(bypassList, function(single, index){
                    if(_objType == single){
                        isBypass = true;
                    }
                });

                /*
                if(!isBypass)
                {
                    if(_objType != "login")
                    {
                        window.location.href = window.location.pathname+"?p=login";
                    }
                }
                */
            }

            /**
            *
            * This is to make sure once you logged in, you will not see the login page again
            * You will only be greeted with login when you logout, resume app, or timeout
            **/
            if(_objType == "login"){
                if(isLogin){
                    _objType = "home";
                }
            }

            if(_objType == "navigationcontroller"){
                obj = new NavigationController();
                obj.placeAt(container);

                topic.publish("navigationcontroller.created", obj);
                return obj;
            }

            var uri = "";
            var constructor_optional = null;
            var construct_optional = "";
			
			var keymatch = _.findWhere(keypairsData, {kn: key});
			
			if(typeof keymatch === "undefined"){
				
				if(_.contains(restricted, key)){
					topic.publish("navigation.page", "403");
					topic.publish("modal.page", "prompt");
					
					topic.publish("internal.tracking", "page|403|view|"+key);
					topic.publish("analytics.page", "/?p="+key, "403 - No Access");
					
					return;
				}
			
				topic.publish("internal.tracking", "page|404|view|"+key);
				topic.publish("analytics.page", "/?p="+key, "404 - No Access");
				topic.publish("navigation.page.jump", "home");
				
				return;
			}else{
				array.forEach(keypairsData, function(single, i){
	                if(single.kn == key)
	                {
	                    uri = single.kuri;
	                    construct_optional = single.c;
	                    constructor_optional = single.optionals;
	                }
	            });
	
				require([uri],function(_block){
	                try{
		                obj = new _block(constructor_optional);
		                obj.placeAt(container);
	                }catch(err){
		                console.log("Err blockfactory", err);
	                }
					
	                topic.publish("block.created.xhr.require", obj, construct_optional);
	            });	
			}
			
            return obj;
        }

        return block;
    });