define([
        "dojo/_base/declare",
        "dojo/topic",
        "dojo/_base/array"
    ],

    function(declare, topic, array) {

        var block = declare("js.custom.ui.ModalFactory", [], {});

        block.create = function(objType, container) {
            var module_name, module, obj;
            var _objType = objType;

            switch (_objType) {
                /*
                * Modals
                */
                
                case "feature": require(["js/custom/ui/admin/modal/FeatureModal"], function(_modal){  obj = new _modal(); topic.publish("modal.created.xhr.require", obj); });break;
                case "featurefreeform": require(["js/custom/ui/admin/modal/FeatureFreeformModal"], function(_modal){  obj = new _modal(); topic.publish("modal.created.xhr.require", obj); });break;
                case "featurecrop" : require(["js/custom/ui/admin/modal/FeatureCrop"], function(_modal){  obj = new _modal(); topic.publish("modal.created.xhr.require", obj); });break;
                case "featuresinglecrop" : require(["js/custom/ui/admin/modal/FeatureSingleUploadCrop"], function(_modal){  obj = new _modal(); topic.publish("modal.created.xhr.require", obj); });break;
                
                case "googlemap": require(["js/custom/ui/modal/GoogleMap"], function(_modal){  obj = new _modal(); topic.publish("modal.created.xhr.require", obj); });break;
                case "image": require(["js/custom/ui/modal/ImageModal"], function(_modal){  obj = new _modal(); topic.publish("modal.created.xhr.require", obj); });break;
                case "iframe": require(["js/custom/ui/modal/IframeModal"], function(_modal){  obj = new _modal(); topic.publish("modal.created.xhr.require", obj); });break;
                
                default:
                    console.warn("No modal is selected");
                break;
            }

            return obj;
        }

        return block;
    });