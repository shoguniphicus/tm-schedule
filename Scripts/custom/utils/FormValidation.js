define(["dojo/_base/declare",
 		"dijit/_Widget",
 		"dojo/_base/array",
 		"dojo/dom-style",
 		"dojo/dom-attr",
 		"dojo/_base/array",
		"dojo/topic",
		"js/custom/ui/alert/ErrorWarning"], 
        function(declare,_Widget, array, domStyle, domAttr, array, topic, ErrorWarning) {

	return declare("js.custom.utils.FormValidation", null,{
		validate:function(inputfield, unique_identifier, params){
			
			unique_identifier = unique_identifier+domAttr.get(inputfield, "name");

			var error = new Array();
			array.forEach(params, function(single, i){
				var value = domAttr.get(inputfield, "value");
				switch(single){
					case "required":
						if(isEmpty(value))
							error.push("required");
					break;
					
					case "numberonly":
						if(isNaN(value))
							error.push("numberonly");
					break;
					
					case "maxlength":
					break;
					
					case "minlength":
					break;
					
					case "email":
						if(!validateEmail(value))
							error.push("email");
					break;
					
					default:
					break;
				}
			});
			
			function isEmpty(_value){
				if(_value == ""){
					return true;
				}else {
					return false;
				}
			}
			
			function validateEmail(email) {
			    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			    return re.test(email);
			}
			
			if(error.length >= 1){
				$("."+unique_identifier).remove();

				var errorWarning = new ErrorWarning();
				var errorSetting = new Object();
				errorSetting.unique_identifier = unique_identifier;
				errorWarning.construct(errorSetting);
				$(inputfield).after(errorWarning.domNode);

				$(inputfield).on("input change", function(){
					$("."+unique_identifier).remove();
				});

				return true;
			}

			$(inputfield).off();
			return false;
		}
	});
});