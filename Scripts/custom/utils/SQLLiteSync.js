define(["dojo/_base/declare",
 		"dijit/_Widget",
 		"dojo/_base/array",
 		"dojo/dom-style",
 		"dojo/dom-attr",
		"dojo/topic"], 
        function(declare,_Widget, array, domStyle, domAttr, topic) {

	return declare("js.custom.utils.SQLLiteSync", null,{
		initDB: function(tablename){
			var db = null;
			try{
				var db = window.sqlitePlugin.openDatabase({name: tablename});
				//var db = sq({name: tablename});
			}catch(err){
				alert(err);
				console.warn("No SQLLITE found");
			}

			return db;
		}
		,createStore: function(db, tablename, dropIfExist, tablecolumns) {
			if(typeof db === "undefined" || db == null){
				alert("Database is not initialized");
				return;
			}

			db.transaction(function(tx) {
				if(dropIfExist)
	        		tx.executeSql('DROP TABLE IF EXISTS '+tablename);

	        	//id integer primary key, data text, data_num integer
	            tx.executeSql('CREATE TABLE IF NOT EXISTS '+tablename+' ('+tablecolumns.toString()+')');
	        });
		},
		insertData: function(db, tablename, tablecolumns, datacolumns, placeholder){
			if(typeof db === "undefined" || db == null){
				alert("Database is not initialized");
				return;
			}

			db.transaction(function(tx) {
                tx.executeSql("INSERT INTO "+tablename+" ("+tablecolumns.toString()+") VALUES ("+placeholder.toString()+")", datacolumns, function(tx, res) {
                db.transaction(function(tx) {
                	//alert("Data inserted "+ tx);
                });
             });
            }, function(e) {
              console.log("ERROR: " + e.message);
            });
		},
		getDataArray:function(db, tablename){
			if(typeof db === "undefined" || db == null){
				alert("Database is not initialized");
				return;
			}

			var dataArray = [];
			
			db.transaction(function(transaction) {
			   transaction.executeSql('SELECT * FROM '+tablename+';', [],
                 function(transaction, result) {
                  if (result != null && result.rows != null) {
                    for (var i = 0; i < result.rows.length; i++) {
                      var row = result.rows.item(i);
                      dataArray.push(row);
                    }

                    topic.publish("sqllite.data.array."+tablename, dataArray);
                  }
                 },errorHandler);
             },errorHandler,nullHandler);

			function errorHandler(transaction, error) {
               alert('Error: ' + error.message + ' code: ' + error.code);
            }
             
            function nullHandler(){
               //alert("Null!");
            };
		}
	});
});