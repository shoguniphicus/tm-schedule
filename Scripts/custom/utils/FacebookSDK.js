define(["dojo/_base/declare",
 		"dijit/_Widget",
 		"dojo/_base/array",
 		"dojo/query",
 		"dojo/on",
 		"dojo/dom-style",
 		"dojo/dom-attr",
		"dojo/topic",
		"dojo/window",
		"dojo/dom-geometry",
		], 
        function(declare,_Widget, array, query, on, domStyle, domAttr, topic, win, domGeom) {

	return declare("js.custom.utils.FacebookSDK", null,{
		login:function(){
			var context = this;
			FB.login(function(response) {
			   if (response.authResponse) {
			     FB.api('/me', function(response) {
			     	$.jStorage.set("fb.data", response);
			     	context.getLoginStatus();
			     });
			   } else {
			     console.log('User cancelled login or did not fully authorize.');
			   }
			 });
		},getLoginStatus: function() {
			FB.getLoginStatus(function(response) {
			  if (response.status === 'connected') {
			    // the user is logged in and has authenticated your
			    // app, and response.authResponse supplies
			    // the user's ID, a valid access token, a signed
			    // request, and the time the access token 
			    // and signed request each expire
			    var uid = response.authResponse.userID;
			    var accessToken = response.authResponse.accessToken;
			    
			    $.jStorage.set("fb.authresp", accessToken);
			    topic.publish("fb.logged.in.status", true);
			  } else if (response.status === 'not_authorized') {
			    // the user is logged in to Facebook, 
			    // but has not authenticated your app
			    topic.publish("fb.logged.in.status", null);
			  } else {
			    // the user isn't logged in to Facebook.
			    topic.publish("fb.logged.in.status", false);
			  }
			 });
		},api:function(_path){
			FB.api(_path,  function(resp) {
				//console.log(resp);
				topic.publish(_path, resp);
            });
		},share:function(_path){
			FB.ui(
			  {
			    method: 'share',
			    href: _path,
			  },
			  function(response) {
			    if (response && !response.error_code) {
			    	topic.publish("internal.tracking", "btn|facebook|share|share::"+_path);
			    } else {
			    }
			  }
			);
		}
	});
});