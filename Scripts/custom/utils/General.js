define(["dojo/_base/declare",
 		"dijit/_Widget",
 		"dojo/_base/array",
 		"dojo/query",
 		"dojo/on",
 		"dojo/dom-style",
 		"dojo/dom-attr",
		"dojo/topic",
		"dojo/window",
		"dojo/dom-geometry",
		"Scripts/custom/utils/URLparam"
		], 
        function(declare,_Widget, array, query, on, domStyle, domAttr, topic, win, domGeom, URLparam) {

	return declare("js.custom.utils.General", null,{
		createUUID: function() {
		    var s = [];
		    var hexDigits = "0123456789abcdef";
		    for (var i = 0; i < 36; i++) {
		        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
		    }
		    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
		    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
		    s[8] = s[13] = s[18] = s[23] = "-";
		
		    var uuid = s.join("");
		    return uuid;
		},
		formatCurrency: function(number) {
		    return parseFloat(number).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		},
		addInputFocus:function(dom){
            // Add focus event to all your different form elements
            on(query("input"), "focus", function(){
                var inputElement = domGeom.position(this);
                var initialHeight = parseInt(win.getBox().h);

                setTimeout(function() {
                    var newHeight = parseInt(win.getBox().h);
                    var keyboardHeight = initialHeight - newHeight;
                    //alert("initial height:"+initialHeight+" newHeight:"+newHeight);
    
                    if((parseInt(inputElement.y)+ parseInt(inputElement.h)) > newHeight){
                    	//alert("Y:"+inputElement.y)
                        //var newPosition = (parseInt(inputElement.y) + parseInt(inputElement.h)) - newHeight + 20;
                        var newPosition = parseInt(inputElement.y) + keyboardHeight;
                        $(dom).scrollTop(parseInt(inputElement.y));
                    }
                }, 500);
            });
        },
        calculatedistance:function(coorda, coordb){
        	var distanceInMeter = 0;

        	var coorda_lat_mapped = coorda.coords.latitude;
        	var coorda_lon_mapped = coorda.coords.longitude;

        	var coordb_lat_mapped = coordb.lat;
        	var coordb_lon_mapped = coordb.lon;

        	distanceInMeter = getDistanceFromLatLonInKm(coorda_lat_mapped, coorda_lon_mapped, coordb_lat_mapped, coordb_lon_mapped);
        	distanceInMeter *= 1000;
        	distanceInMeter = Math.round(distanceInMeter);

        	function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
			  var R = 6371; // Radius of the earth in km
			  var dLat = deg2rad(lat2-lat1);  // deg2rad below
			  var dLon = deg2rad(lon2-lon1); 
			  var a = 
			    Math.sin(dLat/2) * Math.sin(dLat/2) +
			    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
			    Math.sin(dLon/2) * Math.sin(dLon/2)
			    ; 
			  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
			  var d = R * c; // Distance in km
			  return d;
			}

			function deg2rad(deg) {
			  return deg * (Math.PI/180)
			}

        	return distanceInMeter
        },checkelementinviewport: function(el) {
		 	if(el == null || typeof el === "undefined")
		 	{
			 	console.warn("EL None", el);
		 		return;
		 	}
		 	
			var rect = el.getBoundingClientRect();
				
			return (
				rect.top >= 0 &&
				rect.left >= 0 &&
				Math.floor(rect.bottom) + 200 <= (window.innerHeight || document. documentElement.clientHeight) && /*or $(window).height() */
				Math.floor(rect.right) <= (window.innerWidth || document. documentElement.clientWidth) /*or $(window).width() */
			);
		},breadcrumb:function(extra){
			var params = new URLparam().getUrlParams();
			var currentPage = params['p'];
			var currentPath = "";
			return;
		}
	});
});