define(["dojo/_base/declare",
 		"dijit/_Widget",
 		"dojo/_base/array",
 		"dojo/query",
 		"dojo/on",
 		"dojo/dom-style",
 		"dojo/dom-attr",
		"dojo/topic",
		"dojo/window",
		"dojo/dom-geometry",
		], 
        function(declare,_Widget, array, query, on, domStyle, domAttr, topic, win, domGeom) {

	return declare("js.custom.utils.DynamicVendor", null,{
		inject: function(path, type) {
			switch(type){
				case "js":
					$.ajax({
					  url: dojo.config.assetURL+"js/vendor/"+path+".js",
					  dataType: "script",
					  cache: true,
					  success: function(){
					  	 topic.publish("vendor.script.loaded."+path);
					  }
					});
				break;
				
				case "css":
					var link = document.createElement('link');
					link.setAttribute('rel', 'stylesheet');
					link.setAttribute('type', 'text/css');
					link.setAttribute('href', dojo.config.assetURL+"resources/css/"+path+".css");
					document.getElementsByTagName('body')[0].appendChild(link);
				break;
				
				default:
				break;
			}
		}
	});
});