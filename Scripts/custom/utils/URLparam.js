define(["dojo/_base/declare",
 		"dijit/_Widget",
 		"dojo/_base/array",
 		"dojo/dom-style",
 		"dojo/dom-attr",
		"dojo/topic"], 
        function(declare,_Widget, array, domStyle, domAttr, topic) {

	return declare("js.custom.utils.URLparam", null,{
		getUrlParams: function getUrlParams() {
		  var paramMap = {};
		  if (location.search.length == 0) {
			return paramMap;
		  }
		  var parts = location.search.substring(1).split("&");
		  
		  for (var i = 0; i < parts.length; i ++) {
			var component = parts[i].split("=");
			paramMap [decodeURIComponent(component[0])] = decodeURIComponent(component[1]);
		  }
		  
		  return paramMap;
		},
		getUrlParamsFrom: function(_location){
			var paramMap = {};
			if (_location.length == 0) {
				return paramMap;
			}
			
			var parts = _location.substring(0).split("&&");
			for (var i = 0; i < parts.length; i ++) {
				var component = parts[i].split("??");
				
				paramMap [decodeURIComponent(component[0])] = decodeURIComponent(component[1]);
			}
			
			return paramMap;
		}
	});
});