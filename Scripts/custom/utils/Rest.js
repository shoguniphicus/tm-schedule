define(["dojo/_base/declare",
 		"dijit/_Widget", 
		"dojo/topic",
		"dojo/_base/array",
		"dojo/_base/config",
		"dojo/dom-construct",
		"dojo/request/xhr"
		], 
        function(declare,_Widget, topic, array, config, domConstruct,xhr) {

	return declare("js.custom.utils.Rest", null,{
		get: function(dataUrl) {
		 	var requestParam = dataUrl;
		 	var deferred;
		 	
			deferred = xhr(dataUrl, {
		      handleAs: "json",
		      headers: {
					    "Content-Type": "application/json"
				       }
		    });
			
			// deferred = new JsonRest({target:dataUrl});
			// return deferred.get();
			return deferred;
		},
		post: function(dataUrl, datapackage) {
		 	var requestParam = dataUrl;
		 	var deferred;
		 	
			deferred = xhr(dataUrl, {
		      handleAs: "json",
		      headers: {
					    "Content-Type": "application/json"
				       }
		    });
			
			// deferred = new JsonRest({target:dataUrl});
			// return deferred.get();
			return deferred;
		}
	});
});