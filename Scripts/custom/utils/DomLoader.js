define(["dojo/_base/declare",
 		"dijit/_Widget", 
		"dojo/topic",
		"dojo/_base/array",
		"dojo/dom-construct"], 
        function(declare,_Widget, topic, array, domConstruct) {

	return declare("js.custom.utils.DomLoader", null,{
		construct: function construct(data, container) {
			var xhrArgs = {
				    url: data,
				    handleAs: "text",
				    headers: {
				      "Content-Type": "text/plain"
				    },
				    load: function(data){
				    	
				    	var node = domConstruct.toDom(data);
				    	container.innerHTML = data;
				    },
				    error: function(error){
				      console.log("An unexpected error occurred: ", error);
				    }
				  }
				  
			var deferred = dojo.xhrGet(xhrArgs);
				  
			return deferred;
		}
	});
});