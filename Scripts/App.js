require([
		"dojo/ready",
		"dojo/html",
		"dojo/dom-construct",
		"topic",	
		"dojo/_base/window",
		"dojo/window",
		"dojo/on",
		"dojo/dom",
		"dojo/dom-style",
		"dojo/query",
		"dojo/dom-attr",
		"dojo/dom-class",
		"dojo/_base/array",
		"dojo/_base/config",
		"dojox/timing/_base",
		"dojo/_base/xhr",
		// "Scripts/custom/ui/BlockFactory",
		// "Scripts/custom/ui/ModalFactory",
		// "Scripts/custom/utils/URLparam",
		// "Scripts/custom/utils/Rest",
		// "Scripts/custom/utils/General",
		"Factory",
		"Element"
		],
		
		function(ready, _html,_domConstruct,_topic,_win, _windows,_on, _dom,_domStyle, _query,_domAttr, _domClass,_array, _config, _timer, xhr, /*BlockFactory, ModalFactory, URLparam, Rest, General, */ _Factory, _Element){
	
	var baseURL = _config.cibaseurl;
	
	var vs, width, height;
	var activeBlock = null;
	var activeModal = null;
	var activeBlockArray = new Array();
	var container = null;
	var navigationController = null;
	var navigationArrayStack = new Array();
		
	var currentURL = window.location.href;

	var keypairsData = null;
	var restrictedData = null;
	var allGlobalCount = 0;

	ready(function()
	{
		setupGlobal();
		startListening();
		getblockKeyPairs();
		console.log("[EXPRIMENTAL] (╯°Д°）╯︵ /(.□ . \)", config.isExperimental);
	});

	function init()
	{	
		setup_document();
		first_load();
		setupResize();
		setupNewsSubsribe();
	}
	
	function getblockKeyPairs(){
		var factory = _Factory.init();
		//console.log(factory);

		setTimeout(function(){
			topic.publish("app.ready");
		}, 200);
	}

	function startListening()
	{

		/*
			*This is called whenever the context block is cleared and a new navigationcontroller is created
		*/
		topic.subscribe("navigationcontroller.created", function(_block){
			navigationController = _block;
			navigationController.startup();
		});
		
		/*
			*This is called whenever a block is loaded in BlockFactory
		*/
		topic.subscribe("block.created.xhr.require", function(_block, _construct_optional){
			activeBlock = _block;
			activeBlockArray.push(_block);
			
			blockStartup(_construct_optional);

			topic.publish("navigation.controller.animate.push", activeBlockArray.length);
		});
		
		topic.subscribe("navigation.controller.back.button", function(){
			topic.publish("navigation.controller.animate.pop");
		});

		/*
			*This is called whenever the back button is pressed
		*/
		topic.subscribe("navigation.controller.animate.pop", function(){
			var cleanUpSplice = activeBlockArray.splice(activeBlockArray.length-1);
			navigationArrayStack.pop();

			$.jStorage.set("afterpop.current.page", navigationArrayStack[activeBlockArray.length-1]);

			array.forEach(cleanUpSplice, function(single, i){
				setTimeout(function(){
					if(single)
						single.destroy();
				}, 400);
			});
		});
		
		topic.subscribe("block.loaded", function(){
			// toggleLoading(false);
		});

		topic.subscribe("modal.page",function(modal){
			topic.publish("promodal.animate.hide");
			cleanup_modal();
			loadModal(modal);
		});

		topic.subscribe("promodal.animate.hide", function(){
			cleanup_modal();
		});
		
		topic.subscribe("modal.created.xhr.require", function(_modal){
			activeModal = _modal;
			modalStartup();
		});
	}

	function setup_document()
	{
		vs = windows.getBox();
		width = vs.w;
		height = vs.h;

		container = dom.byId("pagecontainer");
	}

	function first_load(){
		if(container == null)
		{
			console.warn("No container, abort!");
			return;
		}
		
		if(typeof thispage === "undefined")
			thispage = "home";
	
		BlockFactory.create("navigationcontroller", container);

		loadBlock(thispage);
	}
	
	/**
	*
	* This function loads block depending on the section name
	*
	**/
	function loadBlock(section){
		/**
		*
		* Blockfactory will create the section and place them inside navigationcontroller
		*
		**/
		BlockFactory.create(section,navigationController, keypairsData, restrictedData);

		/**
		*
		* The section name will then be pushed into an array for back button and history purposes
		*
		**/
		navigationArrayStack.push(section);

		/*
		*	Disable History stack as suspected issue from this
		*/
		/*
		if(navigationArrayStack.length > 1){
			var navigationArrayStackString = navigationArrayStack.join("|");
			History.pushState(null, section, "?p="+navigationArrayStackString);
		}else{
			History.pushState(null, section, "?p="+section);
		}*/
	}
	

	function loadModal(section)
	{
		activeModal = ModalFactory.create(section);
	}

	/**
	*
	* Blockstartup is called when the module and dependencies are loaded. Most block will need post-inject initialization
	*
	**/
	function blockStartup(_construct_optional){
		activeBlock.construct(_construct_optional);
		$("#initial-loading").hide();

		/**
		*
		* Register the current push index to the block so that it position correctly in the navigationcontroller view
		*
		**/
		activeBlock.initNavigationPush(activeBlockArray.length);
	}

	/**
		*
		* Modal Startup is called when the module and dependencies are loaded
		*
		**/
	function modalStartup()
	{
		activeModal.placeAt(win.body());
		activeModal.startup();
		activeModal.construct();
	}
	
	/**
	*
	* Cleanup is called everytime the base container needed to be cleaned up, usually by "navigation.page" topic.
	*
	**/
	function cleanup(){
		/**
		*
		* This is known as suicide switch where we tell each and everyone of them in the array to commit suicide. 
		* Then we clean up the dom after that just to be sure
		*
		**/
		array.forEach(activeBlockArray, function(single, i){
			if(single)
				single.destroy();
		})
		activeBlockArray = new Array();
		domConstruct.empty(container);
		
		/**
		*
		* Shortly after cleanup, we will destory the navigationcontroller and then create a navigationcontroller
		*
		**/
		navigationController.destroy();
		BlockFactory.create("navigationcontroller", container);

		navigationArrayStack = new Array();

		/**
		*
		* Then, we flush.
		* Need to relook into this flush issue, as login process cannot flush
		**/
		//$.jStorage.flush();
	}

	function cleanup_modal()
	{
		var body = win.body();
		
		if(domClass.contains(body, "modal-open")){
			domClass.remove(body, "modal-open");
		}
		
		if(domClass.contains(body, "page-overflow")){
			domClass.remove(body, "page-overflow");
		}
	}
	
	function setupResize(){
		var resizeTimeout = 200;
		var _semaphoreRS = null;
		
		var vs,width,height;
		
		vs = windows.getBox();
		width = vs.w;
		height = vs.h;
		
		$( window ).resize(function() {
		  if (_semaphoreRS != null) {
				clearTimeout(_semaphoreRS);
			}
			
			_semaphoreRS = setTimeout(function() {
				console.log("Resize Yo");
				vs = windows.getBox();
				width = vs.w;
				height = vs.h;
				
				topic.publish("update.window.resize", width, height);
			}, resizeTimeout);
		});
		
		setTimeout(function(){
			topic.publish("update.window.resize", width, height);
		}, 500);
	}

    function setupGlobal(){
		html = _html;
		domConstruct = _domConstruct;
		topic = _topic;
		win = _win;
		windows = _windows;
		on = _on;
		dom = _dom;
		domStyle = _domStyle;
		query = _query;
		domAttr = _domAttr;
		domClass = _domClass;
		array = _array;
		config = _config;
		timer = _timer;
		Fac = _Factory;
		Elem = _Element;
	}
});
