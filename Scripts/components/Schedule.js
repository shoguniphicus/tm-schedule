define(["declare",
		"widget",
		"templatemixin",
        Fac.path('Basepro'),
        Fac.temp('components/Schedule')
    ],
    function(declare, _Widget, TemplatedMixin, basepro, template) {
        return declare("scripts.components.pack.Schedule", [basepro, TemplatedMixin], {
            templateString: template,
            setting: null,
            construct: function(setting) {
            	var context = this;
            	var domNode = this.domNode;

                context.setting = setting;
            	
            	context.initInteraction();
            	context.startListening();
                context.setupContent(setting);
            }, startup: function() {
                var domNode = this.domNode;
                var context = this;

            }, initInteraction:function(){
	            var context = this;
	            
	        }, startListening: function(){
                var context = this;
                var domNode = this.domNode;

                context.inherited(arguments);

                var duration = 500;
                var currentScrollLeft = 0;
                var currentDivision = 0;

                on(context.scheduleContainerNode, "scroll", function(){
                    currentScrollLeft = $(context.scheduleContainerNode).scrollLeft();

                    topic.publish("schedule.scroll.top", $(context.scheduleContainerNode).scrollTop());
                    topic.publish("schedule.scroll.left", currentScrollLeft);
                });

                on(context.hourLeftNode, "click", function(){
                    getNearestWidth();

                    currentDivision--;
                    scrollLeftRight();
                });

                on(context.hourRightNode, "click", function(){
                     getNearestWidth();

                    currentDivision++;
                    scrollLeftRight();
                });

                function scrollLeftRight(){
                    var widthOfScheduleHour = $(".container-schedule").width() / 25;
                    var nextScrollPosition = currentDivision * widthOfScheduleHour;

                    $(context.scheduleContainerNode).animate({
                        'scrollLeft': nextScrollPosition
                    }, duration);
                }

                function getNearestWidth(){
                    var widthOfScheduleHour = $(".container-schedule").width() / 25;
                    currentDivision = Math.ceil(currentScrollLeft / widthOfScheduleHour);
                    // console.log(widthOfScheduleHour);
                    // return currentDivision;
                }

                var scrollHandle = topic.subscribe("schedule.show.expanded", function(_domNode){
                    $(context.scheduleContainerNode).animate({
                        'scrollTop': $(_domNode).position().top + $(context.scheduleContainerNode).scrollTop()
                    }, duration);
                });
                topic.publish("topic.handlers.add", scrollHandle, domAttr.get(domNode, "id"));

                var scrollTopHandle = topic.subscribe("schedule.scroll.top", function(value){
                    domStyle.set(context.channelListContainerNode, "top", "-"+value+"px");
                });
                topic.publish("topic.handlers.add", scrollTopHandle, domAttr.get(domNode, "id"));

                var autoScrollHandle = topic.subscribe("schedule.show.scroll.direct", function(value){
                    currentDivision = value;
                    scrollLeftRight();
                });
                topic.publish("topic.handlers.add", autoScrollHandle, domAttr.get(domNode, "id"));

                var changeDateHandle = topic.subscribe("schedule.date.change", function(_date){
                    domConstruct.empty(context.channelListContainerNode);
                    domConstruct.empty(context.listContainerNode);

                    //Have to convert date into proper API consumption url
                    context._getList(context.setting, _date);
                });
                topic.publish("topic.handlers.add", changeDateHandle, domAttr.get(domNode, "id"));
            }, setupContent: function(setting){
                var context = this;
                var domNode = this.domNode;

                Fac.spawn("ScheduleDate", context.dateContainerNode);
                Fac.spawn("ScheduleHour", context.hourContainerNode);
                
                context._getList(setting);
            }, _getList: function(setting, date){
                var context = this;
                var domNode = this.domNode;

                if (date == undefined) date = new Date();

                $('.schedule-wrap').prepend('<div class="col-xs-12 preloader" style="padding:20px 0 0;"><img class="center-block" src="' + config.imgURL + 'loading.gif" /></div>');

                setTimeout(function(){
                    context.xhr({
                        url : setting.api_url + '?date=' + date.toJSON(),
                        type: "GET",
                        cache: false,
                        dataType: "json"
                    }, function(response){
                        context._setupList(setting, response);
                    });
                }, 300);
            }, _setupList: function(setting, response){
                var context = this;
                var domNode = this.domNode;

                $('.schedule-wrap').find('.preloader').remove();

                array.forEach(response.channel_list, function(single, index){
                    Fac.spawn("ScheduleHead", context.channelListContainerNode, single);
                    Fac.spawn("ScheduleRow", context.listContainerNode, { rowSetting:single });
                });

                context._scrollToCurrentHour(setting,response);
            }, _scrollToCurrentHour: function(setting, response){
                var context = this;
                var domNode = this.domNode;
                var currentDate = new Date();
                var listDate = new Date(response.date);

                var isSameDay = (listDate.getDate() == currentDate.getDate() 
                                && listDate.getMonth() == currentDate.getMonth()
                                && listDate.getFullYear() == currentDate.getFullYear());

                if (isSameDay) {
                    var currentHour = currentDate.getHours();
                    setTimeout(function(){
                        topic.publish("schedule.show.scroll.direct", currentHour);
                    },500);
                }
            }
        });

    });