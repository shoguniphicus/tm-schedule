define(["declare",
		"widget",
		"templatemixin",
        Fac.path('Basepro'),
        Fac.temp('components/list/ScheduleDate')
    ],
    function(declare, _Widget, TemplatedMixin, basepro, template) {
        return declare("scripts.components.list.ScheduleDate", [basepro, TemplatedMixin], {
            templateString: template,
            currentViewingIndex: null,
            currentDate: null,
            construct: function(setting) {
            	var context = this;
            	var domNode = this.domNode;
            	
            	context.initInteraction();
            	context.startListening();
                context.setupContent();
            }, startup: function() {
                var domNode = this.domNode;
                var context = this;

                currentViewingIndex = 0;
                currentDate = null;
            }, initInteraction:function(){
	            var context = this;
                var domNode = this.domNode;
	            
                on(context.previousNode, "click", function(){
                    currentViewingIndex--;
                    context._setupDate();
                });

                on(context.nextNode, "click", function(){
                    currentViewingIndex++;
                    context._setupDate();
                });
	        }, startListening: function(){
                var context = this;
                var domNode = this.domNode;

                context.inherited(arguments);

                var changeDateHandle = topic.subscribe("calendar.date.change", function(_date){
                    currentDate = _date;
                    currentViewingIndex = 0;
                    context._setupDate();
                });
                topic.publish("topic.handlers.add", changeDateHandle, domAttr.get(domNode, "id"));
            }, setupContent: function(setting){
                var context = this;
                var domNode = this.domNode;

                context._setupDate();
                context._setupCalendar();
            }, _setupCalendar: function(){
                var context = this;
                var domNode = this.domNode;

                // $(context.datepickerNode).datepicker();

                var chooseDate = $(context.datepickerNode).datepicker({
                  onRender: function(date) {
                    return date.valueOf() < now.valueOf() ? 'disabled' : '';
                  }
                }).on('changeDate', function(ev) {
                  var newDate = new Date(ev.date);

                  topic.publish("calendar.date.change", newDate);

                  chooseDate.datepicker('hide');
                });
            }, _setupDate:function(){
                var context = this;
                var domNode = this.domNode;

                domConstruct.empty(context.listNode);

                if(currentDate == null)
                    currentDate = new Date();

                function calcDate(index){
                    var _currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate());
                    _currentDate.setDate(currentDate.getDate()+index+currentViewingIndex);

                    var currentMonth = _currentDate.getMonth()+1;
                    var currentDay = _currentDate.getDate();
                    var currentDayName = _currentDate.getDay();

                    var dayArray = ["SUN","MON","TUE","WED","THU","FRI","SAT"];

                    if(currentDay < 10)
                        currentDay = "0"+currentDay;

                    if(currentMonth < 10)
                        currentMonth = "0"+currentMonth;

                    return {
                                day:dayArray[currentDayName],
                                date: currentDay+"/"+currentMonth,
                                dateSetting: _currentDate
                            }
                }                

                for(var i=-1; i < 6; i++){
                    var daySetting = calcDate(i);

                    if(i == 0){
                        topic.publish("schedule.date.change", daySetting.dateSetting);
                        daySetting.active = true;
                    }

                    Fac.spawn("ScheduleDateCell", context.listNode, daySetting);
                }
            }, _calculateDaysInMonth:function(){
                var domNode = this.domNode;
                var context = this;
                var isLeapYear = context._isLeapYear(new Date(dateData.year, dateData.month, 1));
                var month = dateData.month + 1;
                var days = 0;
                
                switch(month){
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:
                        days = 31;
                    break;
                    
                    case 2:
                        if(isLeapYear)
                        {
                            days = 29;
                        }else{
                            days = 28;
                        }
                    break;
                    
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                        days = 30;
                    break;
                    
                    default:
                    break;
                }
                
                return days;
            }, _isLeapYear:function(utc) {
                var y = utc.getFullYear();
                return !(y % 4) && (y % 100) || !(y % 400);
            }
        });

    });