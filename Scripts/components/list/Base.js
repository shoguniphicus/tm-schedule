define(["declare",
		"widget",
		"templatemixin",
        Fac.path('Basepro'),
        Fac.temp('components/list/Base')
    ],
    function(declare, _Widget, TemplatedMixin, basepro, template) {
        return declare("scripts.components.list.Base", [basepro, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
            	var context = this;
            	var domNode = this.domNode;
            	
            	context.initInteraction();
            	context.startListening();

                if (typeof setting.api_url !== 'undefined') {
                    context._getList(setting);
                } else {
                    context._setupList(setting);
                }
            }, startup: function() {
                var domNode = this.domNode;
                var context = this;

                context.inherited(arguments);
            }, initInteraction:function(){
	            var context = this;
                var domNode = this.domNode;
                
	            context.inherited(arguments);
	        }, startListening: function(){
	        	var context = this;
                var domNode = this.domNode;
                
                context.inherited(arguments);
            }, _setupList: function(setting){
                var context = this;
                var domNode = this.domNode;

                array.forEach(setting.list, function(single, index){
                     Fac.spawn(setting.cellClass, domNode, {
                        cellSetting: single,
                        index: index
                    });
                });
            }, _getList: function(setting){
                var context = this;
                var domNode = this.domNode;

                context.xhr({
                    url : setting.api_url,
                    type : "GET",
                    cache : false,
                    dataType : "json"
                }, function(response){
                    setting.list = response;
                    context._setupList(setting);
                });
            }
        });

    });