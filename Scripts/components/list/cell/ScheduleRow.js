define(["declare",
		"widget",
		"templatemixin",
        Fac.path('Basepro'),
        Fac.temp('components/list/cell/ScheduleRow')
    ],
    function(declare, _Widget, TemplatedMixin, basepro, template) {
        return declare("scripts.components.list.cell.ScheduleRow", [basepro, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
            	var context = this;
            	var domNode = this.domNode;
                
            	context.initInteraction();
            	context.startListening();
                context.setupContent(setting);
            }, startup: function() {
                var domNode = this.domNode;
                var context = this;

            }, initInteraction:function(){
	            var context = this;
	            
	        }, startListening: function(){
	            var context = this;
	            var domNode = this.domNode;

                context.inherited(arguments);
            }, setupContent: function(setting){
                var context = this;
                var domNode = this.domNode;

                array.forEach(setting.rowSetting.list, function(single, index){
                    Fac.spawn("ScheduleColumn", context.listContainerNode, 
                            {
                                rowSetting: single
                            });
                });
            }
        });

    });