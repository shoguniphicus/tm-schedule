define(["declare",
		"widget",
		"templatemixin",
        Fac.path('Basepro'),
        Fac.temp('components/list/cell/ScheduleDate')
    ],
    function(declare, _Widget, TemplatedMixin, basepro, template) {
        return declare("scripts.components.list.ScheduleDate", [basepro, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
            	var context = this;
            	var domNode = this.domNode;
            	
            	context.initInteraction(setting);
            	context.startListening();
                context.setupContent(setting);
            }, startup: function() {
                var domNode = this.domNode;
                var context = this;
            }, initInteraction:function(setting){
	            var context = this;
	            var domNode = this.domNode;

                on(domNode, "click", function(){
                    if(typeof setting.active === "undefined"){
                        topic.publish("calendar.date.change", setting.dateSetting);
                    }
                });
	        }, startListening: function(){
                var context = this;
                var domNode = this.domNode;

                context.inherited(arguments);
            }, setupContent: function(setting){
                var context = this;
                var domNode = this.domNode;

                html.set(context.dayNode, setting.day);
                html.set(context.dateNode, setting.date);

                if(typeof setting.active !== "undefined"){
                    domClass.add(domNode, "active");
                }
            }
        });

    });