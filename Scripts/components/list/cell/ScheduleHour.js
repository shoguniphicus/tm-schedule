define(["declare",
		"widget",
		"templatemixin",
        Fac.path('Basepro'),
        Fac.temp('components/list/cell/ScheduleHour')
    ],
    function(declare, _Widget, TemplatedMixin, basepro, template) {
        return declare("scripts.components.list.cell.ScheduleHour", [basepro, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
            	var context = this;
            	var domNode = this.domNode;
                
            	context.initInteraction();
            	context.startListening();
                context.setupContent(setting);
            }, startup: function() {
                var domNode = this.domNode;
                var context = this;

            }, initInteraction:function(){
	            var context = this;
	            
	        }, startListening: function(){
	            var context = this;
	            var domNode = this.domNode;

                context.inherited(arguments);
            }, setupContent: function(setting){
                var context = this;
                var domNode = this.domNode;

                html.set(context.labelNode, setting.hour+":00");
            }
        });

    });