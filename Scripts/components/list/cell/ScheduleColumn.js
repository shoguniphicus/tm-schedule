define(["declare",
		"widget",
		"templatemixin",
        Fac.path('Basepro'),
        Fac.temp('components/list/cell/ScheduleColumn')
    ],
    function(declare, _Widget, TemplatedMixin, basepro, template) {
        return declare("scripts.components.list.cell.ScheduleColumn", [basepro, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
            	var context = this;
            	var domNode = this.domNode;
                
                if(setting.rowSetting == null)
                    return;

            	context.initInteraction(setting);
            	context.startListening(setting);
                context.setupContent(setting);
            }, startup: function() {
                var domNode = this.domNode;
                var context = this;

            }, initInteraction:function(setting){
	            var context = this;
                var domNode = this.domNode;
	           
                var isActive = domAttr.get(domNode, "data-active");

                if(typeof setting.rowSetting.showtime.additional !== "undefined"){
                    on(context.showMoreNode, "click", function(){
                        domClass.add(context.innerNode, "active");

                        domStyle.set(context.showLessNode, "display", "block");
                        domStyle.set(context.showMoreNode, "display", "none");
                        domStyle.set(context.additionalContainerNode, "display", "block");
                        domClass.add(context.additionalContainerNode, 'in');

                        domAttr.set(domNode, "data-active", 'true');
                        isActive = domAttr.get(domNode, "data-active");

                        topic.publish("schedule.show.expanded", domNode);
                    });

                    on(context.showLessNode, "click", function(){
                        isActive = domAttr.get(domNode, "data-active");
                        if(isActive){
                            domAttr.set(domNode, "data-active", 'false');
                            isActive = domAttr.get(domNode, "data-active");

                            domClass.remove(context.innerNode, "active");
                            domClass.remove(context.additionalContainerNode, 'in');

                            domStyle.set(context.showLessNode, "display", "none");
                            domStyle.set(context.showMoreNode, "display", "block");
                            domStyle.set(context.additionalContainerNode, "display", "none");
                            return;
                        }
                    });
                }
	        }, startListening: function(setting){
	            var context = this;
	            var domNode = this.domNode;

                context.inherited(arguments);

                var scrollHandle = topic.subscribe("schedule.show.expanded", function(_domNode){
                    var isActive = domAttr.get(domNode, "data-active");

                    if(typeof setting.rowSetting.showtime.additional === "undefined")
                        return;

                    if(isActive == "false")
                        return;

                    if(_domNode == domNode)
                        return;

                    //This is to ensure additional ScheduleColumn wihtin ScheduleColumn is not affected by this topic
                    if(typeof setting.isAdditional !== "undefined")
                        return;

                    domAttr.set(domNode, "data-active", 'false');
                    domClass.remove(context.additionalContainerNode, 'in');

                    domClass.remove(context.innerNode, "active");

                    domStyle.set(context.showLessNode, "display", "none");
                    domStyle.set(context.showMoreNode, "display", "block");
                    domStyle.set(context.additionalContainerNode, "display", "none");
                });
                topic.publish("topic.handlers.add", scrollHandle, domAttr.get(domNode, "id"));
            }, setupContent: function(setting){
                var context = this;
                var domNode = this.domNode;

                html.set(context.startTimeNode, setting.rowSetting.showtime.start);
                html.set(context.labelNode, setting.rowSetting.title);
                //html.set(context.labelNode, $(domNode).position().top.toString());

                if(typeof setting.rowSetting.showtime.additional !== "undefined"){
                    domStyle.set(context.showMoreNode, "display", "block");

                    array.forEach(setting.rowSetting.showtime.additional, function(single, index){
                        Fac.spawn("ScheduleColumn", context.additionalContainerNode, 
                            {
                                cell_class:"col-xs-12 active",
                                rowSetting:single,
                                isAdditional:true
                            });
                    });
                }

                if(typeof setting.cell_class !== "undefined"){
                    domClass.remove(domNode, "col-xs-24-1");
                    domClass.add(domNode, setting.cell_class);
                }

                if(typeof setting.isAdditional !== "undefined"){
                    domClass.add(context.innerNode, "active");
                    domStyle.set(context.lineNode, "display", "block");
                }
            }
        });

    });