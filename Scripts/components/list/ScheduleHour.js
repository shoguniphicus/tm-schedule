define(["declare",
		"widget",
		"templatemixin",
        Fac.path('Basepro'),
        Fac.temp('components/list/ScheduleHour')
    ],
    function(declare, _Widget, TemplatedMixin, basepro, template) {
        return declare("scripts.components.list.ScheduleHour", [basepro, TemplatedMixin], {
            templateString: template,
            construct: function(setting) {
            	var context = this;
            	var domNode = this.domNode;
            	
            	context.initInteraction();
            	context.startListening();
                context.setupContent();
            }, startup: function() {
                var domNode = this.domNode;
                var context = this;

            }, initInteraction:function(){
	            var context = this;
	            
	        }, startListening: function(){
                var context = this;
                var domNode = this.domNode;

                context.inherited(arguments);

                var scrollHandle = topic.subscribe("schedule.scroll.left", function(value){
                    domStyle.set(domNode, "left", "-"+value+"px");
                });
                topic.publish("topic.handlers.add", scrollHandle, domAttr.get(domNode, "id"));
            }, setupContent: function(setting){
                var context = this;
                var domNode = this.domNode;

                for(var i=0; i<24; i++){
                    Fac.spawn("ScheduleHourCell", context.listNode, 
                            {
                                hour:i
                            });
                }
            }
        });

    });