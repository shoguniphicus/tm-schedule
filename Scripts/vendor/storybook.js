$(document).ready(function() {
	var rootURL = "../../";
	var elemAnimateIn = $('.animate-in'),
		currentPage = 0,
		container = $('#story-container'),
		bg01 = $('.bgPage01'),
		bg02 = $('.bgPage02'),
		bookPages = $('.bookPages'),
		btnNext = $('#pageButton .btn-pageNext'),
		btnPrev = $('#pageButton .btn-pagePrev');

	var cloudsCover01, cloudsCover02,
		clouds101, clouds102, clouds103,
		clouds201, clouds202,
		clouds301, clouds302, leaves3,
		clouds401, clouds402, rainbow4,
		clouds501, clouds502,
		clouds801, clouds802,
		clouds901,
		clouds1001, clouds1002,
		clouds1101, clouds1102,
		bubble01, bubble02, bubble03,
		ball7, stars701, stars702, stars703,
		love701, love702, love903,
		note901, note902, note903;

	$('.buttons-nojs').hide();

	// add 20px to every element that will be animated in.
	elemAnimateIn.each(function() {
		resetPos($(this));
	});

	// Assign backgrounds
	bg01.css({
		'background' : 'url("'+rootURL+'img/storybook/why/bgPage0.jpg") 0 0 repeat',
		'z-index' : 0
	}).attr('data-current', '0');

	bg02.css({
		'background' : 'url("'+rootURL+'/img/storybook/why/bgPage1.jpg") 0 0 repeat',
		'z-index' : -1
	}).attr('data-current', '-1');

	// Initiate cover page
	animateIn($('#get-storybook-why0'), 500);
	panning($('.why0-cloud01'), '-247', '837', 30000);
	panning($('.why0-cloud02'), '825', '-380', 20000);
	cloudsCover01 = setInterval(function(){
		panning($('.why0-cloud01'), '-247', '837', 30000);
	}, 31000);
	cloudsCover02 = setInterval(function(){
		panning($('.why0-cloud02'), '825', '-380', 20000);
	}, 21000);

	bookPages.css({
		'margin-left' : '-530px'
	});

	btnNext.addClass('btn-start');
	btnPrev.hide();


	btnNext.on('click', function() {
		currentPage++;

		animateOut($('.get-storybook-why').eq(currentPage-1), 750);

		
		changeBG(currentPage, currentPage+1);

		if (currentPage == 1) {
			$(this).removeClass('btn-start');
			$('.btn-pagePrev').show();
			bookPages.animate({
				'margin-left' : '-355px'
			}, 1000);
		}
		pageFlip(bookPages, currentPage, 0, 'next');
		specificPage(currentPage);

		if (currentPage == 11) {
			$(this).hide();
		} else {
			$(this).show();
		}

		$('.btn-disable').show();
		return false;
	});

	btnPrev.on('click', function() {
		currentPage--;

		animateOut($('.get-storybook-why').eq(currentPage+1), 750);

		changeBG(currentPage, currentPage+1);
		pageFlip(bookPages, currentPage, 9, 'prev');

		if (currentPage == 0) {
			bookPages.animate({
				'margin-left' : '-530px'
			}, 1000);

			$(this).hide();
			btnNext.addClass('btn-start');
		}

		if (currentPage == 11) {
			btnNext.hide();
		} else {
			btnNext.show();
		}
		// console.log(currentPage);
		specificPage(currentPage);
		$('.btn-disable').show();
		return false;
	});

	$('#storybook-backtostart').on('click', function() {
		animateOut($('.get-storybook-why').eq(11), 750);
		animateIn($('#get-storybook-why0'), 500);
		bookPages.css({
			'margin-left' : '-530px',
			'background-position': '0 0'
		});

		btnNext.addClass('btn-start').show();
		btnPrev.hide();
		currentPage = 0;

		bg01.css({
			'background' : 'url("'+rootURL+'/img/storybook/why/bgPage0.jpg") 0 0 repeat',
			'z-index' : 0
		}).attr('data-current', '0');

		bg02.css({
			'background' : 'url("'+rootURL+'/img/storybook/why/bgPage1.jpg") 0 0 repeat',
			'z-index' : -1
		}).attr('data-current', '-1');
		return false;
	});

	function changeBG(pageOn, pageNext) {
		if (bg01.attr('data-current') == 0) {
			bg01.animate({
				'opacity' : 0
			}, 500, function() {
				bg01.css({
					'opacity': 1,
					'z-index': '-1',
					'background': 'url("'+rootURL+'/img/storybook/why/bgPage' + pageNext + '.jpg") 0 0 repeat'
				}).attr('data-current', '-1');
				bg02.css({
					'z-index': 0,
					'background': 'url("'+rootURL+'/img/storybook/why/bgPage' + pageOn + '.jpg") 0 0 repeat'
				}).attr('data-current', '0');
			})
		}

		if (bg01.attr('data-current') == -1) {
			bg02.animate({
				'opacity' : 0
			}, 500, function() {
				bg02.css({
					'opacity': 1,
					'z-index': '-1',
					'background': 'url("'+rootURL+'/img/storybook/why/bgPage' + pageNext + '.jpg") 0 0 repeat'
				}).attr('data-current', '-1');
				bg01.css({
					'z-index': 0,
					'background': 'url("'+rootURL+'/img/storybook/why/bgPage' + pageOn + '.jpg") 0 0 repeat'
				}).attr('data-current', '0');
			})
		}
	}

	function specificPage(currentPage) {
		// cover page
		if (currentPage != 0) {
			clearInterval(cloudsCover01);
			clearInterval(cloudsCover02);
		}

		if (currentPage == 1) {
			panning($('.why1-cloud01'), '824', '-492', 30000);
			panning($('.why1-cloud02'), '-363', '824', 15000);
			panning($('.why1-cloud03'), '-160', '825', 10000);
			clouds101 = setInterval(function(){
				panning($('.why1-cloud01'), '824', '-492', 30000);
			}, 31000);
			clouds102 = setInterval(function(){
				panning($('.why1-cloud02'), '-363', '824', 15000);
			}, 16000);
			clouds103 = setInterval(function(){
				panning($('.why1-cloud03'), '-160', '825', 10000);
			}, 11000);
		} else {
			$('.why1-cloud01, .why1-cloud02, .why1-cloud03').stop(true, true);
			clearInterval(clouds101);
			clearInterval(clouds102);
			clearInterval(clouds103);
		}

		if (currentPage == 2) {
			panning($('.why2-cloud01'), '-445', '825', 20000);
			panning($('.why2-cloud02'), '825', '-395', 15000);
			clouds201 = setInterval(function(){
				panning($('.why2-cloud01'), '-445', '825', 20000);
			}, 21000);
			clouds202 = setInterval(function(){
				panning($('.why2-cloud02'), '825', '-395', 15000);
			}, 16000);

			fallIn($('.why2-apple'), 1300, 1500, 200, 500);
		} else {
			$('.why2-cloud01, .why2-cloud02').stop(true, true);
			clearInterval(clouds201);
			clearInterval(clouds202);
		}

		if (currentPage == 3) {
			panning($('.why3-cloud01'), '825', '-395', 25000);
			panning($('.why3-cloud02'), '830', '-315', 30000);
			panning($('.why3-leaves'), '-120', '830', 5000);
			clouds301 = setInterval(function(){
				panning($('.why3-cloud01'), '825', '-395', 25000);
			}, 26000);
			clouds302 = setInterval(function(){
				panning($('.why3-cloud02'), '830', '-315', 30000);
			}, 31000);
			leaves3 = setInterval(function(){
				panning($('.why3-leaves'), '-120', '830', 5000);
			}, 6000);
		} else {
			$('.why3-cloud01, .why3-cloud02, .why3-leaves').stop(true, true);
			clearInterval(clouds301);
			clearInterval(clouds302);
			clearInterval(leaves3);
		}

		if (currentPage == 4) {
			panning($('.why4-cloud01'), '825', '-565', 35000);
			panning($('.why4-cloud02'), '-50', '830', 30000);
			panningLR($('.why4-rainbow'), '84', '204', 10000);
			clouds401 = setInterval(function(){
				panning($('.why4-cloud01'), '825', '-565', 35000);
			}, 36000);
			clouds402 = setInterval(function(){
				panning($('.why4-cloud02'), '-50', '830', 30000);
			}, 31000);
			rainbow4 = setInterval(function() {
				panningLR($('.why4-rainbow'), '84', '204', 10000);
			}, 11000)
		} else {
			$('.why4-cloud01, .why4-cloud02').stop(true, true);
			clearInterval(clouds401);
			clearInterval(clouds402);
		}

		if (currentPage == 5) {
			panning($('.why5-cloud01'), '825', '-568', 35000);
			panning($('.why5-cloud02'), '-175', '825', 30000);
			clouds401 = setInterval(function(){
				panning($('.why5-cloud01'), '825', '-568', 35000);
			}, 36000);
			clouds402 = setInterval(function(){
				panning($('.why5-cloud02'), '-175', '825', 30000);
			}, 31000);

			$('.why5-milk').css('opacity', '0');
			fallIn($('.why5-milk'), 1500, 0, 192, 500);
		} else {
			$('.why5-cloud01, .why5-cloud02').stop(true, true);
			clearInterval(clouds501);
			clearInterval(clouds502);
		}

		// bath page
		if (currentPage == 6) {
			wiggle($('.why6-bubble02'), 600, 600, 4000);
			wiggle($('.why6-bubble01'), 200, 300, 2000);
			wiggle($('.why6-bubble03'), 500, 300, 4000);
			bubble01 = setInterval(function() {
				wiggle($('.why6-bubble01'), 200, 300, 2000);
			}, 2100);
			bubble02 = setInterval(function() {
				wiggle($('.why6-bubble02'), 600, 600, 4000);
			}, 4100);
			bubble03 = setInterval(function() {
				wiggle($('.why6-bubble03'), 500, 300, 4000);
			}, 4100);

			$('.why6-tiles').animate({'opacity': 1}, 500, function() {
				$('.why6-curtain').delay(1000).animate({'right': '480px'}, 1500);
				$('.text-floatup').each(function() {
					$(this).delay($(this).data('queue')*300).animate({
						'bottom' : parseInt($(this).data('pos-bottom')) +'px',
						'opacity' : 1
					}, 1000);
				});
			});
		} else {
			clearInterval(bubble01);
			clearInterval(bubble02);
			clearInterval(bubble03);
			$('.why6-bubble01, .why6-bubble02, .why6-bubble03').stop(true, true);
			$('.why6-curtain').css({'right': '210px'});
		}
		
		// night page
		if (currentPage == 7) {
			fadeBlink($('.why7-stars01'), 1000, 1000);
			fadeBlink($('.why7-stars02'), 500, 1000);
			fadeBlink($('.why7-stars03'), 2000, 500);
			wiggle($('.why7-love01'), 50, 100, 2000);
			wiggle($('.why7-love02'), 50, 50, 2000);
			stars701 = setInterval(function() {
				fadeBlink($('.why7-stars01'), 1000, 1000)
			}, 2100);
			stars702 = setInterval(function() {
				fadeBlink($('.why7-stars02'), 500, 1000)
			}, 2000);
			stars703 = setInterval(function() {
				fadeBlink($('.why7-stars03'), 2000, 500)
			}, 3000);
			love701 = setInterval(function() {
				wiggle($('.why7-love01'), 50, 100, 2000);
			}, 2100);
			love702 = setInterval(function() {
				wiggle($('.why7-love02'), 50, 50, 2000);
			}, 2100);

			ball7 = setInterval(function() {
				panningLR($('.why7-ball'), '290', '130', 5000);
			}, 5000);
		} else {
			$('.why7-stars01, .why7-stars02, .why7-stars03, .why7-ball').stop(true, true);
			clearInterval(stars701);
			clearInterval(stars702);
			clearInterval(stars703);
			clearInterval(love701);
			clearInterval(love702);
			clearInterval(ball7);
		}

		if (currentPage == 8) {
			panning($('.why8-cloud01'), '825', '-132', 35000);
			panning($('.why8-cloud02'), '-96', '825', 30000);
			clouds801 = setInterval(function(){
				panning($('.why8-cloud01'), '825', '-132', 35000);
			}, 36000);
			clouds802 = setInterval(function(){
				panning($('.why8-cloud02'), '-96', '825', 30000);
			}, 31000);
		} else {
			$('.why8-cloud01, .why8-cloud02').stop(true, true);
			clearInterval(clouds801);
			clearInterval(clouds802);
		}

		if (currentPage == 9) {
			wiggle($('.why9-note01'), 100, 100, 2000);
			wiggle($('.why9-note02'), 100, 100, 2000);
			wiggle($('.why9-note03'), 100, 100, 2000);
			panning($('.why9-clouds'), '825', '-560', 35000);
			clouds901 = setInterval(function(){
				panning($('.why9-clouds'), '825', '-560', 35000);
			}, 36000);
			note901 = setInterval(function(){
				wiggle($('.why9-note01'), 100, 100, 2000);
			}, 2100);
			note902 = setInterval(function(){
				wiggle($('.why9-note02'), 100, 100, 2000);
			}, 2100);
			note903 = setInterval(function(){
				wiggle($('.why9-note03'), 100, 100, 2000);
			}, 2100);
		} else {
			$('.why9-clouds, .why9-note01, .why9-note02, .why9-note03').stop(true, true);
			clearInterval(clouds901);
			clearInterval(note901);
			clearInterval(note902);
			clearInterval(note903);
		}

		if (currentPage == 10) {
			panning($('.why10-clouds01'), '825', '-580', 30000);
			panning($('.why10-clouds02'), '-305', '825', 35000);
			clouds1001 = setInterval(function(){
				panning($('.why10-clouds01'), '825', '-580', 30000);
			}, 31000);
			clouds1002 = setInterval(function(){
				panning($('.why10-clouds02'), '-305', '825', 35000);
			}, 36000);
		} else {
			$('.why10-clouds01, .why10-clouds02').stop(true, true);
			clearInterval(clouds1001);
			clearInterval(clouds1002);
		}

		if (currentPage == 11) {
			panning($('.why11-clouds01'), '-480', '825', 30000);
			panning($('.why11-clouds02'), '823', '-90', 20000);
			clouds1101 = setInterval(function(){
				panning($('.why11-clouds01'), '-480', '825', 30000);
			}, 31000);
			clouds1102 = setInterval(function(){
				panning($('.why11-clouds02'), '823', '-90', 20000);
			}, 21000);
		} else {
			$('.why11-clouds01, .why11-clouds02').stop(true, true);
			clearInterval(clouds1101);
			clearInterval(clouds1102);
		}
	}
});


// meant to reset position of elements with .animate-in class
function resetPos(item) {
	// get offset position of element
	var currentPos = item.data('pos-bottom');

	item.css({
		'bottom' : parseInt(currentPos - 10) + 'px',
		'opacity' : 0
	});
}

function pageFlip(item, page, p, dir) {
	var flip = setInterval(function() {
		if (dir == 'next') {
			p++;
			if (p == 8) {
				clearInterval(flip);

				animateIn($('.get-storybook-why').eq(page), 750, page);
			}
		} else if (dir == 'prev') {
			p--;
			if (p == 0) {
				clearInterval(flip);
				animateIn($('.get-storybook-why').eq(page), 750, page);
			}
		}

		if (page == 1) {
			item.css({
				'background-position': (p*-710) + 'px -342px'
			});
			console.log('this one')
		} else if (page == 0)  {
			item.css({
				'background-position': (p*-710) + 'px 0px'
			});
		} else {
			item.css({
				'background-position': (p*-710) + 'px ' + ((page-1)*-342) + 'px'
			});
		}
	}, 50)
}

function animateIn(item, speed, page) {
	var elemIn = item.find('.animate-in'),
		elemMask = item.find('.mask-in'),
		elem = item.find('.element');
	item.show();

	elemIn.each(function() {
		$(this).delay($(this).data('queue')*250).animate({
			'bottom' : parseInt($(this).data('pos-bottom')) +'px',
			'opacity': 1
		}, speed);
	});

	if (elemMask.length > 0) {
		elemMask.each(function() {
			var dir = $(this).data('dir'),
				size = $(this).data('size');

			if (dir == 'h') {
				$(this).delay($(this).data('queue')*250).animate({
					'height': size
				}, 1000);
			}

			if (dir == 'v') {
				$(this).delay($(this).data('queue')*250).animate({
					'width': size
				}, 1000);
			}
		});
	}

	$('.btn-disable').delay(2000).hide();
}

function animateOut(item) {
	var elem = item.find('.animate-in'),
		mask = item.find('.mask-in');

	item.fadeOut(500);
	elem.animate({'opacity' : 0}, 500, function() {
		resetPos(item.find('.animate-in'));
	});

	if (mask.length > 0) {
		mask.each(function() {
			if ($(this).data('dir') == 'h') {
				$(this).css('height', '0');
			}
			if ($(this).data('dir') == 'v') {
				$(this).css('width', '0');
			}
		});
	}
}


function panning(item, pointStart, pointEnd, speed) {
	item
	.css({
		'left': pointStart + 'px',
		'right': 'auto'
	})
	.animate({
		'left': pointEnd + 'px'
	}, speed);
}

function fadeBlink(item, speed, delay) {
	item.animate({
		'opacity': 0
	}, speed, 
	function() { // this is callback
		item.delay(delay).animate({
			'opacity': 1
		}, speed)
	});
}

function panningLR(item, pointStart, pointEnd, speed) {
	item.animate({
		'left': pointEnd
	}, speed, function() {
		item.animate({
			'left': pointStart
		}, speed)
	});
}


function wiggle(item, left, top, speed) {
	item.each(function() {
		item.animate({
			'left': Math.round(Math.random() * (left+1)) + 'px',
			'top': Math.round(Math.random() * (top+1)) + 'px'
		}, speed)
	});
}

function fallIn(item, delayFadeIn, delayFall, bottomPos, speed) {
	item.delay(delayFadeIn).animate({
		'opacity': 1
	}, 500).delay(delayFall).animate({
		'bottom' : bottomPos + 'px'
	}, speed);
}